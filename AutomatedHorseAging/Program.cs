﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using ThoroughWorks.Data;
using ThoroughWorks.Models;
using ThoroughWorks.Repository.HorseManagement;


namespace AutomatedHorseAging
{
    class Program
    {
        static void Main(string[] args)
        {
            int MinAgeToIncrement = Int32.Parse(ConfigurationManager.AppSettings["MinAgeToIncrement"]);
            int MaxAgeToIncrement = Int32.Parse(ConfigurationManager.AppSettings["MaxAgeToIncrement"]);

            // Work from older to younger
            for (int ac = MaxAgeToIncrement; ac >= MinAgeToIncrement; ac--)
            {
                List<Horse> horses = null;

                // Get all horses
                horses = (from hrs in SelectHorses.SelectAllActiveHorseForAllSubscribers()
                          where (hrs.AgeCategory.HasValue)
                          where (hrs.AgeCategory == ac)
                          select hrs).ToList();

                foreach (Horse h in horses)
                {
                    h.AgeCategory += 1;
                    h.UpdatedBy = 0;
                    h.UpdatedDate = DateTime.Now;

                    UpdateHorse uh = new UpdateHorse();
                    uh.Do(h);
                }
        
            }

        }
    }
}
