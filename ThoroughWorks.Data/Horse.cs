//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ThoroughWorks.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class Horse
    {
        public Horse()
        {
            this.CardHorses = new HashSet<CardHors>();
            this.HorseOwners = new HashSet<HorseOwner>();
            this.VetWorks = new HashSet<VetWork>();
            this.UserHorseSales = new HashSet<UserHorseSale>();
            this.ViewLogHistories = new HashSet<ViewLogHistory>();
        }
    
        public int HorseID { get; set; }
        public Nullable<int> UserID { get; set; }
        public string Name { get; set; }
        public Nullable<System.DateTime> DOB { get; set; }
        public Nullable<int> SireID { get; set; }
        public string Dam { get; set; }
        public string Image { get; set; }
        public Nullable<System.DateTime> DateArrived { get; set; }
        public Nullable<System.DateTime> DateSold { get; set; }
        public string Comments { get; set; }
        public string Attachments { get; set; }
        public bool Active { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public string Sex { get; set; }
        public string Colour { get; set; }
        public int SubscriberID { get; set; }
        public Nullable<int> AgeCategory { get; set; }
        public bool Current { get; set; }
    
        public virtual ICollection<CardHors> CardHorses { get; set; }
        public virtual Sire Sire { get; set; }
        public virtual ICollection<HorseOwner> HorseOwners { get; set; }
        public virtual ICollection<VetWork> VetWorks { get; set; }
        public virtual ICollection<UserHorseSale> UserHorseSales { get; set; }
        public virtual ICollection<ViewLogHistory> ViewLogHistories { get; set; }
        public virtual UserDetail UserDetail { get; set; }
        public virtual Subscriber Subscriber { get; set; }
    }
}
