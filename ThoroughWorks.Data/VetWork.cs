//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ThoroughWorks.Data
{
    using System;
    using System.Collections.Generic;
    
    public partial class VetWork
    {
        public VetWork()
        {
            this.VetWorkCompleteds = new HashSet<VetWorkCompleted>();
        }
    
        public int VetWorkID { get; set; }
        public int HorseID { get; set; }
        public int UserID { get; set; }
        public System.DateTime VisitDate { get; set; }
        public string InFor { get; set; }
        public string Status { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> UpdatedDate { get; set; }
        public Nullable<int> UpdatedBy { get; set; }
        public bool Active { get; set; }
        public Nullable<int> AgeCategory { get; set; }
        public string WorkCompleted { get; set; }
    
        public virtual ICollection<VetWorkCompleted> VetWorkCompleteds { get; set; }
        public virtual Horse Horse { get; set; }
        public virtual UserDetail UserDetail { get; set; }
    }
}
