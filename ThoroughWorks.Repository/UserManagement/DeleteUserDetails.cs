﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.UserManagement
{
    public class DeleteUserDetails
    {
        /// <summary>
        /// logical delete of the record
        /// </summary>
        /// <param name="userdetail"></param>
        public UserDetail Do(int userid)
        {
            //get latest
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            UserDetail userdetail = db.GetContext().UserDetails.Where(j => j.UserID == userid).FirstOrDefault();

            //set active false
            userdetail.Active = false;

            //update
            db.GetContext().UserDetails.Attach(userdetail);
            db.GetContext().Entry(userdetail).State = EntityState.Modified;
            db.SaveChanges();
            return userdetail;
        } 
    }
}
