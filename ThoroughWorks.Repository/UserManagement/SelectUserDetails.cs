﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;
using System.Data.Entity;


namespace ThoroughWorks.Repository.UserManagement
{
    public static class SelectUserDetails
    {

        #region "Select methods"
        /// <summary>
        /// select all active users
        /// </summary>
        /// <returns></returns>
        public static List<UserDetail> SelectAllActiveUsers(int SubscriptionID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            List<UserDetail> lst = db.GetContext().UserDetails.Include("UserRoles").Include("UserRoles.RoleMaster").AsQueryable().Where(j => j.Active == true && j.SubscriberUsers.Any(x => x.SubscriberID == SubscriptionID)).ToList();
            return lst;

        }
        
        /// <summary>
        /// select all active users
        /// </summary>
        /// <returns></returns>
        public static List<UserDetail> SelectAdministrationUsers()
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            List<UserDetail> lst = db.GetContext().UserDetails.Include("UserRoles").Include("UserRoles.RoleMaster").AsQueryable()
                                    .Where(j => j.Active == true 
                                                && !j.SubscriberUsers.Any()
                                                && j.UserRoles.Any(x => x.RoleMaster.RoleName == "Super Admin")).ToList();
            return lst;

        }

        /// <summary>
        /// select user by id
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public static UserDetail SelectByUserID(int userid)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            UserDetail userdetail = db.GetContext().UserDetails.Where(j => j.UserID == userid).FirstOrDefault();

            if (userdetail != null)
            {
                db.GetContext().Entry(userdetail)
                            .Collection("UserRoles")
                            .Load();
            }

            return userdetail;
        }



        /// <summary>
        /// select user by username password
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static UserDetail SelectUserByUserNamePassword(string username, string password)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            UserDetail userdetail = db.GetContext().UserDetails.Where(j => j.UserName == username && j.Password == password && j.Active == true).FirstOrDefault();

            //load roles subscriber sub entities
            if (userdetail != null)
            {
                db.GetContext().Entry(userdetail)
                                .Collection("UserRoles")
                                .Load();

                db.GetContext().Entry(userdetail)
                                .Collection("SubscriberUsers")
                                .Load();

            }
            
            return userdetail;
        }




        public static List<UserDetail> SelectVets()
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            List<UserDetail> lst = db.GetContext().UserDetails.Include("UserRoles").Include("UserRoles.RoleMaster").AsQueryable()
                                    .Where(j => j.Active == true
                                                && j.UserRoles.Any(x => x.RoleMaster.RoleName == "Vet")).ToList();
            return lst;

        }

                
        #endregion


    }
}
