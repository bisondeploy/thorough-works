﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.UserManagement
{
    public class CreateUserDetails
    {
            

        #region "Public methods"
        /// <summary>
        /// create record in database.

        /// </summary>
        /// <param name="userdetail"></param>
        public UserDetail Do(UserDetail userdetail, int SubscriberID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            if (SubscriberID > 0)
            {
                userdetail.SubscriberUsers.Add(new SubscriberUser()
                {
                    CreatedBy = userdetail.CreatedBy,
                    CreatedDate = userdetail.CreatedDate,
                    SubscriberID = SubscriberID,
                });
            }
            db.GetContext().UserDetails.Add(userdetail);
            db.SaveChanges();

            return userdetail;
        } 
        #endregion

    }
}
