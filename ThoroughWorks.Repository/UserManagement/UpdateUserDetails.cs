﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;
using RefactorThis.GraphDiff;
namespace ThoroughWorks.Repository.UserManagement
{
    public class UpdateUserDetails
    {
        /// <summary>
        /// update record in database
        /// </summary>
        /// <param name="userdetail"></param>
        public UserDetail Do(UserDetail userdetail)
        {
            UserDetail updatedobj = SelectUserDetails.SelectByUserID(userdetail.UserID);

            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            userdetail.CreatedDate = updatedobj.CreatedDate;
            userdetail.CreatedBy = updatedobj.CreatedBy;

            // don't override password if it's not provided
            if (String.IsNullOrEmpty(userdetail.Password))
                userdetail.Password = updatedobj.Password;

            db.GetContext().UpdateGraph(userdetail , map => map
                            .OwnedCollection(p => p.UserRoles)
                        );

            db.GetContext().SaveChanges();

            return userdetail;
        }


        // CHeck password entered matches existing
        public bool CheckPassword(int UserID, string pwd)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            ThoroughWorksEntities context = db.GetContext();
            UserDetail existingobj = context.UserDetails.Where(j => j.UserID == UserID).FirstOrDefault();
            return existingobj.Password == pwd;
        }

        /// <summary>
        /// change password
        /// </summary>
        /// <param name="userdetail"></param>
        /// <returns></returns>
        public UserDetail ChangePassword(UserDetail userdetail)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            ThoroughWorksEntities context = db.GetContext();
            UserDetail updatedobj = context.UserDetails.Where(j => j.UserID == userdetail.UserID).FirstOrDefault();

            updatedobj.Password = userdetail.Password;
            updatedobj.UpdatedDate = DateTime.Now;
            context.UserDetails.Attach(updatedobj);
            context.Entry(updatedobj).State = EntityState.Modified;
            db.SaveChanges();

            return updatedobj;
        }
    }
}
