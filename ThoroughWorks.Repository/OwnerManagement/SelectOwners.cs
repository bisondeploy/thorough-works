﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.OwnerManagement
{
  public class SelectOwners
  {
    /// <summary>
    /// select all active owners
    /// </summary>
    /// <returns></returns>
    public static List<Owner> SelectAllActiveOwners(int SubscriberID)
    {
      UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
      List<Owner> lst = db.GetContext().Owners.AsQueryable().Where(j => j.Active == true && j.SubscriberID == SubscriberID).ToList();
      return lst;
    }

    public static Owner SelectOwnerByID(int ownerID)
    {
      UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
      return db.GetContext().Owners.Where(j => j.OwnerID == ownerID).FirstOrDefault();
    }

    public static List<Owner> SelectOwnersByHorseSale(int userSaleLocationID)
    {
      UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

      //Get the horses at this sale
      var userHorseSalesQuery = from uhs in db.GetContext().UserHorseSales
                     where uhs.UserSaleLocationID == userSaleLocationID
                     select uhs.HorseID;
      var horseIDs = userHorseSalesQuery.ToList();

      //Get the owners of these horses
      var query = from o in db.GetContext().Owners
                  join ho in db.GetContext().HorseOwners on o.OwnerID equals ho.OwnerID
                  join h in db.GetContext().Horses on ho.HorseID equals h.HorseID
                  where horseIDs.Contains((int)h.HorseID)
                  orderby o.Name
                  select o;
     
      return query.Distinct().ToList();
    }


  }
}
