﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;
using RefactorThis.GraphDiff;

namespace ThoroughWorks.Repository.OwnerManagement
{
  public class UpdateOwner
  {
    /// <summary>
    /// update record in database
    /// </summary>
    /// <param name="owner"></param>
    public Owner Do(Owner owner)
    {
      //SelectOwners selectowner = new SelectOwners();

      Owner existingRecord = SelectOwners.SelectOwnerByID(owner.OwnerID);

      UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
      //existingRecord.OwnerID = owner.OwnerID;
      existingRecord.Name = owner.Name;
      existingRecord.EmailAddress = owner.EmailAddress;
      existingRecord.ContactNumber = owner.ContactNumber;
      //existingRecord.Active = owner.Active;

      db.GetContext().UpdateGraph(existingRecord, map => map);
      db.GetContext().SaveChanges();

      return existingRecord;
    }
  }
}
