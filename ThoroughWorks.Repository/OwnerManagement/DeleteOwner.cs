﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.OwnerManagement
{
  public class DeleteOwner
  {
    /// <summary>
    /// delete record
    /// </summary>
    /// <param name="ownerID"></param>
    public Owner Do(int ownerID)
    {
      UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

      Owner owner = db.GetContext().Owners.Where(j => j.OwnerID == ownerID).FirstOrDefault();
      owner.Active = false;

      db.SaveChanges();
      return owner;
    }
  }
}
