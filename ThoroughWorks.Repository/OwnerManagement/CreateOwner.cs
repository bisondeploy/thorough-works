﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.OwnerManagement
{
    public class CreateOwner
    {
        #region "Public methods"
        /// <summary>
        /// create record in database.
        /// </summary>
        /// <param name="owner"></param>
        public Owner Do(Owner owner)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

            owner.Active = true;
            db.GetContext().Owners.Add(owner);
            db.SaveChanges();

            return owner;
        }
        #endregion
    }
}
