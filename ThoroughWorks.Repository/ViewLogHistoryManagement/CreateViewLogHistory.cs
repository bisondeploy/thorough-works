﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.ViewLogHistoryManagement
{
    public class CreateViewLogHistory
    {
        public ViewLogHistory Do(ViewLogHistory viewloghistory)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            db.GetContext().ViewLogHistories.Add(viewloghistory);
            db.SaveChanges();

            return viewloghistory;
        }

        public List<ViewLogHistory> Do(List<ViewLogHistory> viewloghistory)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            db.GetContext().ViewLogHistories.AddRange(viewloghistory);
            db.SaveChanges();

            return viewloghistory;
        } 
    }
}
