﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.ViewLogHistoryManagement
{
    public class SelectViewLogHistory
    {
        public List<ViewLogHistory> SelectViewLogHistoryByLocation(int LocationID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            List<ViewLogHistory> lstloghistory = db.GetContext().ViewLogHistories.Include("UserDetail").Include("UserSaleLocation").Include("Horse").AsQueryable().Where(j => j.UserSaleLocationID == LocationID).ToList();
            return lstloghistory;

        }
    }
}
