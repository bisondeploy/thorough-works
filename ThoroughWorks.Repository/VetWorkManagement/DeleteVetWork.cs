﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.VetWorkManagement
{
    public class DeleteVetWork
    {
        public VetWork Do(int vetworkid)
        {
            //get latest
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            VetWork vw = db.GetContext().VetWorks.Where(j => j.VetWorkID == vetworkid).FirstOrDefault();

            //set active false
            vw.Active = false;

            //update
            db.GetContext().VetWorks.Attach(vw);
            db.GetContext().Entry(vw).State = EntityState.Modified;

            
            db.SaveChanges();
            return vw;
        } 
    }
}
