﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;
using ThoroughWorks.Models;

namespace ThoroughWorks.Repository.VetWorkManagement
{
    public class SelectVetWork
    {

        public static List<VetWork> SelectAllVetWork()
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            List<VetWork> lst = db.GetContext().VetWorks.Where(j => j.Active == true).ToList();
            return lst;

        }

        // Group by vet by day
        public static List<VetWorkDayCount> SelectVetWorkCountsByDay(int vetID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            

            List<VetWorkDayCount> vw = (from v in db.GetContext().VetWorks
                                        where v.Active
                                        where (v.UserID == vetID || vetID == 0)
                                        group v by new { v.UserDetail.UserID, v.UserDetail.UserName, v.VisitDate } into resultSet
                                        select new VetWorkDayCount { VetID = resultSet.Key.UserID, VetName = resultSet.Key.UserName, VisitDate = resultSet.Key.VisitDate, AppointmentCount = resultSet.Count() }).ToList();

            return vw;

        }


        public static List<VetWork> SelectAllVetWorkByDay(DateTime visitDate)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();


            List<VetWork> vw = (from v in db.GetContext().VetWorks
                                where v.VisitDate == visitDate
                                where v.Active
                                select v).ToList();

            return vw;

        }

        public static List<VetWork> SelectAllVetWorkByVetByDay(int VetID, DateTime visitDate)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();


            List<VetWork> vw = (from v in db.GetContext().VetWorks
                                where v.UserDetail.UserID == VetID
                                where v.VisitDate == visitDate
                                where v.Active
                                select v).ToList();

            return vw;

        }


        public static VetWork SelectVetWorkByID(int VetWorkID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();


            return db.GetContext().VetWorks.Where(j => j.VetWorkID == VetWorkID && j.Active == true).FirstOrDefault();


        }

        // Get the visit prior to the date specified
        public static DateTime? GetLastVisitDate(int horseID, DateTime visitDate)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

            var res = (from v in db.GetContext().VetWorks
                                where v.HorseID == horseID
                                where v.VisitDate < visitDate
                                where v.Active
                                orderby v.VisitDate descending 
                                select v.VisitDate).Take(1);


            if( res.ToList().Count > 0 )
                return res.ToList()[0];
            else
                return null;
        }


        public static DateTime? SelectNextVisitDate(int horseID, DateTime visitDate)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

            var res = (from v in db.GetContext().VetWorks
                       where v.HorseID == horseID
                       where v.VisitDate > visitDate
                       where v.Active
                       orderby v.VisitDate descending
                       select v.VisitDate).Take(1);


            if (res.ToList().Count > 0)
                return res.ToList()[0];
            else
                return null;
        }


        public static VetWork SelectNextVisitDetails(int horseID, DateTime visitDate)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

            var visit = db.GetContext().VetWorks.Where(j => j.HorseID == horseID && j.VisitDate > visitDate && j.Active == true).OrderByDescending( j => j.VisitDate).FirstOrDefault();

            return visit == null ? new VetWork() : visit;

        }
        

        public static List<VetWork> SelectAllVetWorkByHorse(int horseID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();


            List<VetWork> vw = (from v in db.GetContext().VetWorks
                                where v.HorseID == horseID
                                where v.Active
                                select v).ToList();

            return vw;

        }

    }
}
