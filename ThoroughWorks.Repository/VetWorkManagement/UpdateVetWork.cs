﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;
using RefactorThis.GraphDiff;

using System.Data.Entity.Validation;
using System.Diagnostics;

namespace ThoroughWorks.Repository.VetWorkManagement
{
    public class UpdateVetWork
    {
        public VetWork Do(VetWork updatedvw)
        {

            VetWork oldvw = SelectVetWork.SelectVetWorkByID(updatedvw.VetWorkID);

            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            oldvw.HorseID = updatedvw.HorseID;
            oldvw.UserID = updatedvw.UserID;
            oldvw.VisitDate = updatedvw.VisitDate;
            oldvw.InFor = updatedvw.InFor;
            oldvw.WorkCompleted = updatedvw.WorkCompleted;
            oldvw.Status = updatedvw.Status;
            oldvw.AgeCategory = updatedvw.AgeCategory;
            // The VetWorkCompleted data is in POCO objs - put in bound objs
            for( int i = 0; i < updatedvw.VetWorkCompleteds.Count; i++ )
            {
                int vwcID = updatedvw.VetWorkCompleteds.ElementAt(i).VetWorkCompletedID;
                VetWorkCompleted existingRecord = oldvw.VetWorkCompleteds.FirstOrDefault(v => v.VetWorkCompletedID == vwcID);
                if (vwcID != 0 && existingRecord != null)
                {
                    // If attribute existed, update it
                    existingRecord.AttributeValue = updatedvw.VetWorkCompleteds.ElementAt(i).AttributeValue;
                    existingRecord.UpdatedDate = updatedvw.UpdatedDate;
                    existingRecord.UpdatedBy = updatedvw.UpdatedBy;
                }
                else
                {
                    oldvw.VetWorkCompleteds.Add(updatedvw.VetWorkCompleteds.ElementAt(i));
                    oldvw.VetWorkCompleteds.Last().CreatedDate = updatedvw.UpdatedDate;
                    oldvw.VetWorkCompleteds.Last().CreatedBy = updatedvw.UpdatedBy;
                    oldvw.VetWorkCompleteds.Last().VetWorkID = oldvw.VetWorkID;
                    oldvw.VetWorkCompleteds.Last().VetWorkCompletedID = 0;
                }
            }

            oldvw.UpdatedDate = updatedvw.UpdatedDate;
            oldvw.UpdatedBy = updatedvw.UpdatedBy;


            try
            {
                db.GetContext().UpdateGraph(oldvw, map => map);

                //db.GetContext().Configuration.ProxyCreationEnabled = true;
                //db.GetContext().VetWorkCompleteds.AsNoTracking();
                db.GetContext().UpdateGraph(oldvw, map => map
                           .OwnedCollection(p => p.VetWorkCompleteds)
                       );

                db.SaveChanges();

                db.GetContext().SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw dbEx;
            }
            return oldvw;
        }
    }
}
