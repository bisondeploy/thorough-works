﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.VetWorkManagement
{
    public class CreateVetWork
    {
        #region "Public methods"
        /// <summary>
        /// create record in database.
        /// </summary>
        /// <param name="vetwork"></param>
        public VetWork Do(VetWork vw)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

            db.GetContext().VetWorks.Add(vw);
            db.SaveChanges();

            return vw;
        }
        #endregion
    }
}
