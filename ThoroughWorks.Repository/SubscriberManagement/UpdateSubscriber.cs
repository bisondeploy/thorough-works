﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;
using RefactorThis.GraphDiff;
using System.Data.Entity;
namespace ThoroughWorks.Repository.SubscriberManagement
{
    public class UpdateSubscriber
    {
        /// <summary>
        /// update subscriber
        /// </summary>
        /// <param name="subscriber">subscriber</param>
        /// <returns></returns>
        public Subscriber Do(Subscriber subscriber)
        {
            Subscriber olddata = SelectSubscriber.SelecSubscriberByID(subscriber.SubscriberID);

            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            subscriber.CreatedDate = olddata.CreatedDate;
            subscriber.CreatedBy = olddata.CreatedBy;

            db.GetContext().UpdateGraph(subscriber);

            db.GetContext().SaveChanges();

            return subscriber;
        }

        /// <summary>
        /// update subscriber payment
        /// </summary>
        /// <param name="subscriberpayment">object of SubscriberPayment</param>
        /// <returns></returns>
        public SubscriberPayment UpdatePayment(SubscriberPayment subscriberpayment)
        {
            SubscriberPayment olddata = SelectSubscriber.SelectSubscriberPaymentByID(subscriberpayment.SubsciberPaymentID);

            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            subscriberpayment.CreatedDate = olddata.CreatedDate;
            subscriberpayment.CreatedBy = olddata.CreatedBy;

            db.GetContext().UpdateGraph(subscriberpayment);
            db.GetContext().SaveChanges();

            return subscriberpayment;
        }


        public bool RemoveImage(int id, int userID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            var sub = db.GetContext().Subscribers.FirstOrDefault(x => x.SubscriberID == id);

            sub.LogoFile = null;
            sub.UpdatedBy = userID;
            sub.UpdatedDate = DateTime.Now;

            db.GetContext().Entry(sub).State = EntityState.Modified;
            db.GetContext().SaveChanges();

            return true;
        }
        
    }
}
