﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.SubscriberManagement
{
    public class DeleteSubscriber
    {
        /// <summary>
        /// delete subscriber
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public Subscriber Do(int ID, int changedBy)
        {
            //get latest
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            db.GetContext().Configuration.LazyLoadingEnabled = true;
            db.GetContext().Configuration.ProxyCreationEnabled = true;
            Subscriber subscriber = db.GetContext().Subscribers.Where(j => j.SubscriberID == ID).FirstOrDefault();

            subscriber.Active = false;
            subscriber.UpdatedBy = changedBy;
            subscriber.UpdatedDate = DateTime.Now;
            db.SaveChanges();
            return subscriber;
        }

        public SubscriberPayment DeletePayment(int id)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            SubscriberPayment subscriberpayment = db.GetContext().SubscriberPayments.Where(j => j.SubsciberPaymentID == id).FirstOrDefault();
            db.GetContext().SubscriberPayments.Remove(subscriberpayment);
            db.SaveChanges();
            return subscriberpayment;
        }

        public SubscriberUser DeleteUser(int id)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            SubscriberUser subscriberuser = db.GetContext().SubscriberUsers.Where(j => j.SubscriberUserID == id).FirstOrDefault();
            db.GetContext().SubscriberUsers.Remove(subscriberuser);
            db.SaveChanges();
            return subscriberuser;
        }
    }
}
