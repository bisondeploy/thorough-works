﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.SubscriberManagement
{
    public class CreateSubscriber
    {
        /// <summary>
        /// create subscriber
        /// </summary>
        /// <param name="subscriber">object of subscriber</param>
        /// <returns></returns>
        public Subscriber Do(Subscriber subscriber)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            subscriber.CreatedDate = DateTime.Now;

            db.GetContext().Subscribers.Add(subscriber);
            db.SaveChanges();

            return subscriber;
        }

        public SubscriberPayment CreatePayment(SubscriberPayment payment)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            payment.CreatedDate = DateTime.Now;
            db.GetContext().SubscriberPayments.Add(payment);
            db.SaveChanges();

            return payment;
        }

        public SubscriberUser CreateUser(SubscriberUser  user)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            user.CreatedDate = DateTime.Now;
            db.GetContext().SubscriberUsers.Add(user);
            db.SaveChanges();

            return user;
        }
    }
}
