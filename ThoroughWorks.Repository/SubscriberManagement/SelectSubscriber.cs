﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.SubscriberManagement
{
    public static class SelectSubscriber
    {
        /// <summary>
        /// select all subscriber information
        /// </summary>
        /// <returns></returns>
        public static List<Subscriber> SelectAllSubscriber()
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            List<Subscriber> lst = db.GetContext().Subscribers.Include("SubscriberPayments").Include("SubscriberUsers").AsQueryable().Where(x => x.Active).ToList();
            return lst;

        }

        /// <summary>
        /// select by id primary key
        /// </summary>
        /// <param name="id">primary key</param>
        /// <returns></returns>
        public static Subscriber SelecSubscriberByID(int id)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            db.GetContext().Configuration.LazyLoadingEnabled = true;
            db.GetContext().Configuration.ProxyCreationEnabled = true;
            return db.GetContext().Subscribers.Where(j => j.SubscriberID == id).FirstOrDefault();
        }

        /// <summary>
        /// select payment by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static SubscriberPayment SelectSubscriberPaymentByID(int id)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            return db.GetContext().SubscriberPayments.Where(j => j.SubsciberPaymentID == id).FirstOrDefault();
        
        }

        public static List<int> GetSubscriberUserByID(int SubscriberID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            List<SubscriberUser> lst = db.GetContext().SubscriberUsers.Where(j => j.SubscriberID == SubscriberID).ToList();
            return (from aa in lst
                    select aa.UserID).ToList();

        }

        public static bool IsSubscriptionValidUserID(int UserID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            db.GetContext().Configuration.LazyLoadingEnabled = true;
            db.GetContext().Configuration.ProxyCreationEnabled = true;

            var subscriber = db.GetContext().Subscribers.Include("SubscriberPayments").Where(j => j.SubscriberUsers.Any(x => x.UserID == UserID) && j.Active).FirstOrDefault();

            if (subscriber != null && subscriber.SubscriberPayments != null)
                if (subscriber.SubscriberPayments.Any())
                    return (subscriber.SubscriberPayments.Max(x => x.ExpirationDate) >= DateTime.Today);
                else
                    return false;
            else
                return false;
            
        }

    }
}
