﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.RoleManagement
{
    public partial class SelectRoles
    {
        /// <summary>
        /// select all roles
        /// </summary>
        /// <returns></returns>
        public List<RoleMaster> SelectAllActiveRoles(bool isSuperAdmin)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            //return db.GetContext().RoleMasters.AsQueryable().Where(j => j.Active == true && j.RoleName != "Super Admin").ToList();
            return db.GetContext().RoleMasters.AsQueryable()
                        .Where(j => j.Active == true 
                                    && (isSuperAdmin && j.RoleName == "Super Admin"
                                        || !isSuperAdmin && j.RoleName != "Super Admin"))
                        .ToList();
        }
    }
}
