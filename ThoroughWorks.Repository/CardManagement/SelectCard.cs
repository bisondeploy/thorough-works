﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.CardManagement
{
  public static class SelectCard
  {
    /// <summary>
    /// select card by location id
    /// </summary>
    /// <param name="locationid">locationid</param>
    /// <returns></returns>
    public static List<Card> SelectCardByLocation(int locationid, DateTime FromDate, DateTime ToDate)
    {
      UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
      db.GetContext().Configuration.LazyLoadingEnabled = true;
      db.GetContext().Configuration.ProxyCreationEnabled = true;
      List<Card> lst = db.GetContext().Cards.AsQueryable()
                          .Where(j => j.Active
                                      && j.UserSaleLocationID == locationid
                                      && j.InspectionDate >= FromDate
                                      && j.InspectionDate <= ToDate)
                          .ToList();
      return lst;
    }

    /// <summary>
    /// select card for particular user
    /// </summary>
    /// <param name="UserID">userid</param>
    /// <returns></returns>
    public static List<Card> SelectCardBySubscriber(int SubscriberID)
    {
      UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
      db.GetContext().Configuration.LazyLoadingEnabled = true;
      db.GetContext().Configuration.ProxyCreationEnabled = true;
      List<Card> lst = db.GetContext().Cards.AsQueryable().Where(j => j.Active && j.SubscriberID == SubscriberID).ToList();
      return lst;
    }

    /// <summary>
    /// select card by primary key
    /// </summary>
    /// <param name="id">primary key</param>
    /// <returns></returns>
    public static Card SelectCardByID(int id)
    {
      UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
      db.GetContext().Configuration.LazyLoadingEnabled = true;
      db.GetContext().Configuration.ProxyCreationEnabled = true;
      Card card = db.GetContext().Cards.AsQueryable().Where(j => j.CardID == id).FirstOrDefault();
      return card;
    }


    public static List<Card> SelectViewersBySubscriberID(int SubscriberID)
    {
        UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
        List<Card> lst = db.GetContext().Cards.Where(j => j.Active
                                        && j.SubscriberID == SubscriberID).ToList();
        return lst;

    }

  }
}
