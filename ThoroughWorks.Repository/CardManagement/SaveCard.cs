﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ThoroughWorks.Data;
using ThoroughWorks.Models;

namespace ThoroughWorks.Repository.CardManagement
{
    public class SaveCard
    {
        SaleCard _dto;

        public SaveCard(Models.SaleCard DTO)
        {
            _dto = DTO;
        }

        /// <summary>
        /// save card details
        /// </summary>
        public bool Do()
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

            var card = db.GetContext().Cards.Include("CardHorses").FirstOrDefault(x => x.CardID == _dto.CardID);
            if (card != null)
            {
                card.CardUID = card.CardUID;
                card.UpdatedBy = _dto.UserID;
                card.UpdatedDate = DateTime.Now;
                card.PersonName = _dto.PersonName;
                card.InspectionDate = _dto.InspectionDate;
                db.SaveChanges();
                // clear all
                foreach (var horse in card.CardHorses.ToList())
                {
                    db.GetContext().CardHorses.Remove(horse);
                }
                db.SaveChanges();

                // now re-assigned changed.
                foreach (var horse in _dto.Horses.Where(x => x.Selected).ToList())
                {
                    card.CardHorses.Add(new CardHors()
                    {
                        HorseID = horse.HorseID,
                    });// remove.
                }
                db.SaveChanges();
            }

            return true;
        }
    }
}
