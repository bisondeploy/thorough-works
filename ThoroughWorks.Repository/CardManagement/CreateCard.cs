﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ThoroughWorks.Data;
using ThoroughWorks.Models;

namespace ThoroughWorks.Repository.CardManagement
{
  public class CreateCard
  {
    Models.SaleCard _dto;

    public CreateCard(Models.SaleCard DTO)
    {
      _dto = DTO;
    }

    /// <summary>
    /// create card
    /// </summary>
    /// <param name="card"></param>
    public int Do()
    {
      UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

      // generate Card Ref Number
      // get the number of interest in this sale
      var numCards = db.GetContext().Cards.Where(x => x.UserSaleLocationID == _dto.UserSaleLocationID).Count();
      bool validCardUID = false;
      var cardID = "";
      while (!validCardUID)
      {
        cardID = "";
        if (!String.IsNullOrEmpty(_dto.PersonName))
        {
          char[] charSeparators = new char[] { ' ' };
          var names = _dto.PersonName.Split(charSeparators, StringSplitOptions.RemoveEmptyEntries);
          foreach (string name in names)
          {
            cardID = cardID + name.ToUpper().Substring(0, 1);
          }
        }
        cardID = cardID + ((numCards + 1) * 6).ToString();

        if (!db.GetContext().Cards.Any(x => x.Active && x.UserSaleLocationID == _dto.UserSaleLocationID && x.CardUID == cardID))
          validCardUID = true;
      }

      var card = new Card()
      {
        Active = _dto.Active,
        CardID = 0,
        CardUID = cardID,
        CreatedBy = _dto.UserID,
        CreatedDate = DateTime.Now,
        PersonName = _dto.PersonName.Trim(),
        InspectionDate = _dto.InspectionDate,
        UserID = _dto.UserID,
        SubscriberID = _dto.SubscriberID,
        UserSaleLocationID = _dto.UserSaleLocationID,
        CardHorses = new List<CardHors>(),
      };

      foreach (var horse in _dto.Horses)
      {
        if (horse.Selected)
        {
          card.CardHorses.Add(new CardHors()
            {
              HorseID = horse.HorseID,
            });
        }
      }

      db.GetContext().Cards.Add(card);
      db.SaveChanges();

      return card.CardID;
    }
  }
}
