﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ThoroughWorks.Data;
using ThoroughWorks.Models;

namespace ThoroughWorks.Repository.CardManagement
{
    public class RemoveCard
    {
        int _id;
        int _userID;

        public RemoveCard(int ID, int UserID)
        {
            _id = ID;
            _userID = UserID;
        }

        /// <summary>
        /// save card details
        /// </summary>
        public bool Do()
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

            var card = db.GetContext().Cards.FirstOrDefault(x => x.CardID == _id);
            if (card != null)
            {
                card.Active = false;
                card.UpdatedBy = _userID;
                card.UpdatedDate = DateTime.Now;
                db.SaveChanges();
            }

            return true;
        }
    }
}
