﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.UserHorseSaleManagement
{
    public class DeleteUserHorseSale
    {
        /// <summary>
        /// delete record
        /// </summary>
        /// <param name="userdetail"></param>
        public UserHorseSale Do(int ID)
        {
            //get latest
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
             
            UserHorseSale userhorsesale = db.GetContext().UserHorseSales.Where(j => j.UserHorseSaleID == ID).FirstOrDefault();

            db.GetContext().UserHorseSales.Remove(userhorsesale);

            db.SaveChanges();
            return userhorsesale;
        } 
    }
}
