﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.UserHorseSaleManagement
{
    public class CreateUserHorseSale
    {
        public UserHorseSale Do(UserHorseSale userhorsesale)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            db.GetContext().UserHorseSales.Add(userhorsesale);
            db.SaveChanges();

            return userhorsesale;
        } 
    }
}
