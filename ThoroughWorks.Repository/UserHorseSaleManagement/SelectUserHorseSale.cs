﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.UserHorseSaleManagement
{
    public static class SelectUserHorseSale
    {
        /// <summary>
        /// select  horse sale by location
        /// </summary>
        /// <param name="UserID">userid</param>
        /// <returns></returns>
        public static List<UserHorseSale> SelectUserHorseSaleByLocation(int LocationID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            List<UserHorseSale> lstUserSaleLocation = db.GetContext().UserHorseSales.Include("UserSaleLocation").Include("Horse").AsQueryable().Where(j => j.UserSaleLocationID == LocationID).ToList();
            return lstUserSaleLocation;

        }
    }
}
