﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ThoroughWorks.Models;
using ThoroughWorks.Data;
using EF.UnitWork;

namespace ThoroughWorks.Repository.UserHorseSaleManagement
{
  public class SaveUserHorseSale
  {
    List<UserHorseSale_M> _dto;
    int _userID;
    public SaveUserHorseSale(List<UserHorseSale_M> DTO, int UserID)
    {
      _dto = DTO;
      _userID = UserID;
    }

    public bool Do()
    {
      UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

      foreach (var saleHorse in _dto)
      {
        if (saleHorse.UserHorseSaleID == 0)
        {
          // is this new?
          if (saleHorse.Delete)
          {
            // if it's deleted then ignore it.
          }
          else
          {
            // add a new one.
            var ush = new UserHorseSale()
            {
              HorseID = saleHorse.HorseID,
              LotNumber = saleHorse.LotNumber,
              SessionNumber = saleHorse.SessionNumber,
              Comments = saleHorse.Comments,
              Sequence = saleHorse.Sequence,
              UserHorseSaleID = saleHorse.UserHorseSaleID,
              UserSaleLocationID = saleHorse.UserSaleLocationID,
            };
            db.GetContext().UserHorseSales.Add(ush);
          }
        }
        else
        {
          var ush = db.GetContext().UserHorseSales.FirstOrDefault(x => x.UserHorseSaleID == saleHorse.UserHorseSaleID);
          if (saleHorse.Delete)
          {
            // if it's deleted then get rid of it..
            if (ush != null)
              db.GetContext().UserHorseSales.Remove(ush);
          }
          else
          {
            // add a new one.
            ush.HorseID = saleHorse.HorseID;
            ush.LotNumber = saleHorse.LotNumber;
            ush.SessionNumber = saleHorse.SessionNumber;
            ush.Comments = saleHorse.Comments;
            ush.Sequence = saleHorse.Sequence;
            ush.UserHorseSaleID = saleHorse.UserHorseSaleID;
            ush.UserSaleLocationID = saleHorse.UserSaleLocationID;
          }
        }

      }
      db.GetContext().SaveChanges();
      return true;

    }
  }
}
