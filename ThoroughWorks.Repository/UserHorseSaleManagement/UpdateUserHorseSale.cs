﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;
using RefactorThis.GraphDiff;
using System.Data.Entity;
namespace ThoroughWorks.Repository.UserHorseSaleManagement
{
    public class UpdateUserHorseSale
    {
        /// <summary>
        /// update user sale location
        /// </summary>
        /// <param name="usersalelocation"></param>
        /// <returns></returns>
        public void SequenceUpdate(List<UserHorseSale> horsesale)
        {
            
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            foreach (UserHorseSale horse in horsesale)
            {
                UserHorseSale updatehorse = db.GetContext().UserHorseSales.Where(j => j.UserHorseSaleID == horse.UserHorseSaleID).FirstOrDefault();
                updatehorse.Sequence = horse.Sequence;
                db.GetContext().UserHorseSales.Attach(updatehorse);
                db.GetContext().Entry(updatehorse).State = EntityState.Modified;
                
            }
            db.SaveChanges();
        }
    }
}
