﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.HorseManagement
{
    public class CreateHorses
    {
        #region "Public methods"
        /// <summary>
        /// create record in database.
        /// </summary>
        /// <param name="horse"></param>
        public Horse Do(Horse horse)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            horse.CreatedDate = DateTime.Now;

            db.GetContext().Horses.Add(horse);
            db.SaveChanges();

            return horse;
        }
        #endregion
    }
}
