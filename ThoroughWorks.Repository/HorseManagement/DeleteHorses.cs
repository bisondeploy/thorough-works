﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.HorseManagement
{
    public class DeleteHorses
    {
        public Horse Do(int horseid)
        {
            //get latest
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            Horse horse = db.GetContext().Horses.Where(j => j.HorseID == horseid).FirstOrDefault();

            //set active false
            horse.Active = false;

            //update
            db.GetContext().Horses.Attach(horse);
            db.GetContext().Entry(horse).State = EntityState.Modified;

            //delete from sale
            List<UserHorseSale> lst = db.GetContext().UserHorseSales.Where(j=>j.HorseID == horseid).ToList();
            
            db.GetContext().UserHorseSales.RemoveRange(lst);
            db.SaveChanges();
            return horse;
        } 
    }
}
