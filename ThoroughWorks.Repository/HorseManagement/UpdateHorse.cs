﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;
using RefactorThis.GraphDiff;

namespace ThoroughWorks.Repository.HorseManagement
{
    public class UpdateHorse
    {
        /// <summary>
        /// update record in database
        /// </summary>
        /// <param name="userdetail"></param>
        public Horse Do(Horse updatedhorse)
        {
            SelectHorses selecthorse = new SelectHorses();

            Horse oldhorse = selecthorse.SelectHorseByID(updatedhorse.HorseID);

            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            oldhorse.Active = updatedhorse.Active;
            oldhorse.Attachments = updatedhorse.Attachments;
            oldhorse.Colour = updatedhorse.Colour;
            oldhorse.Comments = updatedhorse.Comments;
            oldhorse.Dam = updatedhorse.Dam;
            oldhorse.DateArrived = updatedhorse.DateArrived;
            oldhorse.DateSold = updatedhorse.DateSold;
            oldhorse.DOB = updatedhorse.DOB;
            oldhorse.Image = updatedhorse.Image;
            oldhorse.Name = updatedhorse.Name;
            oldhorse.HorseOwners = updatedhorse.HorseOwners;
            oldhorse.Sex = updatedhorse.Sex;
            oldhorse.SireID = updatedhorse.SireID;
            oldhorse.UpdatedDate = DateTime.Now;
            oldhorse.UpdatedBy = updatedhorse.UpdatedBy;
            oldhorse.AgeCategory = updatedhorse.AgeCategory;
            oldhorse.Current = updatedhorse.Current;

            db.GetContext().UpdateGraph(oldhorse, map => map);
            db.SaveChanges();

            db.GetContext().SaveChanges();

            return oldhorse;
        }

        public bool RemoveImage(int id, int userID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            var horse = db.GetContext().Horses.FirstOrDefault(x => x.HorseID == id);

            horse.Image = null;
            horse.UpdatedBy = userID;
            horse.UpdatedDate = DateTime.Now;

            db.GetContext().Entry(horse).State = EntityState.Modified;
            db.GetContext().SaveChanges();

            return true;
        }

        public bool RemoveAttachment(int id, int userID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            var horse = db.GetContext().Horses.FirstOrDefault(x => x.HorseID == id);

            horse.Attachments = null;
            horse.UpdatedBy = userID;
            horse.UpdatedDate = DateTime.Now;

            db.GetContext().Entry(horse).State = EntityState.Modified;
            db.GetContext().SaveChanges();

            return true;
        }
    }
}
