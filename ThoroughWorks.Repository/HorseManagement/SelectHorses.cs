﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.HorseManagement
{
    public class SelectHorses
    {
        /// <summary>
        /// select all active users
        /// </summary>
        /// <returns></returns>
        public static List<Horse> SelectAllActiveHorse(int SubscriberID, bool CurrentOnly )
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            List<Horse> lst;

            lst = db.GetContext().Horses.Include("UserDetail").AsQueryable().Where(j => j.SubscriberID == SubscriberID && j.Active == true).ToList();

            if (CurrentOnly)
                lst = lst.Where(j => j.Current == true).ToList();
            
            return lst;

        }

        public static List<Horse> SelectAllActiveHorseForAllSubscribers()
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            List<Horse> lst;

            lst = db.GetContext().Horses.Include("UserDetail").AsQueryable().Where(j => j.Active == true).ToList();

            return lst;

        }

        public static int SelectHorseCountForAllSubscribers()
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            return db.GetContext().Horses.Include("UserDetail").AsQueryable().Where(j => j.Active == true).Count();

        }

        public Horse SelectHorseByID(int horseid)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            return db.GetContext().Horses.Where(j => j.HorseID == horseid).FirstOrDefault();
        }

        public static int? SelectHorseAgeCategoryOnDate(int horseID, DateTime VisitDate)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

            Horse h = db.GetContext().Horses.Where(j => j.HorseID == horseID).FirstOrDefault();
            int? currentAgeCat = h.AgeCategory;


            // If 1st Aug is not going to roll round between now and appt, just use their current age
            DateTime dt1Aug = new DateTime(2000, 8, 1);
            if (DateTime.Now.DayOfYear < dt1Aug.DayOfYear && VisitDate.DayOfYear >= dt1Aug.DayOfYear)
            {
                // If it's in the first 3 age categories, move it up
                if (currentAgeCat >= 1 && currentAgeCat <= 3)
                {
                    currentAgeCat += 1;
                }
            }

            return currentAgeCat;
        }

    }
}
