﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.VetWorkAttributeManagement
{
    public class CreateVetWorkAttribute
    {
        #region "Public methods"
        /// <summary>
        /// create record in database.
        /// </summary>
        /// <param name="vetwork"></param>
        public VetWorkAttribute Do(VetWorkAttribute vw)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

            db.GetContext().VetWorkAttributes.Add(vw);
            db.SaveChanges();

            return vw;
        }

        #endregion
    }
}
