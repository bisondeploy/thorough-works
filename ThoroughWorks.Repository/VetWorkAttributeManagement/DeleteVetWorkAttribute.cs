﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF.UnitWork;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.VetWorkAttributeManagement
{
    public class DeleteVetWorkAttribute
    {
        public VetWorkAttribute Do(int vetworkattributeid)
        {
            //get latest
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            VetWorkAttribute vw = db.GetContext().VetWorkAttributes.Where(j => j.VetWorkAttributeID == vetworkattributeid).FirstOrDefault();

            //set active false
            vw.Active = false;

            //update
            db.GetContext().VetWorkAttributes.Attach(vw);
            db.GetContext().Entry(vw).State = EntityState.Modified;


            db.SaveChanges();
            return vw;
        } 
    }
}
