﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF.UnitWork;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.VetWorkAttributeManagement
{
    public class SelectVetWorkAttribute
    {
        public static List<VetWorkAttribute> SelectAllVetWorkAttributes(int subscriberID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            List<VetWorkAttribute> lst = db.GetContext().VetWorkAttributes.Where(j => j.Active == true && j.SubscriberID == subscriberID).OrderBy(x => x.Sequence).ToList();
            return lst;

        }



        public static VetWorkAttribute SelectVetWorkAttributeByID(int vwaID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

            return db.GetContext().VetWorkAttributes.Where(j => j.VetWorkAttributeID == vwaID && j.Active == true).FirstOrDefault();

        }


        public static List<VetWorkAttribute> SearchVetWorkAttributes(int subscriberID, string search)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            List<VetWorkAttribute> lst = db.GetContext().VetWorkAttributes.Where(j => j.SubscriberID == subscriberID && j.Active == true && j.AttributeName.Contains(search)).ToList();
            return lst;

        }

    }
}
