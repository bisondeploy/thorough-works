﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ThoroughWorks.Models;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.VetWorkAttributeManagement
{
    public class SaveAttributeSequence
    {
        List<VetWorkAttribute_M> _model;
        ThoroughWorksEntities _db;
        int _subscriberID;
        int _changedBy;

        public SaveAttributeSequence(List<VetWorkAttribute_M> Model, int SubscriberID, int ChangedBy)
        {
            _db = new ThoroughWorksEntities();
            _model = Model;
            _subscriberID = SubscriberID;
            _changedBy = ChangedBy;
        }

        public void Do()
        {
            var list = _db.VetWorkAttributes.Where(x => x.SubscriberID == x.SubscriberID && x.Active).ToList();
            foreach (var item in _model)
            {
                var dbItem = list.FirstOrDefault(x => x.VetWorkAttributeID == item.VetWorkAttributeID);
                if (dbItem != null)
                {
                    dbItem.Sequence = item.Sequence;
                    dbItem.UpdatedBy= _changedBy;
                    dbItem.UpdatedDate = DateTime.Now;
                }
            }
            _db.SaveChanges();


        }
    }
}
