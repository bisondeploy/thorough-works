﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF.UnitWork;
using ThoroughWorks.Data;
using RefactorThis.GraphDiff;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace ThoroughWorks.Repository.VetWorkAttributeManagement
{
    public class UpdateVetWorkAttribute
    {
        public VetWorkAttribute Do(VetWorkAttribute updatedvw)
        {

            VetWorkAttribute oldvw = SelectVetWorkAttribute.SelectVetWorkAttributeByID(updatedvw.VetWorkAttributeID);

            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            oldvw.AttributeName = updatedvw.AttributeName;

            oldvw.UpdatedDate = updatedvw.UpdatedDate;
            oldvw.UpdatedBy = updatedvw.UpdatedBy;


            try
            {
                db.GetContext().UpdateGraph(oldvw, map => map);
                db.SaveChanges();

                db.GetContext().SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw dbEx;
            }
            return oldvw;
        }
    }
}
