﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF.UnitWork;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.HorseOwnerManagement
{
    public class CreateHorseOwner
    {
        public HorseOwner Do(HorseOwner vw)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

            db.GetContext().HorseOwners.Add(vw);
            db.SaveChanges();

            return vw;
        }

    }
}
