﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF.UnitWork;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.HorseOwnerManagement
{
    public class DeleteHorseOwner
    {
        public HorseOwner Do(int id)
        {
            //get latest
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            HorseOwner vw = db.GetContext().HorseOwners.Where(j => j.HorseOwnerID == id).FirstOrDefault();

            if (vw != null)
            {
                db.GetContext().HorseOwners.Remove(vw);
                db.SaveChanges();
            }
            return vw;
        } 
    }
}
