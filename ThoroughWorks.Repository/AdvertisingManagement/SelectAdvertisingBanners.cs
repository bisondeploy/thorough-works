﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ThoroughWorks.Data;
using ThoroughWorks.Models;

namespace ThoroughWorks.Repository.AdvertisingManagement
{
    public static class SelectAdvertisingBanners
    {
        /// <summary>
        /// select  horse sale by location
        /// </summary>
        /// <param name="UserID">userid</param>
        /// <returns></returns>
        public static AdvertisingBanner GetByID(int ID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            return db.GetContext().AdvertisingBanners.Where(j => j.ID == ID).FirstOrDefault();
        }

        public static AdvertisingBanner_M GetByIDForDisplay(int ID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            return db.GetContext().AdvertisingBanners.Where(j => j.ID == ID)
                    .Select(j => new AdvertisingBanner_M()
                    {
                        Active = j.Active,
                        Created = j.Created,
                        CreatedBy = j.CreatedBy,
                        DisplayedFrom = j.DisplayedFrom,
                        DisplayedTo = j.DisplayedTo,
                        ID = j.ID,
                        ImagePath = j.ImagePath,
                        LastUpdated = j.LastUpdated,
                        LastUpdatedBy = j.LastUpdatedBy,
                        Name = j.Name,
                        Url = j.Url,
                    })
                    .FirstOrDefault();
        }

        public static List<AdvertisingBanner_M> GetBannerList()
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            return db.GetContext().AdvertisingBanners
                        .Where(j => j.Active)
                        .Select(j => new AdvertisingBanner_M
                            {
                                Active = j.Active,
                                Created = j.Created,
                                CreatedBy = j.CreatedBy,
                                DisplayedFrom = j.DisplayedFrom,
                                DisplayedTo = j.DisplayedTo,
                                ID = j.ID,
                                ImagePath = j.ImagePath,
                                LastUpdated = j.LastUpdated,
                                LastUpdatedBy = j.LastUpdatedBy,
                                Name = j.Name,
                                Url = j.Url,
                            })
                        .ToList();

        }

        public static List<AdvertisingBanner_M> GetCurrentBannersForDisplay()
        {
            var date = DateTime.Now;
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            return db.GetContext().AdvertisingBanners.Where(j => j.Active 
                                                                && j.DisplayedFrom <= date
                                                                && !j.DisplayedTo.HasValue || j.DisplayedTo.Value >= date)
                                                        .Select(j => new AdvertisingBanner_M()
                                                        {
                                                            Active = j.Active,
                                                            Created = j.Created,
                                                            CreatedBy = j.CreatedBy,
                                                            DisplayedFrom = j.DisplayedFrom,
                                                            DisplayedTo = j.DisplayedTo,
                                                            ID = j.ID,
                                                            ImagePath = j.ImagePath,
                                                            LastUpdated = j.LastUpdated,
                                                            LastUpdatedBy = j.LastUpdatedBy,
                                                            Name = j.Name,
                                                            Url = j.Url,
                                                        })
                                                     .ToList();
        }

        public static AdvertisingBanner_M GetRandomBannerForDisplay()
        {
            var date = DateTime.Now;
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            var list = db.GetContext().AdvertisingBanners.Where(j => j.Active
                                                                && j.DisplayedFrom <= date
                                                                && (!j.DisplayedTo.HasValue || j.DisplayedTo.Value >= date))
                                                        .Select(j => new AdvertisingBanner_M()
                                                        {
                                                            Active = j.Active,
                                                            Created = j.Created,
                                                            CreatedBy = j.CreatedBy,
                                                            DisplayedFrom = j.DisplayedFrom,
                                                            DisplayedTo = j.DisplayedTo,
                                                            ID = j.ID,
                                                            ImagePath = j.ImagePath,
                                                            LastUpdated = j.LastUpdated,
                                                            LastUpdatedBy = j.LastUpdatedBy,
                                                            Name = j.Name,
                                                            Url = j.Url,
                                                        })
                                                        .ToList();

            if (list.Count > 0)
            {
                var random = new Random();
                var number = random.Next(list.Count);
                return list[number];
            }
            else
                return null;
        }
    }
}
