﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.AdvertisingManagement
{
    public class CreateAdvertisingBanner
    {
        public AdvertisingBanner Do(AdvertisingBanner banner)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            db.GetContext().AdvertisingBanners.Add(banner);
            db.SaveChanges();

            return banner;
        } 
    }
}
