﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;
using RefactorThis.GraphDiff;
using System.Data.Entity;

namespace ThoroughWorks.Repository.AdvertisingManagement
{
    public class UpdateAdvertisingBanner
    {
        /// <summary>
        /// update user sale location
        /// </summary>
        /// <param name="usersalelocation"></param>
        /// <returns></returns>
        public AdvertisingBanner Do(AdvertisingBanner banner)
        {
            AdvertisingBanner updatedobj = SelectAdvertisingBanners.GetByID(banner.ID);

            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            banner.Created = updatedobj.Created;
            banner.CreatedBy = updatedobj.CreatedBy;

            db.GetContext().UpdateGraph(banner, map => map);
            db.GetContext().SaveChanges();

            return banner;
        }

    }
}
