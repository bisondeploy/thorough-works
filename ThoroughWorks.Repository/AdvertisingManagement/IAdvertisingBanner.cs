﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.AdvertisingManagement
{
  public interface IAdvertisingBanner
  {

    AdvertisingBanner CreateAdvertisingBanner(AdvertisingBanner model);

  }
}
