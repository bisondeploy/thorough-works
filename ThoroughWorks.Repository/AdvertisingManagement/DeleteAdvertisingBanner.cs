﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.AdvertisingManagement
{
    public class DeleteAdvertisingBanner
    {
        /// <summary>
        /// delete record
        /// </summary>
        /// <param name="userdetail"></param>
        public AdvertisingBanner Do(int ID)
        {
            //get latest
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

            AdvertisingBanner banner= db.GetContext().AdvertisingBanners.Where(j => j.ID  == ID).FirstOrDefault();
            banner.Active = false;

            db.SaveChanges();
            return banner;
        } 
    }
}
