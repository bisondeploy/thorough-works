﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF.UnitWork;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.SireManagement
{
    public class DeleteSire
    {
        public Sire Do(int Sireid)
        {
            //get latest
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            Sire vw = db.GetContext().Sires.Where(j => j.SireID == Sireid).FirstOrDefault();

            //set active false
            vw.Active = false;

            //update
            db.GetContext().Sires.Attach(vw);
            db.GetContext().Entry(vw).State = EntityState.Modified;


            db.SaveChanges();
            return vw;
        } 
    }
}
