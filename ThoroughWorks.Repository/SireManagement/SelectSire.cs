﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF.UnitWork;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.SireManagement
{
    public class SelectSire
    {
        public static List<Sire> SelectAllSires()
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            List<Sire> lst = db.GetContext().Sires.Where(j => j.Active == true).OrderBy(x => x.SireName).ToList();
            return lst;

        }



        public static Sire SelectSireByID(int vwaID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

            return db.GetContext().Sires.Where(j => j.SireID == vwaID && j.Active == true).FirstOrDefault();

        }


        public static List<Sire> SearchSires(string search)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            List<Sire> lst = db.GetContext().Sires.Where(j => j.Active == true && j.SireName.Contains(search)).ToList();
            return lst;

        }
    }
}
