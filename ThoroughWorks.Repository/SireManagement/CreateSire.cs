﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF.UnitWork;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.SireManagement
{
    public class CreateSire
    {
        public Sire Do(Sire vw)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

            db.GetContext().Sires.Add(vw);
            db.SaveChanges();

            return vw;
        }
    }
}
