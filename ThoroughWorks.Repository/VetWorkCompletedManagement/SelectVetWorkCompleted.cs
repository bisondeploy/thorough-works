﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF.UnitWork;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.VetWorkCompletedManagement
{
    public class SelectVetWorkCompleted
    {
        //public static List<VetWorkCompleted> SelectAllVetWorkByVetWorkID(int VetWorkID)
        //{
        //    UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();


        //    return db.GetContext().VetWorkCompleteds.Where(j => j.VetWorkID == VetWorkID ).ToList();


        //}


        public static List<VetWorkCompleted> SelectAllVetWorkOptionsByVetWorkID(int SubscriberID, int VetWorkID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            List<VetWorkCompleted> lst = new List<VetWorkCompleted>();
             
            if (db.GetContext().VetWorkAttributes.Any(x => x.Active))
            {

                lst =
                            (from a in db.GetContext().VetWorkAttributes.Where(t => t.SubscriberID == SubscriberID && t.Active).DefaultIfEmpty()
                             join c in db.GetContext().VetWorkCompleteds.Where(w => w.VetWorkID == VetWorkID).DefaultIfEmpty() on a.VetWorkAttributeID equals c.VetWorkAttributeID into joinedData
                             from joinDetail in joinedData.DefaultIfEmpty()
                             select new { VetWorkCompletedID = (joinDetail == null ? 0 : joinDetail.VetWorkCompletedID), VetWorkID = (joinDetail == null ? 0 : joinDetail.VetWorkID), VetWorkAttributeID = a.VetWorkAttributeID, AttributeValue = (joinDetail == null ? String.Empty : joinDetail.AttributeValue), VetWork = (joinDetail == null ? null : joinDetail.VetWork), VetWorkAttribute = a })
                            .AsEnumerable()
                            .Select(
                            x => new VetWorkCompleted
                                {
                                    VetWorkCompletedID = x.VetWorkCompletedID,
                                    VetWorkID = x.VetWorkID,
                                    VetWorkAttributeID = x.VetWorkAttributeID,
                                    AttributeValue = x.AttributeValue,
                                    VetWork = x.VetWork,
                                    VetWorkAttribute = x.VetWorkAttribute
                                }
                            ).ToList();
            }
            return lst;

            



        }

    }
}
