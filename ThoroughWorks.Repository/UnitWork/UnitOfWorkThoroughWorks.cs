﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using Entity;
using ThoroughWorks.Data;

namespace EF.UnitWork
{
  public class UnitOfWorkThoroughWorks : UnitOfWork<ThoroughWorksEntities, DatabaseFactory<ThoroughWorksEntities>>
  {
    public UnitOfWorkThoroughWorks()
      : base(new DatabaseFactory<ThoroughWorksEntities>())
    {

    }

    public override int SaveChanges()
    {
      return DataContext.SaveChanges();
    }
    public ThoroughWorksEntities GetContext()
    {
      return DataContext;
    }
  }
}
