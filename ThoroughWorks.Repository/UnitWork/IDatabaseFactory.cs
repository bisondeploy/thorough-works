﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EF.UnitWork
{
    public interface IDatabaseFactory<out T> where T : class
    {
        T Get();
    }
}
