﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Linq.Expressions;
using System.Data.Entity;
using ThoroughWorks.Logic.UnitWork;
//using Entity.RepositoryManagement;

namespace EF.UnitWork
{
    public abstract class BaseRepository<T, TU> : Disposable
        where T : class
        where TU : DbContext, IDisposable, new()
    {
        #region Fields

        private TU context;

        #endregion

        #region Constructors
        protected BaseRepository()
        {

        }
        protected BaseRepository(IDatabaseFactory<TU> databaseFactory)
        {
            DatabaseFactory = databaseFactory;
        }

        protected BaseRepository(TU dbcontext)
        {
            context = dbcontext ;
        }

        protected void InitializeContext(IDatabaseFactory<TU> databaseFactory)
        {
            DatabaseFactory = databaseFactory;
        }

        #endregion

        #region Properties

        protected IDbSet<T> DbSet
        {
            get
            {
                return Context.Set<T>();
            }
        }

        protected IDatabaseFactory<TU> DatabaseFactory
        {
            get;
            private set;
        }

        protected TU Context
        {
            get { return context ?? (context = DatabaseFactory.Get()); }
        }

        #endregion

        protected  virtual IQueryable<T> SelectAll()
        {
            return DbSet.AsQueryable();
        }

        protected virtual IQueryable<T> Filter(Expression<Func<T, bool>> predicate)
        {
            return DbSet.Where(predicate).AsQueryable();
        }

        protected virtual IQueryable<T> Filter(Expression<Func<T, bool>> filter, out int total, int index = 0, int size = 10)
        {
            int skipCount = index * size;
            var resetSet = filter != null ? DbSet.Where(filter).AsQueryable() : DbSet.AsQueryable();
            resetSet = skipCount == 0 ? resetSet.Take(size) : resetSet.Skip(skipCount).Take(size);
            total = resetSet.Count();
            return resetSet.AsQueryable();
        }

        protected virtual bool Contains(Expression<Func<T, bool>> predicate)
        {
            return DbSet.Count(predicate) > 0;
        }

        protected virtual T Find(params object[] keys)
        {
            return DbSet.Find(keys);
        }

        protected virtual T Find(Expression<Func<T, bool>> predicate)
        {
            return DbSet.FirstOrDefault(predicate);
        }

        protected virtual T Create(T T)
        {
            var newEntry = DbSet.Add(T);
            return newEntry;
        }

        protected virtual int Count
        {
            get
            {
                return DbSet.Count();
            }
        }

        protected virtual int Delete(T T)
        {
            if (T is ILogicalDelete)
            {
                ((ILogicalDelete)T).Active = true;
                Update(T);
            }
            else
                DbSet.Remove(T);
            
            return 0;
        }

        protected virtual int Delete(IEnumerable<T> T)
        {
            foreach (T item in T)
            {
                DbSet.Remove(item);
            }
            return 0;
        }

        protected virtual int Update(T T)
        {
            DbSet.Attach(T);
            Context.Entry(T).State = System.Data.Entity.EntityState.Modified;
            return 0;
        }

        protected virtual int Delete(Expression<Func<T, bool>> predicate)
        {
            var objects = Filter(predicate);
            foreach (var obj in objects)
                DbSet.Remove(obj);
            return 0;
        }

        protected override void DisposeCore()
        {
            if (context != null)
            {
                context.Dispose();
                context = null;
            }
        }
    }
}
