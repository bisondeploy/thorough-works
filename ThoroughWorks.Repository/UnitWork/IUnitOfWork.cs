﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Entity;

namespace EF.UnitWork
{
    public interface IUnitOfWork : IDisposable
    {
        
        int SaveChanges();
    }
}
