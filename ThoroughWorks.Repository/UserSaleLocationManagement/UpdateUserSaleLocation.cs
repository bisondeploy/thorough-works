﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;
using RefactorThis.GraphDiff;
namespace ThoroughWorks.Repository.UserSaleLocationManagement
{
    public class UpdateUserSaleLocation
    {
        /// <summary>
        /// update user sale location
        /// </summary>
        /// <param name="usersalelocation"></param>
        /// <returns></returns>
        public UserSaleLocation Do(UserSaleLocation usersalelocation)
        {
            UserSaleLocation oldData = SelectUserSaleLocation.SelectUserSaleLocationByID(usersalelocation.UserSaleLocationID);

            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            usersalelocation.CreatedDate = oldData.CreatedDate;
            usersalelocation.CreatedBy = oldData.CreatedBy;
            db.GetContext().UpdateGraph(usersalelocation);
            db.GetContext().SaveChanges();
            return usersalelocation;
        }

        public static void UpdateComments(int userHorseSaleID, string comment)
        {
          UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

          var ush = db.GetContext().UserHorseSales.FirstOrDefault(x => x.UserHorseSaleID == userHorseSaleID);
          ush.Comments = comment;
          db.GetContext().SaveChanges();
        }
    }
}
