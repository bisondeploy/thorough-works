﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using ThoroughWorks.Data;
using ThoroughWorks.Models;

namespace ThoroughWorks.Repository.UserSaleLocationManagement
{
  public static class SelectUserSaleLocation
  {
    #region "Select methods"
    /// <summary>
    /// select allUserSaleLocation
    /// </summary>
    /// <returns></returns>
    public static List<UserSaleLocation> SelectAllUserSaleLocation(int SubscriberID)
    {
      UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
      List<UserSaleLocation> lstUserSaleLocation = db.GetContext().UserSaleLocations.Include("UserHorseSales").AsQueryable().Where(j => j.Active == true && j.SubscriberID == SubscriberID).ToList();

      return lstUserSaleLocation;

    }

    //public List<UserSaleLocation> LoadCompleteUserSaleLocation()
    //{
    //    UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
    //    db.GetContext().Configuration.LazyLoadingEnabled = true;
    //    db.GetContext().Configuration.ProxyCreationEnabled = true;
    //    List<UserSaleLocation> lstUserSaleLocation = db.GetContext().UserSaleLocations.Where(j => j.Active == true).ToList();

    //    return lstUserSaleLocation;

    //}


    /// <summary>
    /// select user sales location by id
    /// </summary>
    /// <param name="ID">int id</param>
    /// <returns></returns>
    public static UserSaleLocation SelectUserSaleLocationByID(int ID)
    {
      UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
      UserSaleLocation usersalelocation = db.GetContext().UserSaleLocations.Include("UserHorseSales").Include("UserDetail").AsQueryable().Where(j => j.UserSaleLocationID == ID).FirstOrDefault();
      return usersalelocation;

    }

    /// <summary>
    /// select user by location id with lazy loading load all data
    /// </summary>
    /// <param name="ID"></param>
    /// <returns></returns>
    public static UserSaleLocation SelectUserSaleLocationByID_LazyLoading(int ID)
    {
      UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
      db.GetContext().Configuration.LazyLoadingEnabled = true;
      db.GetContext().Configuration.ProxyCreationEnabled = true;
      UserSaleLocation usersalelocation = db.GetContext().UserSaleLocations.AsQueryable().Where(j => j.UserSaleLocationID == ID).FirstOrDefault();
      return usersalelocation;

    }

    ///// <summary>
    ///// laoding all relational data
    ///// </summary>
    ///// <returns></returns>
    //public List<UserSaleLocation> SelectUserSaleLocation_LazyLoading()
    //{
    //    UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
    //    db.GetContext().Configuration.LazyLoadingEnabled = true;
    //    db.GetContext().Configuration.ProxyCreationEnabled = true;
    //    List<UserSaleLocation> usersalelocation = db.GetContext().UserSaleLocations.AsQueryable().Where(j => j.Active == true).ToList();
    //    return usersalelocation;

    //}

    /// <summary>
    /// sales location for dashboard
    /// </summary>
    /// <returns></returns>
    public static List<UserSaleLocation_M> SelectUserSalesLocationForDashboard(int SubscriberID)
    {

      UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
      List<UserSaleLocation> lstUserSaleLocation = db.GetContext().UserSaleLocations.Where(x => x.SubscriberID == SubscriberID && x.Active == true).ToList();

      //lstUserSaleLocation = (from saleslocation in lstUserSaleLocation
      //                       select saleslocation).ToList();

      var openSales = lstUserSaleLocation
                          .Where(x => x.StartDate.HasValue && (x.StartDate ?? DateTime.Today).AddDays(-7) <= DateTime.Now
                                      && ((x.EndDate ?? DateTime.Now).AddDays(7) >= DateTime.Now))
                          .Select(x => new UserSaleLocation_M
                                                 {
                                                   SaleName = x.SaleName,
                                                   UserID = x.UserID,
                                                   Location = x.Location,
                                                   StartDate = x.StartDate,
                                                   EndDate = x.EndDate,
                                                   NumberSessions = x.NumberSessions,
                                                   UserSaleLocationID = x.UserSaleLocationID
                                                 }).ToList();
      return openSales;
    }

    /// <summary>
    /// Loads Statistics
    /// </summary>
    /// <returns></returns>
    public static SaleStatisticsModel SelectStatistics(int LocationID, int OwnerID)
    {
      UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
      var location = db.GetContext().UserSaleLocations.Include("Cards").Include("UserHorseSales").Include("UserHorseSales.Horse").Include("Cards.CardHorses").FirstOrDefault(x => x.UserSaleLocationID == LocationID);

      var model = new SaleStatisticsModel()
      {
        SaleID = location.UserSaleLocationID,
        SaleName = location.SaleName,
        UserID = location.UserID,
        UserSaleLocationID = location.UserSaleLocationID,
        RegisteredCards = location.Cards.Where(c => c.Active && (OwnerID == 0 || c.CardHorses.Any(x => x.Horse.HorseOwners.Any(h => h.OwnerID == OwnerID) )) ).Count(),
        Horses = location.UserHorseSales.Where(x => OwnerID == 0 || x.Horse.HorseOwners.Any(h => h.OwnerID == OwnerID)).Count(), 
      };
      var viewList = new List<SaleHorseViews>();
      foreach (var horse in location.UserHorseSales.Where(x => OwnerID == 0 || x.Horse.HorseOwners.Any(h => h.OwnerID == OwnerID) ).ToList())
      {
        var view = new SaleHorseViews()
        {
          HorseID = horse.HorseID,
          HorseName = horse.Horse.Name,
          LotNumber = horse.LotNumber,
        };
        view.Views = location.Cards.Where(c => c.Active).Count(x => x.CardHorses.Any(y => y.HorseID == horse.HorseID));
        //if (view.Views > 0)
        viewList.Add(view);
      }

      model.HorseViews = viewList.AsEnumerable();

      return model;

    }

    /// <summary>
    /// Loads Statistics
    /// </summary>
    /// <returns></returns>
    public static List<CardHorseViewerModel> SelectSaleHorseViewers(int SaleID, int HorseID)
    {
      UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

      var viewers = db.GetContext().Cards
                                      .Include("CardHorses")
                                      .Where(x => x.Active
                                                  && x.UserSaleLocationID == SaleID
                                                  && x.CardHorses.Any(y => y.HorseID == HorseID))
                                      .Select(x => String.IsNullOrEmpty(x.PersonName) ? "Unknown (Ref: " + x.CardUID + ")" : x.PersonName.ToUpper())
                                      .Distinct()
                                      .ToList();

      var list = new List<CardHorseViewerModel>();
      foreach (var viewer in viewers)
      {
        var viewerViews = db.GetContext().Cards
                                        .Include("CardHorses")
                                        .Where(x => x.Active
                                                    && x.UserSaleLocationID == SaleID
                                                    && x.CardHorses.Any(y => y.HorseID == HorseID)
                                                    && (String.IsNullOrEmpty(x.PersonName) ? "Unknown (Ref: " + x.CardUID + ")" : x.PersonName.ToUpper()) == viewer)
                                        .GroupBy(x => System.Data.Entity.DbFunctions.TruncateTime(x.InspectionDate))
                                        .Select(x => new ViewsByViewerDateModel()
                                                {
                                                  Date = x.Key.Value,
                                                  Views = x.Count(y => System.Data.Entity.DbFunctions.TruncateTime(y.InspectionDate) == x.Key.Value)
                                                })
                                        .ToList();

        list.Add(new CardHorseViewerModel()
        {
          ViewerName = viewer,
          ViewList = viewerViews,
        });

      }
      return list;
    }

    public static List<Horse> SelectHorsesOnSaleByOwnerID(int userSaleLocationID, int ownerID)
    {
      UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

      var query = from o in db.GetContext().Owners
                  join ho in db.GetContext().HorseOwners on o.OwnerID equals ho.OwnerID
                  join h in db.GetContext().Horses on ho.HorseID equals h.HorseID
                 join uhs in db.GetContext().UserHorseSales on h.HorseID equals uhs.HorseID
                 where o.OwnerID == ownerID && uhs.UserSaleLocationID == userSaleLocationID
                 select h;

      return query.ToList();
    }

    #endregion
  }
}
