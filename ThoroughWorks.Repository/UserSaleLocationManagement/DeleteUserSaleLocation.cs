﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.UserSaleLocationManagement
{
    public class DeleteUserSaleLocation
    {
        /// <summary>
        /// logical delete of the record
        /// </summary>
        /// <param name="userdetail"></param>
        public UserSaleLocation Do(int ID)
        {
            //get latest
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            UserSaleLocation usersalelocation = db.GetContext().UserSaleLocations.Where(j => j.UserSaleLocationID == ID).FirstOrDefault();

            //set active false
            usersalelocation.Active = false;

            //update
            db.GetContext().UserSaleLocations.Attach(usersalelocation);
            db.GetContext().Entry(usersalelocation).State = EntityState.Modified;
            db.SaveChanges();
            return usersalelocation;
        } 
    }
}
