﻿using EF.UnitWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.UserSaleLocationManagement
{
    public class CreateUserSaleLocation
    {
        /// <summary>
        /// insert record method
        /// </summary>
        /// <param name="usersalelocation">object of UserSaleLocation</param>
        /// <returns></returns>
        public UserSaleLocation Do(UserSaleLocation usersalelocation)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            db.GetContext().UserSaleLocations.Add(usersalelocation);
            db.SaveChanges();

            return usersalelocation;
        } 
    }
}
