﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF.UnitWork;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.ViewerManagement
{
    public class DeleteViewer
    {
        public Viewer Do(int Viewerid)
        {
            //get latest
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            Viewer vw = db.GetContext().Viewers.Where(j => j.ViewerID == Viewerid).FirstOrDefault();

            //set active false
            vw.Active = false;

            //update
            db.GetContext().Viewers.Attach(vw);
            db.GetContext().Entry(vw).State = EntityState.Modified;


            db.SaveChanges();
            return vw;
        } 
    }
}
