﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF.UnitWork;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.ViewerManagement
{
    public class SelectViewer
    {
        public static List<Viewer> SelectAllViewers()
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            List<Viewer> lst = db.GetContext().Viewers.Where(j => j.Active == true).OrderBy(x => x.ViewerName).ToList();
            return lst;

        }



        public static Viewer SelectViewerByID(int vwaID)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

            return db.GetContext().Viewers.Where(j => j.ViewerID == vwaID && j.Active == true).FirstOrDefault();

        }


        public static List<Viewer> SearchViewers(string search)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            List<Viewer> lst = db.GetContext().Viewers.Where(j => j.Active == true && j.ViewerName.Contains(search)).ToList();
            return lst;

        }
    }
}
