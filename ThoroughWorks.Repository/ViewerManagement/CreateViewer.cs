﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF.UnitWork;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.ViewerManagement
{
    public class CreateViewer
    {
        public Viewer Do(Viewer vw)
        {
            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();

            db.GetContext().Viewers.Add(vw);
            db.SaveChanges();

            return vw;
        }
    }
}
