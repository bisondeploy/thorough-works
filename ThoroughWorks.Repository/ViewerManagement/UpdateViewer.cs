﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EF.UnitWork;
using RefactorThis.GraphDiff;
using ThoroughWorks.Data;

namespace ThoroughWorks.Repository.ViewerManagement
{
    public class UpdateViewer
    {
        public Viewer Do(Viewer updatedvw)
        {

            Viewer oldvw = SelectViewer.SelectViewerByID(updatedvw.ViewerID);

            UnitOfWorkThoroughWorks db = new UnitOfWorkThoroughWorks();
            oldvw.ViewerName = updatedvw.ViewerName;

            oldvw.UpdatedDate = updatedvw.UpdatedDate;
            oldvw.UpdatedBy = updatedvw.UpdatedBy;


            try
            {
                db.GetContext().UpdateGraph(oldvw, map => map);
                db.SaveChanges();

                db.GetContext().SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw dbEx;
            }
            return oldvw;
        }
    }
}
