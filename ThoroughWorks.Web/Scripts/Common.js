﻿function OpenModel(url, modelSelection) {
    $.ajax({
        url: url,
        context: document.body,
        success: function (data) {
            $(modelSelection).find('.tw-panel-body').html(data);
            $(this).addClass("done");
            $(modelSelection).modal('show');

        },
        error: function (err) {
            alert(err);
        }
    });
}

function CloseModel(modelSelection) {

    $(modelSelection).modal('hide');
}

function getRequest(url, selector) {
    showloading();
    $.ajax({
        url: url,
        context: document.body,
        success: function (data) {
            $(selector).html(data);
            hideloading();
        },
        error: function (err) {
            alert(err);
            hideloading();
        }
    });
}

function getRequestNoLoading(url, selector) {
    $.ajax({
        url: url,
        context: document.body,
        success: function (data) {
            $(selector).html(data);
        },
        error: function (err) {
            alert(err);
        }
    });
}


function changePage(obj)
{
    //get variable from attributes
    var url = $(obj).attr('ajax-post-url');
    var controlreplace = $(obj).attr('ajax-update-control');
    var formpost = $(obj).attr('ajax-form');
    var beforefunction = $(obj).attr('ajax-before-function');
    var errorfunction = $(obj).attr('error-function');
    var successfunction = $(obj).attr('success-function');
    var ajaxtype = $(obj).attr('ajax-type');
    var updateprogress = $(obj).attr("ajax-update-progress");
    $('#Page').val($(obj).attr('page'));

    //showupdate panel
    if ($.trim(updateprogress) != '') {
        $(updateprogress).css('display', 'inline');
    }
    if ($.trim(beforefunction) != '') {
        var bcallfunction = beforefunction.split(',', 1)[0];
        var bcallargument = beforefunction.split(',').slice(1);
        window[bcallfunction].apply(obj, bcallargument);
    }

    //ajax post
    showloading();
    $.post(url, $(formpost).serialize(), function (data, status) {
        //debugger;
        if (ajaxtype == 'append') {
            $(controlreplace).append(data);
        }
        else {
            $(controlreplace).html(data);
        }

        if ($.trim(successfunction) != '') {
            var bsuccessfunction = successfunction.split(',', 1)[0];
            var bsuccessfunctionargument = successfunction.split(',').slice(1);
            window[bsuccessfunction].apply(obj, bsuccessfunctionargument);
        }

        if ($.trim(updateprogress) != '')
            $(updateprogress).hide();

        hideloading();
        Reloadables();

    }).error(function (error, status) {
        if ($.trim(updateprogress) != '')
            $(updateprogress).hide();

        hideloading();
    });

    return false;
}

function initPlugins() {
    $.fn.multiselect && $('.multiselect').multiselect();
    $.fn.datepicker && $(".date-picker").datepicker({ dateFormat: 'dd/mm/yyyy' });
    $.fn.fileInput && $("input[type='file']").fileInput();
    $.fn.select2 && $(".drp").select2();
    
}


function showloading() {
    if ($('body').find('loadingPanel').length == 0) {
        
        $('body').append("<div class='loadingPanel' style='display:block;'>&nbsp;<div class='loadingimage'>&nbsp;<div><div>")
    }
    else {
        $('.loadingPanel').css('display', 'block');
    }
}

function hideloading() {
    $('.loadingPanel').css('display', 'none');
}

$(function () {
    $('.DecimalTextBox').on('keypress', function (e) {
        alert('hi');
        var x = (e.which) ? e.which : e.keyCode
        if ((x < 48 || x > 57) && x != 0 && x != 8 && x != 9 && x != 46)
            return false;
        if (x == 46) {
            if ($(this).value.length <= 0 || $(this).value.indexOf(".") >= 0) {
                return false;
            }
        }
        return true;
    });
});
function onlyPositiveNumbersWithDecimal(obj, evt) {
    var x = (evt.which) ? evt.which : event.keyCode
    if ((x < 48 || x > 57) && x != 0 && x != 8 && x != 9 && x != 46)
        return false;
    if (x == 46) {
        if (obj.value.length <= 0 || obj.value.indexOf(".") >= 0) {
            return false;
        }
    }
    return true;
}
//$(document).ready(function () {
//    $("[ajax-custom='true']").on('click', function (event) {
//        alert('hi');
//        event.preventDefault();
//        //get variable from attributes
//        var url = $(this).attr('ajax-post-url');
//        var controlreplace = $(this).attr('ajax-update-control');
//        var formpost = $(this).attr('ajax-form');
//        var beforefunction = $(this).attr('ajax-before-function');
//        var errorfunction = $(this).attr('error-function');
//        var successfunction = $(this).attr('success-function');
//        var ajaxtype = $(this).attr('ajax-type');
//        var updateprogress = $(this).attr("ajax-update-progress");

//        //showupdate panel
//        if ($.trim(updateprogress) != '') {
//            $(updateprogress).css('display', 'inline');
//        }
//        if ($.trim(beforefunction) != '') {
//            var bcallfunction = beforefunction.split(',', 1)[0];
//            var bcallargument = beforefunction.split(',').slice(1);
//            window[bcallfunction].apply(this, bcallargument);
//        }

//        //ajax post
//        $.post(url, $(formpost).serialize(), function (data, status) {
//            //debugger;
//            if (ajaxtype == 'append') {
//                $(controlreplace).append(data);
//            }
//            else {
//                $(controlreplace).html(data);
//            }

//            if ($.trim(successfunction) != '') {
//                var bsuccessfunction = successfunction.split(',', 1)[0];
//                var bsuccessfunctionargument = successfunction.split(',').slice(1);
//                window[bsuccessfunction].apply(this, bsuccessfunctionargument);
//            }

//            if ($.trim(updateprogress) != '')
//                $(updateprogress).hide();

//        }).error(function (error, status) {
//            if ($.trim(updateprogress) != '')
//                $(updateprogress).hide();
//        });

//        return false;
//    });
//});

function mainfunc(func) {
    this[func].apply(this, Array.prototype.slice.call(arguments, 1));
}

