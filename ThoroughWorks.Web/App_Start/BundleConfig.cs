﻿using System.Web;
using System.Web.Optimization;

namespace ThoroughWorks.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            /* Styles */
            bundles.Add(new StyleBundle("~/Content/Basic").Include(
                   "~/Content/beyond-admin/css/bootstrap.min.css",
                   "~/Content/beyond-admin/css/font-awesome.min.css"));

            bundles.Add(new StyleBundle("~/Content/BeyondTheme").Include(
                   "~/Content/beyond-admin/css/beyond.min.css",
                   "~/Content/beyond-admin/css/demo.min.css",
                   "~/Content/beyond-admin/css/typicons.min.css",
                   "~/Content/beyond-admin/css/animate.min.css",
                   "~/Content/beyond-admin/css/dataTables.bootstrap.css"
                //"~/Content/Themes/Admin/table.css",
                //"~/Content/Themes/Admin/mws-theme.css",
                //"~/Content/Themes/Admin/icol16.css",
                //"~/Content/Themes/Admin/icol32.css",
                //"~/Content/Themes/Admin/style.css"
                   ));

            bundles.Add(new StyleBundle("~/Content/Customisations").Include(
                    "~/Content/Site.css"));

            /* Scripts */
            bundles.Add(new ScriptBundle("~/Scripts/Jquery").Include(
               "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/Scripts/Common").Include(
                   "~/Scripts/Common.js"));

            bundles.Add(new ScriptBundle("~/Scripts/Bootstrap").Include(
               "~/Content/beyond-admin/js/bootstrap.min.js",
               "~/Content/beyond-admin/js/skins.min.js",
               "~/Content/beyond-admin/js/textarea/jquery.autosize.min.js",
               "~/Content/beyond-admin/js/jquery-ui-1.10.4.custom.min.js"));

            bundles.Add(new ScriptBundle("~/Scripts/BeyondTheme").Include(
               "~/Content/beyond-admin/js/beyond.min.js"));

            bundles.Add(new ScriptBundle("~/Scripts/DatePicker").Include(
                "~/Content/beyond-admin/js/datetime/bootstrap-datepicker.js"));

            bundles.Add(new ScriptBundle("~/Scripts/Select2List").Include(
               "~/Content/beyond-admin/js/select2/select2.js"));

            //bundles.Add(new ScriptBundle("~/flot").Include(
            //    "~/Scripts/flot/jquery.flot.min.js",
            //    "~/Scripts/flot/plugins/jquery.flot.tooltip.min.js",
            //    "~/Scripts/flot/plugins/jquery.flot.axislabels.js",
            //    "~/Scripts/flot/plugins/jshashtable-2.1.js",
            //    "~/Scripts/flot/plugins/jquery.flot.time.js",                
            //    "~/Scripts/flot/plugins/jquery.numberformatter-1.2.3.min.js",
            //    "~/Scripts/flot/plugins/jquery.flot.symbol.js"
            //    ));


            bundles.Add(new ScriptBundle("~/Scripts/JqueryUI").Include(
                "~/Scripts/jquery-ui-1.9.2.min.js",
                "~/Scripts/jquery-ui.custom.min.js",
                "~/Scripts/jquery.ui.touch-punch.min.js"
                ));

            bundles.Add(new ScriptBundle("~/Bootstrap").Include(
                "~/Scripts/bootstrap.min.js"
                ));

            bundles.Add(new ScriptBundle("~/tw").Include(
                "~/Scripts/tw.js"
                ));
            bundles.Add(new ScriptBundle("~/Validation").Include(
              "~/Scripts/jquery.validate.js",
              "~/Scripts/jquery.validate.unobtrusive.js",
                "~/Content/beyond-admin/js/validation/bootstrapValidator.min.js"
                ));

            bundles.Add(new ScriptBundle("~/lightbox").Include(
                 "~/Scripts/lightbox-2.6.min.js"
                ));

            bundles.Add(new StyleBundle("~/lightbox-css").Include(
                 "~/Content/lightbox/css/lightbox.css"
                ));

        }
    }
}