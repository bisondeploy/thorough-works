﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Http;

using System.Text;
using ThoroughWorks.Web.Models.Helpers;
using CommunicationsManager;
using System.Configuration;
using System.Diagnostics;

namespace ThoroughWorks.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            

            EO.Pdf.Runtime.AddLicense(
                "L71GgaSxy5914+30EO2s3OnP566l4Of2GfKe3MKetZ9Zl6TNDOul5vvPuIlZ" +
                "l6Sxy59Zl8DyD+NZ6/0BELxbvNO/++OfmaQHEPGs4PP/6KFtpbSzy653hI6x" +
                "y59Zs7PyF+uo7sKetZ9Zl6TNGvGd3PbaGeWol+jyH+R2mbXA3a9oqbTC3qFZ" +
                "7ekDHuio5cGz36FZpsKetZ9Zl6TNHuig5eUFIPGetdX81tSl7fEL8PF65cvF" +
                "JO5s4PMIEfZ2tMDAHuig5eUFIPGetZGb566l4Of2GfKetZGbdePt9BDtrNzC" +
                "nrWfWZekzRfonNzyBBDInbW4yt+4cqq7w963dabw+g7kp+rp2g8=");

        }


        protected void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs
            HttpContext context = HttpContext.Current;
            Exception ex = context.Server.GetLastError().GetBaseException();

            if (ex.Message.Contains("favicon"))
                return;

            if (!context.Request.Url.ToString().Contains("localhost:"))
            {
                // Send Email
                StringBuilder w = new StringBuilder();
                w.Append("<p>Log Entry : </p>");
                w.Append("<p><b>User ID: </b>" + User.Identity.Name + "</p>");
                w.Append("<p><b>Date/Time: </b>" + DateTime.Now.ToString("dd-MMM-yyyy hh:mm:ss tt") + "</p>");
                w.Append("<p><b>Source: </b>" + ex.Source + "</p>");
                w.Append("<p><b>Target Site: </b>" + ex.TargetSite.ToString() + "</p>");
                string err = "<p>Error in: " + HttpContext.Current.Request.Url.ToString() + " </p><p>Error Message:" + ex.Message + "</p>";
                w.Append(err);
                string stackTrace = "<p>Stack trace: </p>" + ex.StackTrace.ToString().Replace(Environment.NewLine, "<br />");
                w.Append(stackTrace);



                try
                {
                    var user = SessionHelper.DisplayName;

                    CommunicationManager.InsertIntoCommunicationQueue("Email",
                                    ConfigurationManager.AppSettings["CommunicationManager.DefaultMailUsername"],
                                  ConfigurationManager.AppSettings["ExceptionToAddress"],
                                  ConfigurationManager.AppSettings["ExceptionCCAddress"],
                                  null,  // BCC
                                  "ThoroughWorks Error",
                                  "An error has been found on " + HttpContext.Current.Request.Url.ToString() + "<br />" + w.ToString() + "<br /><br />This message was generated automatically. Please do not reply to this message.",
                                  user,
                                  SessionHelper.UserID,
                                  null);

                }
                catch (Exception exception)
                {
                    // do nothing
                    EventLog.WriteEntry("Fatal Error in ThoroughWorks Website : " + exception.ToString(), "Fatal Error in ThoroughWorks Website", EventLogEntryType.Error);
                }
            }
                
        }

    }

}
