﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcPaging;
using ThoroughWorks.Data;
using ThoroughWorks.Models;
using ThoroughWorks.Repository.VetWorkAttributeManagement;
using ThoroughWorks.Web.Models.Helpers;

namespace ThoroughWorks.Web.Controllers
{
    public class VetWorkAttributeController : Controller
    {
        //
        // GET: /VetWorkAttribute/

        public ActionResult Index()
        {
            return View(SearchVetWorkAttributes());
        }

        [HttpGet]
        public ActionResult Add(int? id, string ReturnUrl = "")
        {
            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Url.Action("Index", "VetWorkAttribute") : Server.UrlDecode(ReturnUrl);
            var attributes = SelectVetWorkAttribute.SelectAllVetWorkAttributes(SessionHelper.SubscriptionID);
            var newSequence = 1;
            if (attributes != null)
                if (attributes.Any())
                    newSequence = attributes.Max(x => x.Sequence) + 1;

            var model = new VetWorkAttribute_M()
            {
                SubscriberID = SessionHelper.SubscriptionID,
                Sequence = newSequence,
                Active = true,
            };
            return PartialView("AddEdit", model);
        }

        [HttpGet]
        public ActionResult Edit(int id, string ReturnUrl = "")
        {
            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Url.Action("Index", "VetWorkAttribute") : Server.UrlDecode(ReturnUrl);

            VetWorkAttribute entity = SelectVetWorkAttribute.SelectVetWorkAttributeByID(id);

            VetWorkAttribute_M horse_m = new VetWorkAttribute_M()
            {
                Active = entity.Active,
                VetWorkAttributeID = entity.VetWorkAttributeID,
                AttributeName = entity.AttributeName,
                SubscriberID = entity.SubscriberID,
                Sequence = entity.Sequence,
            };
            return View("AddEdit", horse_m);
        }

        [HttpGet]
        public ActionResult DeleteRecord(int EntityID)
        {
            DeleteVetWorkAttribute deleteentities = new DeleteVetWorkAttribute();
            deleteentities.Do(EntityID);

            return PartialView("Listing", SearchVetWorkAttributes());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveRecord(VetWorkAttribute_M entity_m, string ReturnUrl)
        {
            if (ModelState.IsValid)
            {
                VetWorkAttribute entity = new VetWorkAttribute()
                {
                    VetWorkAttributeID = entity_m.VetWorkAttributeID,
                    AttributeName = entity_m.AttributeName,
                    Active = true,
                    SubscriberID = entity_m.SubscriberID,
                    Sequence = entity_m.Sequence,
                };

                if (entity.VetWorkAttributeID == 0)
                {
                    CreateVetWorkAttribute createEntity = new CreateVetWorkAttribute();
                    entity.CreatedBy = SessionHelper.UserID;
                    entity.CreatedDate = DateTime.Now;
                    createEntity.Do(entity);
                }
                else
                {
                    UpdateVetWorkAttribute updateEntity = new UpdateVetWorkAttribute();
                    entity.UpdatedBy = SessionHelper.UserID;
                    entity.UpdatedDate = DateTime.Now;
                    updateEntity.Do(entity);
                }
                //List<VetWorkAttribute_M> entityPaged = SearchVetWorkAttributes();


                Session["Message"] = "Vet Work Attribute Saved Succesfully";
                return Redirect(ReturnUrl);
            }
            this.Response.StatusCode = 400;
            return PartialView("AddEdit", entity_m);
        }

        List<VetWorkAttribute_M> SearchVetWorkAttributes()
        {
            List<VetWorkAttribute> entitylist = SelectVetWorkAttribute.SelectAllVetWorkAttributes(SessionHelper.SubscriptionID);

            List<VetWorkAttribute_M> entityPagedL = (from vwa in entitylist
                                                      where vwa.Active
                                                      select new VetWorkAttribute_M
                                                      {
                                                          VetWorkAttributeID = vwa.VetWorkAttributeID,
                                                          Active = vwa.Active,
                                                          AttributeName = vwa.AttributeName,
                                                          SubscriberID = vwa.SubscriberID,
                                                          Sequence = vwa.Sequence,
                                                      }).ToList();



            //IPagedList<VetWorkAttribute_M> entityPaged = entityPagedL.ToPagedList(0, 10);

            return entityPagedL;
        }

        [HttpPost]
        public ActionResult AjaxListing(string SearchVetWorkAttribute, int? Page)
        {
            int currentPageIndex = Page.HasValue ? Page.Value - 1 : 0;

            List<VetWorkAttribute> lst = SelectVetWorkAttribute.SearchVetWorkAttributes(SessionHelper.SubscriptionID, SearchVetWorkAttribute);

            List<VetWorkAttribute_M> model = (from aa in lst
                                              select new VetWorkAttribute_M
                                        {
                                            Active = aa.Active,
                                            AttributeName = aa.AttributeName,
                                            VetWorkAttributeID = aa.VetWorkAttributeID,
                                            SubscriberID = aa.SubscriberID,
                                            Sequence = aa.Sequence,
                                        }).ToList();


            IPagedList<VetWorkAttribute_M> itemsPaged = model.ToPagedList(currentPageIndex, 10);
            return PartialView("Listing", itemsPaged);
        }


        [HttpPost]
        public ActionResult ReorderVetWork(List<VetWorkAttribute_M> model)
        {
            try
            {
                var save = new SaveAttributeSequence(model, SessionHelper.SubscriptionID, SessionHelper.UserID);
                save.Do();

                Session["Message"] = "Attribute Sequence Changed Successfully";

            }
            catch (Exception ex)
            {
                Session["Message"] = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                Session["MessageClass"] = "danger";
            }

            return RedirectToAction("Index");
        }

    }
}
