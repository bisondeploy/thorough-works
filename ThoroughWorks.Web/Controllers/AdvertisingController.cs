﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

using ThoroughWorks.Models;
using ThoroughWorks.Data;
using ThoroughWorks.Web.Models.Helpers;
using ThoroughWorks.Repository.AdvertisingManagement;

namespace ThoroughWorks.Web.Controllers
{
    [Authorize]
    public class AdvertisingController : Controller
    {
        //
        // GET: /Advertising/
        public ActionResult Index()
        {
            var model = SelectAdvertisingBanners.GetBannerList().OrderByDescending(x => x.DisplayedFrom).ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult New()
        {
            var model = new AdvertisingBanner_M();
            model.DisplayedFrom = DateTime.Today;
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model = SelectAdvertisingBanners.GetByIDForDisplay(id);
            return View(model);
        }

        [HttpPost]
        public bool DeleteRecord(int BannerID)
        {
            DeleteAdvertisingBanner delete = new DeleteAdvertisingBanner();
            delete.Do(BannerID);

            Session["Message"] = "Advertisement Removed Successfully";
            return true;
        }

        [HttpPost]
        public ActionResult New(AdvertisingBanner_M model)
        {
            if (ModelState.IsValid) //if valid model then update the record and show list
            {
                AdvertisingBanner banner = new AdvertisingBanner()
                {
                    Created = DateTime.Now,
                    CreatedBy = SessionHelper.UserID,
                    DisplayedFrom = model.DisplayedFrom,
                    DisplayedTo = model.DisplayedTo,
                    ImagePath = model.ImagePath,
                    Url = model.Url,
                    Name = model.Name,
                    Active = true,
                };

                banner.ImagePath = model.Image != null ? model.Image.FileName : banner.ImagePath;

                CreateAdvertisingBanner create = new CreateAdvertisingBanner();
                create.Do(banner);

                if (!Directory.Exists("~/files/Advertising"))
                    Directory.CreateDirectory(Server.MapPath("~/files/Advertising"));

                if (!Directory.Exists("~/files/Advertising/" + banner.ID))
                    Directory.CreateDirectory(Server.MapPath("~/files/Advertising/" + banner.ID));

                if (model.Image != null)
                {
                    model.Image.SaveAs(Server.MapPath("~/files/Advertising/" + banner.ID + "/" + model.Image.FileName));
                }
                Session["Message"] = "Advertisement Created Successfully";

                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(AdvertisingBanner_M model)
        {
            if (ModelState.IsValid) //if valid model then update the record and show list
            {
                AdvertisingBanner banner = new AdvertisingBanner()
                {
                    ID = model.ID,
                    LastUpdated= DateTime.Now,
                    LastUpdatedBy = SessionHelper.UserID,
                    DisplayedFrom = model.DisplayedFrom,
                    DisplayedTo = model.DisplayedTo,
                    ImagePath = model.ImagePath,
                    Url = model.Url,
                    Name = model.Name,
                    Active = true,
                };

                banner.ImagePath = model.Image != null ? model.Image.FileName : banner.ImagePath;

                    UpdateAdvertisingBanner update = new UpdateAdvertisingBanner ();
                    update.Do(banner);

                
                if (!Directory.Exists("~/files/Advertising"))
                    Directory.CreateDirectory(Server.MapPath("~/files/Advertising"));

                if (!Directory.Exists("~/files/Advertising/" + banner.ID))
                    Directory.CreateDirectory(Server.MapPath("~/files/Advertising/" + banner.ID));

                if (model.Image != null)
                    model.Image.SaveAs(Server.MapPath("~/files/Advertising/" + banner.ID + "/" + model.Image.FileName));

                Session["Message"] = "Advertisement Updated Successfully";

                return RedirectToAction("Index");
            }
            return View(model);
        }

    }
}