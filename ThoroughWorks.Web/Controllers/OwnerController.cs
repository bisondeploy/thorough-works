﻿using MvcPaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Routing;
using ThoroughWorks.Data;
using ThoroughWorks.Models;
using ThoroughWorks.Web.Models.Helpers;
using ThoroughWorks.Repository.OwnerManagement;

namespace ThoroughWorks.Web.Controllers
{
    [Authorize]
    public class OwnerController : Controller
    {
        #region "get methods"

        public ActionResult Index()
        {
            OwnerSearch ownersearch = (OwnerSearch)TempData["ownersearch"];
            ownersearch = ownersearch ?? new OwnerSearch();
            ViewBag.Search = ownersearch;
            IPagedList<Owner_M> ownerPaged = SelectOwnerFilterBased(ownersearch);
            return View(ownerPaged);
        }

        [HttpGet]
        public ActionResult Add()
        {
            return PartialView("AddEdit", new Owner_M() { SubscriberID = SessionHelper.SubscriptionID });
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            Owner owner = SelectOwners.SelectOwnerByID(id);

            Owner_M owner_m = new Owner_M()
            {
                OwnerID = owner.OwnerID,
                Name = owner.Name,
                EmailAddress = owner.EmailAddress,
                ContactNumber = owner.ContactNumber,
                Active = owner.Active,
                SubscriberID = owner.SubscriberID,
                OwnedHorses = owner.HorseOwners.Select(h => new Horse_M
                {
                    Active = h.Horse.Active,
                    Colour = h.Horse.Colour,
                    Comments = h.Horse.Comments,
                    Dam = h.Horse.Dam,
                    DateArrived = h.Horse.DateArrived,
                    DateSold = h.Horse.DateSold,
                    DOB = h.Horse.DOB,
                    HorseID = h.Horse.HorseID,
                    Name = h.Horse.Name,
                    Sex = h.Horse.Sex,
                    SireID = (int) h.Horse.SireID,
                    SireDisplay = h.Horse.Sire.SireName,
                    SubscriberID = h.Horse.SubscriberID,
                }).ToList(),
            };
            return View("AddEdit", owner_m);
        }

        [HttpGet]
        public ActionResult DeleteRecord(int ownerID)
        {
            DeleteOwner deleteowner = new DeleteOwner();
            deleteowner.Do(ownerID);

            IPagedList<Owner_M> ownersPaged = SelectOwnerFilterBased(new OwnerSearch());
            return PartialView("Listing", ownersPaged);
        }

        private IPagedList<Owner_M> SelectOwnerFilterBased(OwnerSearch search)
        {
            int currentPageIndex = search.Page.HasValue ? search.Page.Value - 1 : 0;
            List<Owner> owners = (from o in SelectOwners.SelectAllActiveOwners(SessionHelper.SubscriptionID)
                                  where ((String.IsNullOrWhiteSpace(search.SearchName) || Helpers.ConvertString(o.Name).ToLower().Contains(search.SearchName.ToLower())))
                                  select o).ToList();

            List<Owner_M> ownermodel = (from aa in owners
                                        select new Owner_M
                                            {
                                                OwnerID = aa.OwnerID,
                                                Name = aa.Name,
                                                EmailAddress = aa.EmailAddress,
                                                ContactNumber = aa.ContactNumber,
                                                Active = aa.Active,
                                                SubscriberID = aa.SubscriberID,
                                            }).ToList();
            IPagedList<Owner_M> ownerpage = ownermodel.OrderBy(j => j.OwnerID).ToPagedList(currentPageIndex, 10);
            return ownerpage;
        }

        #endregion

        #region "Post methods"

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveRecord(Owner_M owner_m, OwnerSearch search)
        {
            if (ModelState.IsValid) //if valid model then update the record and show list
            {
                Owner owner = new Owner()
                {
                    OwnerID = owner_m.OwnerID,
                    Name = owner_m.Name,
                    EmailAddress = owner_m.EmailAddress,
                    ContactNumber = owner_m.ContactNumber,
                    Active = owner_m.Active,
                    SubscriberID = owner_m.SubscriberID
                };

                if (owner.OwnerID == 0)
                {
                    CreateOwner createowner = new CreateOwner();
                    createowner.Do(owner);
                }
                else
                {
                    UpdateOwner updateowner = new UpdateOwner();
                    updateowner.Do(owner);
                }

                IPagedList<Owner_M> ownerPaged = SelectOwnerFilterBased(search);

                TempData["ownersearch"] = search;
                return RedirectToAction("Index");
            }
            this.Response.StatusCode = 400;
            return PartialView("AddEdit", owner_m);
        }

        [HttpPost]
        public ActionResult AjaxListing(OwnerSearch search)
        {
            IPagedList<Owner_M> ownerPaged = SelectOwnerFilterBased(search);
            return PartialView("Listing", ownerPaged);
        }

        [HttpPost]
        public ActionResult Validate(Owner_M owner_m)
        {
            bool valid = true;
            if (ModelState.IsValid)
            {
                valid = true;
                return Json(valid, JsonRequestBehavior.AllowGet);
            }
            else
            {
                this.Response.StatusCode = 400;
                return PartialView("AddEdit", owner_m);
            }
        }

        #endregion
    }
}