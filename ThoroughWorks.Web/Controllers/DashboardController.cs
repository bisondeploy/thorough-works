﻿using MvcPaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThoroughWorks.Data;
using ThoroughWorks.Repository.CardManagement;
using ThoroughWorks.Repository.SubscriberManagement;
using ThoroughWorks.Repository.HorseManagement;
using ThoroughWorks.Repository.UserManagement;
using ThoroughWorks.Repository.UserSaleLocationManagement;
using ThoroughWorks.Repository.ViewLogHistoryManagement;
using ThoroughWorks.Models;
using ThoroughWorks.Web.Models.Helpers;
using ThoroughWorks.Repository.VetWorkManagement;

namespace ThoroughWorks.Web.Controllers
{
  [Authorize]
  public class DashboardController : Controller
  {
    //
    // GET: /Dashboard/
    public ActionResult Index()
    {
      var model = new DashboardStatistics();

      //static counting set
      if (Helpers.isSuperAdmin())
      {
        // get super admin stats
        model.AdminSubscribers = SelectSubscriber.SelectAllSubscriber().Count; ;
        model.Horses = SelectHorses.SelectHorseCountForAllSubscribers();
      }
      else
      {
        var subscriber = SessionHelper.SubscriptionID;
        model.Horses = SelectHorses.SelectAllActiveHorse(subscriber, false).Count;
        model.SalesLocation = SelectUserSaleLocation.SelectAllUserSaleLocation(subscriber).ToList().Count;

        model.CurrentSaleLocations = SelectUserSaleLocation.SelectUserSalesLocationForDashboard(subscriber);
      }

      if (Helpers.isVet())
      {
          VetWorkSearch vwsearch = new VetWorkSearch();
          ViewBag.VetWorkSearch = vwsearch;
          vwsearch.Dt = DateTime.Now.Date;
          List<VetWork> list = null;
          if (Helpers.isAdmin())
              list = SelectVetWork.SelectAllVetWorkByDay(vwsearch.Dt);
          else
              list = SelectVetWork.SelectAllVetWorkByVetByDay(SessionHelper.UserID, vwsearch.Dt);

          model.VetWork = list.Select(vw => new VetWork_M
                                          {
                                              VetWorkID = vw.VetWorkID,
                                              HorseID = vw.HorseID,
                                              HorseName = vw.Horse.Name,
                                              VetID = vw.UserDetail.UserID,
                                              VetName = vw.UserDetail.FirstName + " " + vw.UserDetail.LastName,
                                              AgeCategory = vw.Horse.AgeCategory,
                                              VisitDate = vw.VisitDate,
                                              LastVisitDate = SelectVetWork.GetLastVisitDate(vw.HorseID, vw.VisitDate),
                                              NextVisitDate = SelectVetWork.SelectNextVisitDate(vw.HorseID, vw.VisitDate),
                                              NextInFor = SelectVetWork.SelectNextVisitDetails(vw.HorseID, vw.VisitDate).InFor,
                                              NextVisitID = SelectVetWork.SelectNextVisitDetails(vw.HorseID, vw.VisitDate).VetWorkID,
                                              InFor = vw.InFor,
                                              WorkCompleted = vw.WorkCompleted,
                                              Status = vw.Status
                                          }).OrderBy(vw => vw.VisitDate).ToList();
      }

      return View(model);
    }



    //public ActionResult VetWork(int? Page)
    //{
    //    int VetID = SessionHelper.UserID;
    //    int currentPageIndex = Page.HasValue ? Page.Value - 1 : 0;

    //    List<VetWork> vwl;
    //    vwl = SelectVetWork.SelectAllVetWorkByVetByDay(VetID, DateTime.Now.Date);


    //    List<VetWork_M> vetworkmodel = (from vw in vwl
    //                                    select new VetWork_M
    //                                    {
    //                                        VetWorkID = vw.VetWorkID,
    //                                        HorseID = vw.HorseID,
    //                                        HorseName = vw.Horse.Name,
    //                                        VetID = vw.UserDetail.UserID,
    //                                        VetName = vw.UserDetail.FirstName + " " + vw.UserDetail.LastName,
    //                                        AgeCategory = vw.AgeCategory,
    //                                        VisitDate = vw.VisitDate,
    //                                        LastVisitDate = SelectVetWork.GetLastVisitDate(vw.HorseID, vw.VisitDate),
    //                                        NextVisitDate = SelectVetWork.SelectNextVisitDate(vw.HorseID, vw.VisitDate),
    //                                        NextInFor = SelectVetWork.SelectNextVisitDetails(vw.HorseID, vw.VisitDate).InFor,
    //                                        NextVisitID = SelectVetWork.SelectNextVisitDetails(vw.HorseID, vw.VisitDate).VetWorkID,
    //                                        InFor = vw.InFor,
    //                                        WorkCompleted = vw.WorkCompleted,
    //                                        Status = vw.Status

    //                                    }).OrderBy(vw => vw.VisitDate).ToList();


    //    IPagedList<VetWork_M> vwpagedlist = vetworkmodel.ToPagedList(currentPageIndex, 10);
    //    return PartialView("VetWorkListing", vwpagedlist);
    //}

    [HttpGet]
    public ActionResult DeleteVetWorkRecord(int VetWorkID, int VetID)
    {

        DeleteVetWork deletevw = new DeleteVetWork();
        deletevw.Do(VetWorkID);


        return RedirectToAction("Index");

    }


    //#region "Supporing method"

    //private void SetUserHorseSale()
    //{
    //    SelectUserSaleLocation selectuserlocation = new SelectUserSaleLocation();
    //    List<UserSaleLocation> lst = selectuserlocation.SelectAllUserSaleLocation(SessionHelper.SubscriptionID);

    //    List<UserSaleLocation_M> locationmodel = (from aa in lst
    //                                              select new UserSaleLocation_M
    //                                              {
    //                                                  Active = aa.Active,
    //                                                  CreatedBy = aa.CreatedBy,
    //                                                  CreatedDate = aa.CreatedDate,
    //                                                  EndDate = aa.EndDate,
    //                                                  SaleName = aa.SaleName,
    //                                                  Location = aa.Location,
    //                                                  StartDate = aa.StartDate,
    //                                                  UpdatedBy = aa.UpdatedBy,
    //                                                  UpdatedDate = aa.UpdatedDate,
    //                                                  UserID = aa.UserID,
    //                                                  UserSaleLocationID = aa.UserSaleLocationID,
    //                                                  UserName = aa.UserDetail != null ? aa.UserDetail.UserName : "",
    //                                                  horses = (from hors in aa.UserHorseSales
    //                                                            select new UserHorseSale_M
    //                                                            {
    //                                                                HorseID = hors.HorseID,
    //                                                                Sequence = hors.Sequence,
    //                                                                UserHorseSaleID = hors.UserHorseSaleID,
    //                                                                UserSaleLocationID = hors.UserSaleLocationID
    //                                                            }).ToList()
    //                                              }).ToList();

    //    ViewBag.horsesales = locationmodel;
    //}

    //#endregion
  }
}