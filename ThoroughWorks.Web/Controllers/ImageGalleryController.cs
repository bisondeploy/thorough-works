﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace ThoroughWorks.Web.Controllers
{
    public class ImageGalleryController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult LightBoxPartial()
        {
            return PartialView();
        }
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public FileResult GetImageFile(string imagePath)
        {
            return File(Server.MapPath(imagePath), "image/jpeg");
        }
	}
}