﻿using MvcPaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThoroughWorks.Data;
using ThoroughWorks.Repository.UserManagement;
using ThoroughWorks.Models;
using ThoroughWorks.Web.Models.Helpers;

namespace ThoroughWorks.Web.Controllers
{
    [Authorize]
    public class UserManagementController : Controller
    {
        #region "get methods"

        public ActionResult Index()
        {

            IPagedList<UserDetail_M> userPaged = SelectUserFilterBased(new UserDetailSearch());
            return View(userPaged);
        }

        [HttpGet]
        public ActionResult Add(int? SubscriberID, string ReturnUrl = "")
        {
            var model = new UserDetail_M();
            if (SubscriberID.HasValue)
                model.SubscriberID = SubscriberID.Value;
            else
                model.SubscriberID = SessionHelper.SubscriptionID;

            model.SelectedRoles = new List<int>();
            if (SubscriberID == 0)
            { 
                // super admin
                model.SelectedRoles.Add(5);
            } 
            else
            {
                // normal user by default
                model.SelectedRoles.Add(2);
            }


            ViewBag.ReturnUrl = Url.Action("Index");
            if (!String.IsNullOrEmpty(ReturnUrl))
                ViewBag.ReturnUrl = Server.UrlDecode(ReturnUrl);
            return View("AddEdit", model);


        }

        [HttpGet]
        public ActionResult Edit(int UserID)
        {
            UserDetail userdetail = SelectUserDetails.SelectByUserID(UserID);

            UserDetail_M userdetail_m = new UserDetail_M()
            {
                UserID = userdetail.UserID,
                UserName = userdetail.UserName,
                EmailID = userdetail.EmailID,
                FirstName = userdetail.FirstName,
                Password = userdetail.Password,
                LastName = userdetail.LastName,
                PhoneNumber = userdetail.PhoneNumber,
                RoleMasterIDs = userdetail.UserRoles.Count > 0 ? (from bb in userdetail.UserRoles select Convert.ToString(bb.RoleMasterID)).ToArray() : new string[0],
                Active = true,
                SubscriberID = SessionHelper.SubscriptionID,
                EmailSignature = userdetail.EmailSignature
            };
            ViewBag.ReturnUrl = Url.Action("Index");
            return View("AddEdit", userdetail_m);
        }

        [HttpGet]
        public ActionResult DeleteRecord(int UserID)
        {
            DeleteUserDetails deleteuserDetail = new DeleteUserDetails();
            deleteuserDetail.Do(UserID);

            IPagedList<UserDetail_M> userPaged = SelectUserFilterBased(new UserDetailSearch());
            return PartialView("Listing", userPaged);
        }

        private IPagedList<UserDetail_M> SelectUserFilterBased(UserDetailSearch search)
        {
            int currentPageIndex = search.page.HasValue ? search.page.Value - 1 : 0;
            List<UserDetail> users = new List<UserDetail>();
            if (SessionHelper.SubscriptionID == 0)
            {
                users = (from usr in SelectUserDetails.SelectAdministrationUsers()
                         where ((String.IsNullOrWhiteSpace(search.SearchFirstName) || Helpers.ConvertString(usr.FirstName).ToLower().Contains(Helpers.ConvertString(search.SearchFirstName).ToLower()))
                         && (String.IsNullOrWhiteSpace(search.SearchLastName) || Helpers.ConvertString(usr.LastName).ToLower().Contains(Helpers.ConvertString(search.SearchLastName).ToLower())))
                         select usr).ToList();
            }
            else
            {
                users = (from usr in SelectUserDetails.SelectAllActiveUsers(SessionHelper.SubscriptionID)
                         where ((String.IsNullOrWhiteSpace(search.SearchFirstName) || Helpers.ConvertString(usr.FirstName).ToLower().Contains(Helpers.ConvertString(search.SearchFirstName).ToLower()))
                         && (String.IsNullOrWhiteSpace(search.SearchLastName) || Helpers.ConvertString(usr.LastName).ToLower().Contains(Helpers.ConvertString(search.SearchLastName).ToLower())))
                         select usr).ToList();
            }

            List<UserDetail_M> usermodel = (from aa in users
                                            select new UserDetail_M
                                            {
                                                Active = aa.Active,
                                                EmailID = aa.EmailID,
                                                FirstName = aa.FirstName,
                                                LastName = aa.LastName,
                                                Password = aa.Password,
                                                PhoneNumber = aa.PhoneNumber,
                                                RoleMasterIDs = aa.UserRoles.Count > 0 ? (from bb in aa.UserRoles select Convert.ToString(bb.RoleMasterID)).ToArray() : new string[0],
                                                RoleMasterName = aa.UserRoles.Count > 0 ? String.Join("<br />", (from bb in aa.UserRoles select Convert.ToString(bb.RoleMaster.RoleName)).ToArray()) : "",
                                                UserID = aa.UserID,
                                                UserName = aa.UserName,
                                                EmailSignature = aa.EmailSignature
                                            }).ToList();
            IPagedList<UserDetail_M> userPaged = usermodel.OrderBy(j => j.UserID).ToPagedList(currentPageIndex, 10);
            return userPaged;
        }
        #endregion

        #region "Post methods"
        [HttpPost]
        public ActionResult AjaxListing(UserDetailSearch search)
        {
            IPagedList<UserDetail_M> userPaged = SelectUserFilterBased(search);
            return PartialView("Listing", userPaged);
        }

        [HttpPost]
        public ActionResult Add(UserDetail_M userdetail_m, UserDetailSearch search, string ReturnUrl)
        {
            if (ModelState.IsValid) //if valid model then update the record and show list
            {
                try
                {
                    UserDetail userdetail = new UserDetail()
                    {
                        UserID = userdetail_m.UserID,
                        UserName = userdetail_m.UserName,
                        EmailID = userdetail_m.EmailID,
                        FirstName = userdetail_m.FirstName,
                        Password = userdetail_m.Password,
                        LastName = userdetail_m.LastName,
                        PhoneNumber = userdetail_m.PhoneNumber,
                        Active = true,
                        EmailSignature = userdetail_m.EmailSignature
                    };

                    //user roles
                    List<UserRole> userrole = new List<UserRole>();
                    if (userdetail_m.SelectedRoles != null)
                    {
                        foreach (int role in userdetail_m.SelectedRoles)
                        {
                            userrole.Add(new UserRole() { UserID = userdetail_m.UserID, RoleMasterID = role });
                        }
                    }
                    userdetail.UserRoles = userrole;

                    CreateUserDetails createuserdetail = new CreateUserDetails();
                    userdetail.CreatedBy = SessionHelper.UserID;
                    userdetail.CreatedDate = DateTime.Now;
                    createuserdetail.Do(userdetail, userdetail_m.SubscriberID);

                    Session["Message"] = "User Created Successfully";

                    if (!String.IsNullOrEmpty(ReturnUrl))
                        return Redirect(Server.UrlDecode(ReturnUrl));
                    else
                        return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                }
            }

            ViewBag.ReturnUrl = Url.Action("Index");
            if (!String.IsNullOrEmpty(ReturnUrl))
                ViewBag.ReturnUrl = Server.UrlDecode(ReturnUrl);

            return View("AddEdit", userdetail_m);
        }

        [HttpPost]
        public ActionResult Edit(UserDetail_M userdetail_m, UserDetailSearch search, string ReturnUrl)
        {
            if (String.IsNullOrEmpty(userdetail_m.Password))
                // remove password field
                ModelState.Remove("PassworD");

            if (ModelState.IsValid) //if valid model then update the record and show list
            {
                try
                {

                    UserDetail userdetail = new UserDetail()
                    {
                        UserID = userdetail_m.UserID,
                        UserName = userdetail_m.UserName,
                        EmailID = userdetail_m.EmailID,
                        FirstName = userdetail_m.FirstName,
                        Password = userdetail_m.Password,
                        LastName = userdetail_m.LastName,
                        PhoneNumber = userdetail_m.PhoneNumber,
                        Active = true,
                        EmailSignature = userdetail_m.EmailSignature
                    };

                    //user roles
                    List<UserRole> userrole = new List<UserRole>();
                    if (userdetail_m.SelectedRoles != null)
                    {
                        foreach (int role in userdetail_m.SelectedRoles)
                        {
                            userrole.Add(new UserRole() { UserID = userdetail_m.UserID, RoleMasterID = role });
                        }
                    }
                    userdetail.UserRoles = userrole;

                    UpdateUserDetails updatedetail = new UpdateUserDetails();
                    userdetail.UpdatedBy = SessionHelper.UserID;
                    userdetail.UpdatedDate = DateTime.Now;
                    updatedetail.Do(userdetail);

                    userdetail_m.RoleMasterIDs = userdetail_m.SelectedRoles.Select(x => x.ToString()).ToArray();

                    ViewBag.Message = "User Updated Successfully";
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                }
            }

            ViewBag.ReturnUrl = Url.Action("Index");
            return View("AddEdit", userdetail_m);
        }



        public ActionResult MyProfile(string ReturnUrl)
        {

            UserDetail userdetail = SelectUserDetails.SelectByUserID(SessionHelper.UserID);

            UserDetail_M userdetail_m = new UserDetail_M()
            {
                UserID = userdetail.UserID,
                UserName = userdetail.UserName,
                EmailID = userdetail.EmailID,
                FirstName = userdetail.FirstName,
                Password = userdetail.Password,
                LastName = userdetail.LastName,
                PhoneNumber = userdetail.PhoneNumber,
                RoleMasterIDs = userdetail.UserRoles.Count > 0 ? (from bb in userdetail.UserRoles select Convert.ToString(bb.RoleMasterID)).ToArray() : new string[0],
                Active = true,
                SubscriberID = SessionHelper.SubscriptionID,
                EmailSignature = userdetail.EmailSignature
            };

            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Url.Action("Index", "Dashboard") : Server.UrlDecode(ReturnUrl);
            return View("MyProfile", userdetail_m);
        }


        [HttpPost]
        public ActionResult MyProfile(UserDetail_M userdetail_m, string ReturnUrl)
        {
            if (String.IsNullOrEmpty(userdetail_m.Password))
                ModelState.Remove("PassworD");

            if (ModelState.IsValid) //if valid model then update the record and show list
            {
                try
                {
                    UserDetail userdetail = new UserDetail()
                    {
                        UserID = userdetail_m.UserID,
                        UserName = userdetail_m.UserName,
                        EmailID = userdetail_m.EmailID,
                        FirstName = userdetail_m.FirstName,
                        Password = userdetail_m.Password,
                        LastName = userdetail_m.LastName,
                        PhoneNumber = userdetail_m.PhoneNumber,
                        Active = true,
                        EmailSignature = userdetail_m.EmailSignature
                    };

                    //user roles - sames as what is already in the DB
                    userdetail.UserRoles = SelectUserDetails.SelectByUserID(userdetail_m.UserID).UserRoles;

                    UpdateUserDetails updatedetail = new UpdateUserDetails();
                    userdetail.UpdatedBy = SessionHelper.UserID;
                    userdetail.UpdatedDate = DateTime.Now;
                    updatedetail.Do(userdetail);

                    ViewBag.Message = "User Profile Updated Successfully";

                    userdetail_m.RoleMasterIDs = userdetail.UserRoles.Count > 0 ? (from bb in userdetail.UserRoles select Convert.ToString(bb.RoleMasterID)).ToArray() : new string[0];
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                }
            }

            return View("MyProfile", userdetail_m);
        }
        #endregion


        [HttpPost]
        public ActionResult SendSignatureImage(HttpPostedFileBase file)
        {
            

            // Store in subscriber files. Prefix filename with UserID_

            string relativePath = String.Format("~/UserFiles/{0}/{1}", SessionHelper.UserID.ToString(), file.FileName);

            string fullPath = Server.MapPath(relativePath);
            // Create folder if not exists
            System.IO.FileInfo fileCheck = new System.IO.FileInfo(fullPath);
            fileCheck.Directory.Create();

            file.SaveAs(Server.MapPath(relativePath));

            string savedFileUrl = String.Format("{0}://{1}:{2}{3}", Request.Url.Scheme,
                                                                        Request.Url.Host,
                                                                        Request.Url.Port,
                                                                        VirtualPathUtility.ToAbsolute(relativePath));

            return Json(savedFileUrl);

        }

    }
}