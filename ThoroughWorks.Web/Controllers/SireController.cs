﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcPaging;
using ThoroughWorks.Data;
using ThoroughWorks.Web.Models.Helpers;
using ThoroughWorks.Repository.SireManagement;
using ThoroughWorks.Models;

namespace ThoroughWorks.Web.Controllers
{
    public class SireController : Controller
    {
        public ActionResult Index()
        {
            IPagedList<Sire_M> list = SearchSires();
            return View(list);
        }

        [HttpGet]
        public ActionResult Add(int? id, string ReturnUrl = "")
        {
            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Url.Action("Index", "Sire") : Server.UrlDecode(ReturnUrl);
            var attributes = SelectSire.SelectAllSires();
            
            var model = new Sire_M()
            {
                Active = true,
            };
            return PartialView("AddEdit", model);
        }

        [HttpGet]
        public ActionResult Edit(int id, string ReturnUrl = "")
        {
            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Url.Action("Index", "Sire") : Server.UrlDecode(ReturnUrl);

            Sire entity = SelectSire.SelectSireByID(id);

            Sire_M horse_m = new Sire_M()
            {
                Active = entity.Active,
                SireID = entity.SireID,
                SireName = entity.SireName,

            };
            return View("AddEdit", horse_m);
        }

        [HttpGet]
        public ActionResult DeleteRecord(int EntityID)
        {


            DeleteSire deleteentities = new DeleteSire();
            deleteentities.Do(EntityID);

            return PartialView("Listing", SearchSires());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveRecord(Sire_M entity_m, string ReturnUrl)
        {
            if (ModelState.IsValid)
            {
                Sire entity = new Sire()
                {
                    SireID = entity_m.SireID,
                    SireName = entity_m.SireName,
                    Active = true,
                };

                if (entity.SireID == 0)
                {
                    CreateSire createEntity = new CreateSire();
                    entity.CreatedBy = SessionHelper.UserID;
                    entity.CreatedDate = DateTime.Now;
                    createEntity.Do(entity);
                }
                else
                {
                    UpdateSire updateEntity = new UpdateSire();
                    entity.UpdatedBy = SessionHelper.UserID;
                    entity.UpdatedDate = DateTime.Now;
                    updateEntity.Do(entity);
                }
                //List<Sire_M> entityPaged = SearchSires();


                Session["Message"] = "Sire Saved Succesfully";
                return Redirect(ReturnUrl);
            }
            this.Response.StatusCode = 400;
            return PartialView("AddEdit", entity_m);
        }

        IPagedList<Sire_M> SearchSires()
        {
            List<Sire> entitylist = SelectSire.SelectAllSires();

            List<Sire_M> entityPagedL = (from vwa in entitylist
                                                     where vwa.Active
                                                     select new Sire_M
                                                     {
                                                         SireID = vwa.SireID,
                                                         Active = vwa.Active,
                                                         SireName = vwa.SireName,
                                                         
                                                     }).ToList();



            IPagedList<Sire_M> entityPaged = entityPagedL.ToPagedList(0, 10);

            return entityPaged;
        }

        [HttpPost]
        public ActionResult AjaxListing(string SearchSire, int? Page)
        {
            int currentPageIndex = Page.HasValue ? Page.Value - 1 : 0;

            List<Sire> lst = SelectSire.SearchSires(SearchSire);

            List<Sire_M> model = (from aa in lst
                                              select new Sire_M
                                              {
                                                  Active = aa.Active,
                                                  SireName = aa.SireName,
                                                  SireID = aa.SireID,
                                              }).ToList();


            IPagedList<Sire_M> itemsPaged = model.ToPagedList(currentPageIndex, 10);
            return PartialView("Listing", itemsPaged);
        }


        
	}
}