﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ThoroughWorks.Data;
using ThoroughWorks.Models;
using ThoroughWorks.Repository.HorseManagement;
using ThoroughWorks.Web.Models.Helpers;


namespace ThoroughWorks.Web.Controllers
{
    public class LookupApiController : ApiController
    {

        #region Regions
        [HttpGet]
        public IEnumerable<Horse_M> GetHorses(string query = "")
        {
            IEnumerable<Horse> hList = new List<Horse>();

            //if (!String.IsNullOrEmpty(query))
            hList = SelectHorses.SelectAllActiveHorse(SessionHelper.SubscriptionID, false).Where(h => h.Name.Contains(query == null ? "" : query)).ToList();

            List<Horse_M> horsemodel = (from aa in hList
                                        select new Horse_M
                                        {
                                            Active = aa.Active,
                                            AttachmentsPath = aa.Attachments,
                                            Comments = aa.Comments,
                                            Dam = aa.Dam,
                                            DateArrived = aa.DateArrived,
                                            DateSold = aa.DateSold,
                                            AgeCategory = aa.AgeCategory,
                                            DOB = aa.DOB,
                                            HorseID = aa.HorseID,
                                            ImagePath = aa.Image,
                                            Name = aa.Name,
                                            Owners = aa.HorseOwners.Select(o => new HorseOwner_M()
                                            {
                                                HorseOwnerID = o.HorseOwnerID,
                                                HorseID = o.HorseID,
                                                Active = o.Owner.Active,
                                                ContactNumber = o.Owner.ContactNumber,
                                                EmailAddress = o.Owner.EmailAddress,
                                                Name = o.Owner.Name,
                                                OwnerID = o.Owner.OwnerID,
                                                SubscriberID = o.Owner.SubscriberID,
                                                Delete = false
                                            }).ToList(),
                                            Colour = aa.Colour,
                                            Sex = aa.Sex,
                                            SireID = (int)(aa.SireID.HasValue ? aa.SireID : 0),
                                            SireDisplay = aa.Sire != null ? aa.Sire.SireName : String.Empty,
                                            SubscriberID = aa.SubscriberID,
                                            UserID = aa.UserID,
                                            UserName = aa.UserDetail != null ? aa.UserDetail.UserName : ""
                                        }).ToList();


            return horsemodel;
        }

        [HttpGet]
        public Horse_M ValidateHorse(string query = "")
        {
            IEnumerable<Horse> hList = new List<Horse>();

            hList = SelectHorses.SelectAllActiveHorse(SessionHelper.SubscriptionID, false).Where(h => h.Name.ToUpper() == query.ToUpper()).ToList();

            Horse_M horsemodel = (from aa in hList
                                        select new Horse_M
                                        {
                                            Active = aa.Active,
                                            AttachmentsPath = aa.Attachments,
                                            Comments = aa.Comments,
                                            Dam = aa.Dam,
                                            DateArrived = aa.DateArrived,
                                            DateSold = aa.DateSold,
                                            AgeCategory = aa.AgeCategory,
                                            DOB = aa.DOB,
                                            HorseID = aa.HorseID,
                                            ImagePath = aa.Image,
                                            Name = aa.Name,
                                            Colour = aa.Colour,
                                            Sex = aa.Sex,
                                            SireID = (int)(aa.SireID.HasValue ? aa.SireID : 0),
                                            SireDisplay = aa.Sire != null ? aa.Sire.SireName : String.Empty,
                                            SubscriberID = aa.SubscriberID,
                                            UserID = aa.UserID,
                                            UserName = aa.UserDetail != null ? aa.UserDetail.UserName : ""
                                        }).FirstOrDefault();

            return horsemodel;
        }
        #endregion

        

    }
}