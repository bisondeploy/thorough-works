﻿using MvcPaging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Routing;
using ThoroughWorks.Data;
using ThoroughWorks.Repository.HorseManagement;
using ThoroughWorks.Models;
using ThoroughWorks.Web.Models.Helpers;
using ThoroughWorks.Repository.VetWorkManagement;
using ThoroughWorks.Repository.HorseOwnerManagement;

namespace ThoroughWorks.Web.Controllers
{
    [Authorize]
    public class HorseController : Controller
    {
        #region "get methods"

        public ActionResult Index()
        {
            HorseSearch horsesearch = (HorseSearch)TempData["horsesearch"];
            horsesearch = horsesearch ?? new HorseSearch();
            horsesearch.CurrentOnly = true;
            ViewBag.Search = horsesearch;
            
            IPagedList<Horse_M> horsePaged = SelectHorseFilterBased(horsesearch);
            return View(horsePaged);
        }

        [HttpGet]
        public ActionResult Add(int? id, string ReturnUrl = "")
        {
            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Url.Action("Index", "Horse") : Server.UrlDecode(ReturnUrl);
            //return PartialView("AddEdit", new Horse_M());
            return PartialView("AddEdit", Horse_M.CreateInfo(0, "", true, new List<HorseOwner_M>()));
        }

        [HttpGet]
        public ActionResult Edit(int id, string ReturnUrl = "")
        {

            


            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Url.Action("Index", "Horse") : Server.UrlDecode(ReturnUrl);

            SelectHorses selecthorses = new SelectHorses();
            Horse horse = selecthorses.SelectHorseByID(id);

            Horse_M horse_m = new Horse_M()
            {
                Active = horse.Active,
                Current = horse.Current,
                AttachmentsPath = horse.Attachments,
                HorseID = horse.HorseID,
                UserID = horse.UserID,
                SubscriberID = horse.SubscriberID,
                SireID = (int) (horse.SireID.HasValue ? horse.SireID : 0),
                SireDisplay = horse.Sire.SireName,
                Owners = horse.HorseOwners.Select(o => new HorseOwner_M()
                {
                    HorseOwnerID = o.HorseOwnerID,
                    HorseID = o.HorseID,
                    Active = o.Owner.Active,
                    ContactNumber = o.Owner.ContactNumber,
                    EmailAddress = o.Owner.EmailAddress,
                    Name = o.Owner.Name,
                    OwnerID = o.Owner.OwnerID,
                    SubscriberID = o.Owner.SubscriberID,
                    Delete = false
                }).ToList(),
                Name = horse.Name,
                ImagePath = horse.Image,
                AgeCategory = horse.AgeCategory,
                DOB = horse.DOB,
                DateSold = horse.DateSold,
                Comments = horse.Comments,
                Dam = horse.Dam,
                DateArrived = horse.DateArrived,
                Colour = horse.Colour,
                Sex = horse.Sex == "F" ? "Filly" : "Colt",
            };
            return View("AddEdit", horse_m);
        }

        [HttpGet]
        public ActionResult DeleteRecord(int HorseID)
        {
            DeleteHorses deletehorses = new DeleteHorses();
            deletehorses.Do(HorseID);

            IPagedList<Horse_M> horsePaged = SelectHorseFilterBased(new HorseSearch());
            return PartialView("Listing", horsePaged);
        }

        private IPagedList<Horse_M> SelectHorseFilterBased(HorseSearch search)
        {
            int currentPageIndex = search.Page.HasValue ? search.Page.Value - 1 : 0;

            List<string> searchTags;
            List<Horse> horses = null;


            // Get all horses
            horses = (from hrs in SelectHorses.SelectAllActiveHorse(SessionHelper.SubscriptionID, search.CurrentOnly)
                      select hrs).ToList();

            List<Horse_M> horsemodel = (from aa in horses
                                        select new Horse_M
                                        {
                                            Active = aa.Active,
                                            Current = aa.Current,
                                            AttachmentsPath = aa.Attachments,
                                            Comments = aa.Comments,
                                            Dam = aa.Dam,
                                            DateArrived = aa.DateArrived,
                                            DateSold = aa.DateSold,
                                            AgeCategory = aa.AgeCategory,
                                            DOB = aa.DOB,
                                            HorseID = aa.HorseID,
                                            ImagePath = aa.Image,
                                            Name = aa.Name,
                                            Owners = aa.HorseOwners.Select(o => new HorseOwner_M()
                                            {
                                                HorseOwnerID = o.HorseOwnerID,
                                                HorseID = o.HorseID,
                                                Active = o.Owner.Active,
                                                ContactNumber = o.Owner.ContactNumber,
                                                EmailAddress = o.Owner.EmailAddress,
                                                Name = o.Owner.Name,
                                                OwnerID = o.Owner.OwnerID,
                                                SubscriberID = o.Owner.SubscriberID,
                                                Delete = false
                                            }).ToList(),
                                            Colour = aa.Colour,
                                            Sex = aa.Sex,
                                            SireID = (int)(aa.SireID.HasValue ? aa.SireID : 0),
                                            SireDisplay = aa.Sire != null ?  aa.Sire.SireName : String.Empty,
                                            SubscriberID = aa.SubscriberID,
                                            UserID = aa.UserID,
                                            UserName = aa.UserDetail != null ? aa.UserDetail.UserName : ""
                                        }).ToList();


            if ( search.SearchTags != null )
            {
                searchTags = new List<string> (search.SearchTags.Split(','));     // There may be any number search tags. Convert to array of string so can iterate

            
                var matchingHorses = horsemodel.Where(j => j.Name.Length > 0 );
                foreach (string searchTag in searchTags)
                {
                    string tag = searchTag.ToUpper();

                    horsemodel = horsemodel.Where(j => j.Name.ToUpper().Contains(tag)
                    || j.AgeCategoryDisplay.ToUpper().Contains(tag)
                    || j.SexDisplay.ToUpper().Contains(tag)
                    || (j.DOB.HasValue ? j.DOB.Value.ToString().ToUpper().Contains(tag) : false)
                    //|| (j.Owner.Name != null && j.Owner.Name.ToUpper().Contains(tag))
                    || (j.Owners.Any(o => o.Name.Contains(tag) ))
                    || (j.SireDisplay != null && j.SireDisplay.ToUpper().Contains(tag))
                    || (j.Dam != null && j.Dam.ToUpper().Contains(tag))
                    || (j.Comments != null && j.Comments.ToUpper().Contains(tag))
                    || (j.Colour != null && j.Colour.ToUpper().Contains(tag))
                    || (j.DateArrived.HasValue ? j.DateArrived.Value.ToString().Contains(searchTag) : false) 
                    || (j.DateSold.HasValue ? j.DateSold.Value.ToString().Contains(searchTag) : false)
                    ).ToList();
                }

                
            }
            
            
            IPagedList<Horse_M> horsepage = horsemodel.OrderBy(j => j.HorseID).ToPagedList(currentPageIndex, 10);
            return horsepage;
        }

        #endregion

        #region "Post methods"

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveRecord(Horse_M horse_m, HorseSearch search, string ReturnUrl)
        {
            if (ModelState.IsValid) //if valid model then update the record and show list
            {
                Horse horse = new Horse()
                {
                    UserID = horse_m.UserID,
                    DateArrived = horse_m.DateArrived,
                    Dam = horse_m.Dam,
                    Comments = horse_m.Comments,
                    DateSold = horse_m.DateSold,
                    AgeCategory = horse_m.AgeCategory,
                    DOB = horse_m.DOB,
                    Image = horse_m.ImagePath,
                    Name = horse_m.Name,
                    SireID = horse_m.SireID,
                    HorseID = horse_m.HorseID,
                    Colour = horse_m.Colour,
                    Sex = horse_m.Sex == "Filly" ? "F" : "M",
                    Active = true,
                    Current = horse_m.Current,
                    Attachments = horse_m.AttachmentsPath,
                    SubscriberID = horse_m.SubscriberID,
                    
                };

                if (horse_m.Owners != null)
                {
                    horse.HorseOwners = horse_m.Owners.Select(x => new HorseOwner
                    {
                        OwnerID = x.OwnerID,
                    }).ToList();

                }
                horse.Image = horse_m.Image != null ? horse_m.Image.FileName : horse.Image;
                horse.Attachments = horse_m.Attachments != null ? horse_m.Attachments.FileName : horse.Attachments;

                if (horse.HorseID == 0)
                {
                    CreateHorses createhorses = new CreateHorses();
                    horse.CreatedBy = SessionHelper.UserID;
                    horse.CreatedDate = DateTime.Now;
                    createhorses.Do(horse);
                }
                else
                {
                    UpdateHorse updatehorse = new UpdateHorse();
                    horse.UpdatedBy = SessionHelper.UserID;
                    horse.UpdatedDate = DateTime.Now;
                    updatehorse.Do(horse);
                }

                if (!Directory.Exists("~/files"))
                    Directory.CreateDirectory(Server.MapPath("~/files"));

                if (!Directory.Exists("~/files/" + horse.HorseID))
                    Directory.CreateDirectory(Server.MapPath("~/files/" + horse.HorseID));

                if (horse_m.Attachments != null)
                    horse_m.Attachments.SaveAs(Server.MapPath("~/files/" + horse.HorseID + "/" + horse_m.Attachments.FileName));

                if (horse_m.Image != null)
                {
                    var fullPath = Server.MapPath("~/files/" + horse.HorseID + "/" + horse_m.Image.FileName);
                    // resize image to make sure it's no larger than say 400px wide.
                    using (var ms = new MemoryStream())
                    {
                        horse_m.Image.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();

                        byte[] resizedImage = ImageResize.CreateThumbnail(array, 1000);

                        if (resizedImage != null)
                            System.IO.File.WriteAllBytes(fullPath, resizedImage);
                    }
                }

                IPagedList<Horse_M> horsePaged = SelectHorseFilterBased(search);
                Session["Message"] = "Horse Details Saved Succesfully";
                TempData["horsesearch"] = search;
                return Redirect(ReturnUrl);
            }
            this.Response.StatusCode = 400;
            return PartialView("AddEdit", horse_m);
        }

        [HttpPost]
        public ActionResult AjaxListing(HorseSearch search)
        {
            IPagedList<Horse_M> horsePaged = SelectHorseFilterBased(search);
            return PartialView("Listing", horsePaged);
        }

        [HttpPost]
        public ActionResult Validate(Horse_M horse_m)
        {
            bool valid = true;
            if (ModelState.IsValid)
            {
                valid = true;
                return Json(valid, JsonRequestBehavior.AllowGet);
            }
            else
            {
                this.Response.StatusCode = 400;
                return PartialView("AddEdit", horse_m);
            }
        }

        [HttpPost]
        public JsonResult RemoveImage(int id, string ReturnUrl)
        {
            UpdateHorse updatehorse = new UpdateHorse();
            updatehorse.RemoveImage(id, SessionHelper.UserID);

            return new JsonResult()
            {
                Data = true
            };
            //return Redirect(Server.UrlDecode(ReturnUrl));
        }

        [HttpPost]
        public JsonResult RemoveAttachment(int id, string ReturnUrl)
        {
            UpdateHorse updatehorse = new UpdateHorse();
            updatehorse.RemoveAttachment(id, SessionHelper.UserID);

            return new JsonResult()
            {
                Data = true,
            };
            //return Redirect(Server.UrlDecode(ReturnUrl));
        }



        public ActionResult VetWork(int id, int? Page)
        {
            int currentPageIndex = Page.HasValue ? Page.Value - 1 : 0;
            
            List<VetWork> vwl;
            vwl = SelectVetWork.SelectAllVetWorkByHorse(id);

            List<VetWork_M> vetworkmodel = (from vw in vwl
                                            select new VetWork_M
                                            {
                                                VetWorkID = vw.VetWorkID,
                                                HorseID = vw.HorseID,
                                                HorseName = vw.Horse.Name,
                                                VetID = vw.UserDetail.UserID,
                                                VetName = vw.UserDetail.FirstName + " " + vw.UserDetail.LastName,
                                                AgeCategory = vw.AgeCategory,
                                                VisitDate = vw.VisitDate,
                                                LastVisitDate = SelectVetWork.GetLastVisitDate(vw.HorseID, vw.VisitDate),
                                                NextVisitDate = SelectVetWork.SelectNextVisitDate(vw.HorseID, vw.VisitDate),
                                                NextInFor = SelectVetWork.SelectNextVisitDetails(vw.HorseID, vw.VisitDate).InFor,
                                                NextVisitID = SelectVetWork.SelectNextVisitDetails(vw.HorseID, vw.VisitDate).VetWorkID,
                                                InFor = vw.InFor,
                                                WorkCompleted = vw.WorkCompleted,
                                                Status = vw.Status

                                            }).OrderBy(vw => vw.VisitDate).ToList();


            IPagedList<VetWork_M> vwpagedlist = vetworkmodel.ToPagedList(currentPageIndex, 10);
            return PartialView("VetWorkListing", vwpagedlist);
        }


        [HttpGet]
        public ActionResult DeleteVetWorkRecord(int VetWorkID, int HorseID)
        {

            DeleteVetWork deletevw = new DeleteVetWork();
            deletevw.Do(VetWorkID);

            List<VetWork> vwl = SelectVetWork.SelectAllVetWorkByHorse(HorseID);



            List<VetWork_M> vetworkmodel = (from vw in vwl
                                            select new VetWork_M
                                            {
                                                VetWorkID = vw.VetWorkID,
                                                HorseID = vw.HorseID,
                                                HorseName = vw.Horse.Name,
                                                VetID = vw.UserDetail.UserID,
                                                VetName = vw.UserDetail.FirstName + " " + vw.UserDetail.LastName,
                                                AgeCategory = vw.Horse.AgeCategory,
                                                VisitDate = vw.VisitDate,
                                                LastVisitDate = SelectVetWork.GetLastVisitDate(vw.HorseID, vw.VisitDate),
                                                NextVisitDate = SelectVetWork.SelectNextVisitDate(vw.HorseID, vw.VisitDate),
                                                NextVisitID = SelectVetWork.SelectNextVisitDetails(vw.HorseID, vw.VisitDate).VetWorkID,
                                                InFor = vw.InFor,
                                                WorkCompleted = vw.WorkCompleted,
                                                Status = vw.Status

                                            }).ToList();

            IPagedList<VetWork_M> vwpagedlist = vetworkmodel.ToPagedList(0, 10);  //!! pass Current page index
            return PartialView("VetWorkListing", vwpagedlist);

        }

        
        public ActionResult Owners(int id)
        {
            if (id != 0)
            {
                SelectHorses selecthorses = new SelectHorses();
                Horse horse = selecthorses.SelectHorseByID(id);

                Horse_M horse_m = new Horse_M()
                {
                    Active = horse.Active,
                    Current = horse.Current,
                    AttachmentsPath = horse.Attachments,
                    HorseID = horse.HorseID,
                    UserID = horse.UserID,
                    SubscriberID = horse.SubscriberID,
                    SireID = (int)horse.SireID,
                    SireDisplay = horse.Sire.SireName,
                    Owners = horse.HorseOwners.Select(o => new HorseOwner_M()
                    {
                        HorseOwnerID = o.HorseOwnerID,
                        HorseID = o.HorseID,
                        Active = o.Owner.Active,
                        ContactNumber = o.Owner.ContactNumber,
                        EmailAddress = o.Owner.EmailAddress,
                        Name = o.Owner.Name,
                        OwnerID = o.Owner.OwnerID,
                        SubscriberID = o.Owner.SubscriberID,

                    }).ToList(),
                    Name = horse.Name,
                    ImagePath = horse.Image,
                    AgeCategory = horse.AgeCategory,
                    DOB = horse.DOB,
                    DateSold = horse.DateSold,
                    Comments = horse.Comments,
                    Dam = horse.Dam,
                    DateArrived = horse.DateArrived,
                    Colour = horse.Colour,
                    Sex = horse.Sex == "F" ? "Filly" : "Colt",
                };

                ViewBag.HorseName = horse_m.Name;
                ViewBag.HorseID = horse_m.HorseID;

                return View(horse_m.Owners);
            }
            ViewBag.HorseID = 0;
            return View(new List<HorseOwner_M>());
        }


        [HttpPost]
        public ActionResult Owners(List<HorseOwner_M> owners, int HorseID)
        {
            ViewBag.SubscriberID = SessionHelper.SubscriptionID;


            SelectHorses selecthorses = new SelectHorses();
            Horse horse = selecthorses.SelectHorseByID(HorseID);

            // Remove delete from DB those no longer in the list
            foreach (HorseOwner existingOwner in horse.HorseOwners)
            {
                if (!owners.Where(o => o.OwnerID == existingOwner.OwnerID).Any())
                {
                    DeleteHorseOwner del = new DeleteHorseOwner();
                    del.Do(existingOwner.HorseOwnerID);
                }
            }

            if (owners != null)
            {
                foreach (HorseOwner_M newOwner in owners)
                {
                    if (newOwner.OwnerID != 0)  // ignore new rows with no name selected
                    {
                        // Add new items - those listed that are not in the existing owners list
                        if (!horse.HorseOwners.Where(o => o.OwnerID == newOwner.OwnerID).Any() && newOwner.Delete == false)
                        {
                            CreateHorseOwner cr = new CreateHorseOwner();
                            cr.Do(new HorseOwner() { HorseID = HorseID, OwnerID = newOwner.OwnerID });
                        }
                        else if (newOwner.Delete == true)
                        {
                            DeleteHorseOwner cr = new DeleteHorseOwner();
                            cr.Do(newOwner.HorseOwnerID);
                        }
                    }
                }
            }
            Session["Message"] = "Owner List Saved Successfully";
            return RedirectToAction("Owners", HorseID);
            

            //if (ModelState.IsValid)
            //{
            //    try
            //    {
            //        var update = new UpdateHorse();
            //        update.Do(horse);
            //        Session["Message"] = "Owner List Saved Successfully";
            //        return RedirectToAction("Owners", HorseID);
                    
            //    }
            //    catch (Exception ex)
            //    {
            //        ModelState.AddModelError("", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
            //    }
            //}

            return View(owners);
        }


        #endregion
    }
}
