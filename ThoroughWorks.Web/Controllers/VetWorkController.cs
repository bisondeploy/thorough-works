﻿using MvcPaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThoroughWorks.Data;
using ThoroughWorks.Repository.VetWorkManagement;
using ThoroughWorks.Models;
using ThoroughWorks.Web.Models.Helpers;
using System.Configuration;
using ThoroughWorks.Repository.VetWorkCompletedManagement;
using ThoroughWorks.Repository.VetWorkAttributeManagement;
using ThoroughWorks.Repository.HorseManagement;
using ThoroughWorks.Repository.UserManagement;
using System.Collections.Specialized;

namespace ThoroughWorks.Web.Controllers
{
    [Authorize]
    public class VetWorkController : Controller
    {
        //
        // GET: /VetWork/

        public ActionResult Index()
        {

            return View();
        }



        public JsonResult GetVetWorkCalendarEvents()
        {

            List<VetWorkDayCount> allVetWork = SelectVetWork.SelectVetWorkCountsByDay(Helpers.isAdmin() ? 0 : SessionHelper.UserID);

            var apptList = from v in allVetWork
                           select new
                           {
                               //id = v.VetWorkID,
                               title = v.VetName + " (" + v.AppointmentCount.ToString() + ")",
                               start = v.VisitDate.ToString("s"),
                               end = v.VisitDate.ToString("s"),
                               startDateTime = v.VisitDate.Ticks,
                               //color = e.StatusColor,
                               vetID = v.VetID,
                               allDay = false
                           };
            var rows = apptList.ToArray();
            return Json(rows, JsonRequestBehavior.AllowGet);
        }


        public ActionResult WorkList(int VetID, long VisitDate)
        {
            DateTime dt = new DateTime(VisitDate);

            return GetWorkList(VetID, dt);

        }

        // Fullcalendar has different date formats for event click vs day click so need separate action methods
        public ActionResult WorkListD(string VisitDate)
        {
            DateTime dt;
            DateTime.TryParse(VisitDate, out dt);
            if( dt != DateTime.MinValue )
                return GetWorkList(0, dt);
            else  // Clicked on square that is not a valid date    
                return RedirectToAction("Index");
        }

        public ActionResult GetWorkList(int VetID, DateTime dt)
        {
            VetWorkSearch vwsearch = (VetWorkSearch)TempData["vetworksearch"];
            vwsearch = vwsearch ?? new VetWorkSearch();
            ViewBag.Search = vwsearch;
            vwsearch.VetID = VetID;
            vwsearch.Dt = dt;
            TempData["vetworksearch"] = vwsearch;

            // get Vet
            if (VetID > 0)
            {
                var user = SelectUserDetails.SelectByUserID(VetID);
                ViewBag.PageTitle = String.Format("Vet Work for {0} {1} on {2}", user.FirstName, user.LastName, dt.ToString("dd/MM/yyyy"));
            }
            else
                ViewBag.PageTitle = String.Format("Vet Work on {0}", dt.ToString("dd/MM/yyyy"));

            IPagedList<VetWork_M> vwpagedlist = SearchVetWork(vwsearch);

            return View("WorkList", vwpagedlist);
        }


        public ActionResult WorkSheet(int VetID, string VisitDate, string ReturnUrl)
        {
            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Url.Action("Index", "VetWork", new { VisitDate = VisitDate, VetID = VetID}) : Server.UrlDecode(ReturnUrl);
            List<VetWork_M> vwlist = PrepareWorkSheet(VetID, VisitDate, SessionHelper.SubscriptionID);
            return View("WorkSheet", vwlist);
        }

        [AllowAnonymous]
        public ActionResult WorkSheetPDF(int VetID, string VisitDate, int SubID)
        {
            List<VetWork_M> vwlist = PrepareWorkSheet(VetID, VisitDate, SubID);

            return View("WorkSheetPDF", vwlist);
        }


        private List<VetWork_M> PrepareWorkSheet(int VetID, string VisitDate, int SubscriptionID)
        {
            VetWorkSearch vwsearch = (VetWorkSearch)TempData["vetworksearch"];
            vwsearch = vwsearch ?? new VetWorkSearch();
            ViewBag.Search = vwsearch;
            vwsearch.VetID = VetID;
            vwsearch.Dt = Helpers.EnsureDate(VisitDate);
            TempData["vetworksearch"] = vwsearch;


            List<VetWork_M> vwlist = SearchVetWork(vwsearch, 1000).ToList();

            // Init the VetWork Completed List with all possible items - regrdless of whether they filled them yet
            foreach (VetWork_M itemv in vwlist)
            {
                List<VetWorkCompleted> lst;
                lst = SelectVetWorkCompleted.SelectAllVetWorkOptionsByVetWorkID(SubscriptionID, itemv.VetWorkID);

                itemv.VetWorkCompleted = (from item in lst
                                          select new VetWorkCompleted_M
                                          {
                                              VetWorkCompletedID = item.VetWorkCompletedID,
                                              VetWorkAttributeID = item.VetWorkAttributeID,
                                              VetWorkID = item.VetWorkID,
                                              AttributeValue = item.AttributeValue,
                                              VetWorkAttributeName = item.VetWorkAttribute == null ? "" : item.VetWorkAttribute.AttributeName,
                                              VetWorkAttributeSequence = item.VetWorkAttribute.Sequence,
                                          }).OrderBy(item => item.VetWorkAttributeSequence).ToList();


            }

            // get Vet
            if (VetID > 0)
            {
                var user = SelectUserDetails.SelectByUserID(VetID);
                ViewBag.PageTitle = String.Format("Vet Work for {0} {1} on {2}", user.FirstName, user.LastName, vwsearch.Dt.ToString("dd/MM/yyyy"));
            }
            else
                ViewBag.PageTitle = String.Format("Vet Work on {0}", VisitDate);

            return vwlist;
        }




        [AllowAnonymous]
        public ActionResult WorkListPDFD(int VetID, string VisitDate)
        {
            // Visit date may be a long (if they originally clicked a day) or string like dd/mm/yyyy
            // Either way convert to a date
            DateTime dt = Helpers.EnsureDate(VisitDate);
            
            VetWorkSearch vwsearch = new VetWorkSearch();
            vwsearch.Page = 1;
            vwsearch.VetID = VetID;
            vwsearch.Dt = dt;

            IPagedList<VetWork_M> vwpagedlist = SearchVetWork(vwsearch, 1000);

            // get Vet
            if (VetID > 0)
            {
                var user = SelectUserDetails.SelectByUserID(VetID);
                ViewBag.PageTitle = String.Format("Vet Work for {0} {1} on {2}", user.FirstName, user.LastName, VisitDate);
            }
            else
                ViewBag.PageTitle = String.Format("Vet Work on {0}", VisitDate);

            return View("WorkListPDF", vwpagedlist);
        }



        [HttpGet]
        public ActionResult Add(int? HorseID, string ReturnUrl = "")
        {
            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Url.Action("Index", "VetWork") : Server.UrlDecode(ReturnUrl);

            ViewBag.HorseID = HorseID;

            // Init Vet and date fields in Add screen
            NameValueCollection referrerQueryValues = new NameValueCollection();
            if (Request.UrlReferrer != null)
            {
                referrerQueryValues = HttpUtility.ParseQueryString(Request.UrlReferrer.Query);
            }
            ViewBag.VetID = referrerQueryValues["VetID"];

            if (referrerQueryValues["VisitDate"] != null)
            {
                string visitDate = referrerQueryValues["VisitDate"];
                // Visit date may be a long (if they originally clicked a day) or string like dd/mm/yyyy
                // Either way pass to view as a string
                if (visitDate.Contains("/"))
                    ViewBag.VisitDate = visitDate;
                else
                {
                    long lDt = long.Parse(visitDate);
                    ViewBag.VisitDate = new DateTime(lDt).ToString("dd/MM/yyyy");
                }
            }

            return PartialView("AddEdit", new VetWork_M());
        }

        [HttpGet]
        public ActionResult Edit(int id, string ReturnUrl = "")
        {
            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Url.Action("Index", "VetWork") : Server.UrlDecode(ReturnUrl);

            //SelectVetWork selectvw = new SelectVetWork();
            VetWork vw = SelectVetWork.SelectVetWorkByID(id);

            DateTime? prevVisitDate = SelectVetWork.GetLastVisitDate(vw.HorseID, vw.VisitDate);
            VetWork nextVisitDetails = SelectVetWork.SelectNextVisitDetails(vw.HorseID, vw.VisitDate);
            DateTime? nextVisitDate = SelectVetWork.SelectNextVisitDate(vw.HorseID, vw.VisitDate);
            //string nextInFor = SelectVetWork.GetNextVisitInFor(vw.HorseID, vw.VisitDate);

            VetWork_M vetwork_m = new VetWork_M()
            {
                VetWorkID = vw.VetWorkID,
                HorseID = vw.HorseID,
                HorseName = vw.Horse.Name,
                VetID = vw.UserDetail.UserID,
                VetName = vw.UserDetail.FirstName + " " + vw.UserDetail.LastName,
                AgeCategory = vw.AgeCategory,
                VisitDate = vw.VisitDate,
                LastVisitDate = prevVisitDate,
                NextVisitDate = nextVisitDate,
                InFor = vw.InFor,
                NextInFor = nextVisitDetails.InFor,
                WorkCompleted = vw.WorkCompleted,
                Status = vw.Status,
                Active = vw.Active,
                NextVisitID = nextVisitDetails.VetWorkID

            };


            List<VetWorkCompleted> lst;
            lst = SelectVetWorkCompleted.SelectAllVetWorkOptionsByVetWorkID(SessionHelper.SubscriptionID, id);

            vetwork_m.VetWorkCompleted = (from item in lst
                                          select new VetWorkCompleted_M
                                          {
                                              VetWorkCompletedID = item.VetWorkCompletedID,
                                              VetWorkAttributeID = item.VetWorkAttributeID,
                                              VetWorkID = item.VetWorkID,
                                              AttributeValue = item.AttributeValue,
                                              VetWorkAttributeName = item.VetWorkAttribute == null ? "" : item.VetWorkAttribute.AttributeName,
                                              VetWorkAttributeSequence = item.VetWorkAttribute.Sequence,
                                          }).OrderBy(item => item.VetWorkAttributeSequence).ToList();

            return View("AddEdit", vetwork_m);
        }


        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveRecord(VetWork_M vetwork_m, VetWorkSearch search, string ReturnUrl)
        {
            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Url.Action("Index", "VetWork") : Server.UrlDecode(ReturnUrl);

            if (ModelState.IsValid) //if valid model then update the record and show list
            {

                int? ageCatAtAppt = SelectHorses.SelectHorseAgeCategoryOnDate(vetwork_m.HorseID, (DateTime)vetwork_m.VisitDate);
                VetWork vetwork = new VetWork()
                {
                    VetWorkID = vetwork_m.VetWorkID,
                    HorseID = vetwork_m.HorseID,
                    AgeCategory = ageCatAtAppt,
                    UserID = vetwork_m.VetID,
                    VisitDate = (DateTime)vetwork_m.VisitDate,
                    InFor = vetwork_m.InFor,
                    WorkCompleted = vetwork_m.WorkCompleted,
                    Status = vetwork_m.Status,
                    Active = true
                };



                if (vetwork.VetWorkID == 0)
                {
                    // Get the horse age category and store it against the appt

                    CreateVetWork createvw = new CreateVetWork();
                    vetwork.CreatedBy = SessionHelper.UserID;
                    vetwork.CreatedDate = DateTime.Now;
                    vetwork.Status = "Pending";
                    createvw.Do(vetwork);
                }
                else
                {
                    UpdateVetWork updatevw = new UpdateVetWork();
                    vetwork.UpdatedBy = SessionHelper.UserID;
                    vetwork.UpdatedDate = DateTime.Now;


                    updatevw.Do(vetwork);

                }


                Session["Message"] = "Appointment Details Saved Succesfully";
                return Redirect(ReturnUrl);
            }
            this.Response.StatusCode = 400;
            return PartialView("AddEdit", vetwork_m);
        }


        [HttpGet]
        public ActionResult DeleteRecord(int VetWorkID, int Page)
        {

            DeleteVetWork deletevw = new DeleteVetWork();
            deletevw.Do(VetWorkID);

            List<VetWork> vwl = SelectVetWork.SelectAllVetWork();



            List<VetWork_M> vetworkmodel = (from vw in vwl
                                            select new VetWork_M
                                            {
                                                VetWorkID = vw.VetWorkID,
                                                HorseID = vw.HorseID,
                                                HorseName = vw.Horse.Name,
                                                VetID = vw.UserDetail.UserID,
                                                VetName = vw.UserDetail.FirstName + " " + vw.UserDetail.LastName,
                                                AgeCategory = vw.Horse.AgeCategory,
                                                VisitDate = vw.VisitDate,
                                                LastVisitDate = SelectVetWork.GetLastVisitDate(vw.HorseID, vw.VisitDate),
                                                NextVisitDate = SelectVetWork.SelectNextVisitDate(vw.HorseID, vw.VisitDate),
                                                NextVisitID = SelectVetWork.SelectNextVisitDetails(vw.HorseID, vw.VisitDate).VetWorkID,
                                                InFor = vw.InFor,
                                                WorkCompleted = vw.WorkCompleted,
                                                Status = vw.Status

                                            }).ToList();

            IPagedList<VetWork_M> vwpagedlist = vetworkmodel.ToPagedList(Page, 10); 
            return PartialView("Listing", vwpagedlist);

        }


        [HttpPost]
        public ActionResult AjaxListing(VetWorkSearch search)
        {
            VetWorkSearch vws = search;
            if (TempData["vetworksearch"] != null)
            {
                vws.VetID = ((VetWorkSearch)TempData["vetworksearch"]).VetID;
                vws.Dt = ((VetWorkSearch)TempData["vetworksearch"]).Dt;
            }
            TempData["vetworksearch"] = search;

            IPagedList<VetWork_M> vwpagedlist = SearchVetWork(vws);

            return PartialView("Listing", vwpagedlist);
        }

        private IPagedList<VetWork_M> SearchVetWork(VetWorkSearch vwsearch, int pageSize = 10)
        {
            int currentPageIndex = vwsearch.Page.HasValue ? vwsearch.Page.Value - 1 : 0;

            // If current user is a vet, they should only see their own work
            if (Helpers.isVet() && !Helpers.isAdmin())
                vwsearch.VetID = SessionHelper.UserID;

            List<VetWork> vwl;
            if (vwsearch.VetID == 0)
            {
                vwl = SelectVetWork.SelectAllVetWorkByDay(vwsearch.Dt);
            }
            else
            {
                vwl = SelectVetWork.SelectAllVetWorkByVetByDay(vwsearch.VetID, vwsearch.Dt);
            }

            List<VetWork_M> vetworkmodel = (from vw in vwl
                                            select new VetWork_M
                                            {
                                                VetWorkID = vw.VetWorkID,
                                                HorseID = vw.HorseID,
                                                HorseName = vw.Horse.Name,
                                                VetID = vw.UserDetail.UserID,
                                                VetName = vw.UserDetail.FirstName + " " + vw.UserDetail.LastName,
                                                AgeCategory = vw.Horse.AgeCategory,
                                                VisitDate = vw.VisitDate,
                                                LastVisitDate = SelectVetWork.GetLastVisitDate(vw.HorseID, vw.VisitDate),
                                                NextVisitDate = SelectVetWork.SelectNextVisitDate(vw.HorseID, vw.VisitDate),
                                                NextVisitID = SelectVetWork.SelectNextVisitDetails(vw.HorseID, vw.VisitDate).VetWorkID,
                                                NextInFor = SelectVetWork.SelectNextVisitDetails(vw.HorseID, vw.VisitDate).InFor,
                                                InFor = vw.InFor,
                                                WorkCompleted = vw.WorkCompleted,
                                                Status = vw.Status

                                            }).ToList();

            IPagedList<VetWork_M> vwpagedlist = vetworkmodel.ToPagedList(currentPageIndex, pageSize);
            return vwpagedlist;
        }


        public RedirectResult WorkListToPDF()
        {
            string urlToConvert;
            if (Request.UrlReferrer.ToString().Contains("WorkListD"))
                urlToConvert = Request.UrlReferrer.ToString().Replace("/WorkListD", "/WorkListPDFD");
            else
                urlToConvert = Request.UrlReferrer.ToString().Replace("/WorkList", "/WorkListPDFD");

            // It writes to a file in temp folder
            string pdfFile = ConfigurationManager.AppSettings["EOTempFolderAbsolutePath"] + "\\VetWork.pdf";
            Helpers.PageToPDF(urlToConvert, pdfFile);

            // URL of PDF generated
            string pdfURL = Request.Url.ToString().Replace("VetWork/WorkListToPDF", ConfigurationManager.AppSettings["EOTempFolderRelativePath"] + "/VetWork.pdf");

            return Redirect(pdfURL);

        }



        [HttpGet]
        public ActionResult Complete(int id, string ReturnUrl = "")
        {
            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Request.UrlReferrer.ToString() : Server.UrlDecode(ReturnUrl);

            //SelectVetWork selectvw = new SelectVetWork();
            VetWork vw = SelectVetWork.SelectVetWorkByID(id);

            DateTime? prevVisitDate = SelectVetWork.GetLastVisitDate(vw.HorseID, vw.VisitDate);
            VetWork nextVisitDetails = SelectVetWork.SelectNextVisitDetails(vw.HorseID, vw.VisitDate);
            DateTime? nextVisitDate = SelectVetWork.SelectNextVisitDate(vw.HorseID, vw.VisitDate);
            //string nextInFor = SelectVetWork.GetNextVisitInFor(vw.HorseID, vw.VisitDate);

            VetWork_M vetwork_m = new VetWork_M()
            {
                VetWorkID = vw.VetWorkID,
                HorseID = vw.HorseID,
                HorseName = vw.Horse.Name,
                VetID = vw.UserDetail.UserID,
                VetName = vw.UserDetail.FirstName + " " + vw.UserDetail.LastName,
                AgeCategory = vw.Horse.AgeCategory,
                VisitDate = vw.VisitDate,
                LastVisitDate = prevVisitDate,
                NextVisitDate = nextVisitDate,
                InFor = vw.InFor,
                NextInFor = nextVisitDetails.InFor,
                WorkCompleted = vw.WorkCompleted,
                Status = vw.Status,
                Active = vw.Active,
                NextVisitID = nextVisitDetails.VetWorkID

            };


            List<VetWorkCompleted> lst;
            lst = SelectVetWorkCompleted.SelectAllVetWorkOptionsByVetWorkID(SessionHelper.SubscriptionID, id);

            vetwork_m.VetWorkCompleted = (from item in lst
                                          select new VetWorkCompleted_M
                                          {
                                              VetWorkCompletedID = item.VetWorkCompletedID,
                                              VetWorkAttributeID = item.VetWorkAttributeID,
                                              VetWorkID = item.VetWorkID,
                                              AttributeValue = item.AttributeValue,
                                              VetWorkAttributeName = item.VetWorkAttribute == null ? "" : item.VetWorkAttribute.AttributeName,
                                              VetWorkAttributeSequence = item.VetWorkAttribute.Sequence,
                                          }).OrderBy(item => item.VetWorkAttributeSequence).ToList();

            return View("Complete", vetwork_m);
        }



        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Complete(VetWork_M vetworkc_m, VetWorkSearch search, string ReturnUrl)
        {
            VetWork_M vetwork_m = (VetWork_M)vetworkc_m;

            if (ModelState.IsValid) //if valid model then update the record and show list
            {
                SaveVetWorkRecord(vetwork_m);

                Session["Message"] = "Vet Work Details Saved Succesfully";
                return Redirect(ReturnUrl);
            }
            this.Response.StatusCode = 400;
            //ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Request.UrlReferrer.ToString() : Server.UrlDecode(ReturnUrl);
            return Redirect(ReturnUrl);
        }




        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult WorkSheet(List<VetWork_M> vetwork_m, VetWorkSearch search, string ReturnUrl)
        {
            foreach (VetWork_M item in vetwork_m)
            {
                SaveVetWorkRecord(item);
                Session["Message"] = "Vet Work Sheet Saved Succesfully";
            }

            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Url.Action("Index", "VetWork", new { VisitDate = search.Dt, VetID = search.VetID}) : Server.UrlDecode(ReturnUrl);
            return View(vetwork_m);
            //if (!String.IsNullOrEmpty(ReturnUrl))
            //    return Redirect(Server.UrlDecode(ReturnUrl));
            //else
            //    return RedirectToAction("Index");
        }



        private void SaveVetWorkRecord(VetWork_M vetwork_m)
        {
            VetWork vetwork = new VetWork()
            {
                VetWorkID = vetwork_m.VetWorkID,
                HorseID = vetwork_m.HorseID,
                AgeCategory = vetwork_m.AgeCategory,
                UserID = vetwork_m.VetID,
                VisitDate = (DateTime)vetwork_m.VisitDate,
                InFor = vetwork_m.InFor,
                WorkCompleted = vetwork_m.WorkCompleted,
                Status = vetwork_m.Status,
                Active = true
            };



            if (vetwork.VetWorkID == 0)
            {
                CreateVetWork createvw = new CreateVetWork();
                vetwork.CreatedBy = SessionHelper.UserID;
                vetwork.CreatedDate = DateTime.Now;
                vetwork.Status = "Pending";
                createvw.Do(vetwork);
            }
            else
            {
                UpdateVetWork updatevw = new UpdateVetWork();
                vetwork.UpdatedBy = SessionHelper.UserID;
                vetwork.UpdatedDate = DateTime.Now;

                // Save vet work attributes only if they filled them in
                List<VetWorkCompleted_M> itemsToSave = vetwork_m.VetWorkCompleted.Where(v => v.AttributeValue != null).ToList();

                vetwork.VetWorkCompleteds = itemsToSave.Select(v => new VetWorkCompleted()
                {
                    VetWorkCompletedID = v.VetWorkCompletedID,
                    VetWorkID = v.VetWorkID,
                    VetWorkAttributeID = v.VetWorkAttributeID,
                    AttributeValue = v.AttributeValue,
                    VetWork = SelectVetWork.SelectVetWorkByID(v.VetWorkID),
                    VetWorkAttribute = SelectVetWorkAttribute.SelectVetWorkAttributeByID(v.VetWorkAttributeID)
                }).ToList();

                // Change status to Completed once Next Visit entered
                if (vetwork_m.NextVisitDate != null)
                    vetwork.Status = "Completed";

                updatevw.Do(vetwork);

                // If they added the next visit details, create new appointment in DB, or update the existing
                if (vetwork_m.NextVisitID != 0)  // there was already a next appt
                {

                    if (vetwork_m.NextVisitDate != null)
                    {
                        int? ageCatAtAppt = SelectHorses.SelectHorseAgeCategoryOnDate(vetwork_m.HorseID, (DateTime)vetwork_m.NextVisitDate);

                        // reuse same object
                        vetwork.VetWorkID = vetwork_m.NextVisitID;
                        vetwork.VisitDate = (DateTime)vetwork_m.NextVisitDate;
                        vetwork.InFor = vetwork_m.NextInFor;   //!! Exceptions if blank
                        vetwork.WorkCompleted = "";
                        vetwork.Status = "Pending";
                        vetwork.UpdatedBy = SessionHelper.UserID;
                        vetwork.UpdatedDate = DateTime.Now;
                        vetwork.VetWorkCompleteds.Clear();

                        UpdateVetWork createvw = new UpdateVetWork();
                        createvw.Do(vetwork);
                    }
                }
                else  // add next appt for first time
                {
                    if (vetwork_m.NextVisitDate != null)
                    {
                        int? ageCatAtAppt = SelectHorses.SelectHorseAgeCategoryOnDate(vetwork_m.HorseID, (DateTime)vetwork_m.NextVisitDate);

                        // reuse same object
                        vetwork.VisitDate = (DateTime)vetwork_m.NextVisitDate;
                        vetwork.InFor = vetwork_m.NextInFor;   //!! Exceptions if blank
                        vetwork.WorkCompleted = "";
                        vetwork.Status = "Pending";
                        vetwork.CreatedBy = SessionHelper.UserID;
                        vetwork.CreatedDate = DateTime.Now;
                        vetwork.UpdatedBy = null;
                        vetwork.UpdatedDate = null;
                        vetwork.VetWorkID = 0;
                        vetwork.VetWorkCompleteds.Clear();

                        CreateVetWork createvw = new CreateVetWork();
                        createvw.Do(vetwork);
                    }

                }

            }
        }



        
        public RedirectResult WorkSheetToPDF()
        {
            // Use referrer not current URL to get querystring
            string urlToConvert = "";
            if (Request.UrlReferrer.ToString().Contains("/WorkSheetD"))
                urlToConvert = Request.UrlReferrer.ToString().Replace("/WorkSheetD", "/WorkSheetPDF");
            else if (Request.UrlReferrer.ToString().Contains("/WorkListD"))
                urlToConvert = Request.UrlReferrer.ToString().Replace("/WorkListD", "/WorkSheetPDF");
            else if (Request.UrlReferrer.ToString().Contains("/Worksheet") )
                urlToConvert = Request.UrlReferrer.ToString().Replace("/WorkSheet/", "/WorkSheetPDF");
            else if (Request.UrlReferrer.ToString().Contains("/WorkList"))
                urlToConvert = Request.UrlReferrer.ToString().Replace("/WorkList", "/WorkSheetPDF");

            urlToConvert = urlToConvert + "&Subid=" + SessionHelper.SubscriptionID;
            var filename = "VetWorkSheet_" + DateTime.Now.Ticks + ".pdf";
            // It writes to a file in temp folder
            string pdfFile = ConfigurationManager.AppSettings["EOTempFolderAbsolutePath"] + "\\" + filename;
            Helpers.PageToPDF(urlToConvert, pdfFile);

            // URL of PDF generated
            string pdfURL = Request.Url.ToString().Replace("VetWork/WorkSheetToPDF", ConfigurationManager.AppSettings["EOTempFolderRelativePath"] + "/" + filename);

            return Redirect(pdfURL);

        }


    }
}
