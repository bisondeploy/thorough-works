﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcPaging;
using ThoroughWorks.Data;
using ThoroughWorks.Models;
using ThoroughWorks.Repository.ViewerManagement;
using ThoroughWorks.Web.Models.Helpers;

namespace ThoroughWorks.Web.Controllers
{
    public class ViewerController : Controller
    {
        public ActionResult Index()
        {
            IPagedList<Viewer_M> list = SearchViewers();
            return View(list);
        }

        [HttpGet]
        public ActionResult Add(int? id, string ReturnUrl = "")
        {
            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Url.Action("Index", "Viewer") : Server.UrlDecode(ReturnUrl);
            var attributes = SelectViewer.SelectAllViewers();

            var model = new Viewer_M()
            {
                Active = true,
            };
            return PartialView("AddEdit", model);
        }

        [HttpGet]
        public ActionResult Edit(int id, string ReturnUrl = "")
        {
            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Url.Action("Index", "Viewer") : Server.UrlDecode(ReturnUrl);

            Viewer entity = SelectViewer.SelectViewerByID(id);

            Viewer_M horse_m = new Viewer_M()
            {
                Active = entity.Active,
                ViewerID = entity.ViewerID,
                ViewerName = entity.ViewerName,

            };
            return View("AddEdit", horse_m);
        }

        [HttpGet]
        public ActionResult DeleteRecord(int EntityID)
        {


            DeleteViewer deleteentities = new DeleteViewer();
            deleteentities.Do(EntityID);

            return PartialView("Listing", SearchViewers());
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult SaveRecord(Viewer_M entity_m, string ReturnUrl)
        {
            if (ModelState.IsValid)
            {
                Viewer entity = new Viewer()
                {
                    ViewerID = entity_m.ViewerID,
                    ViewerName = entity_m.ViewerName,
                    Active = true,
                };

                if (entity.ViewerID == 0)
                {
                    CreateViewer createEntity = new CreateViewer();
                    entity.CreatedBy = SessionHelper.UserID;
                    entity.CreatedDate = DateTime.Now;
                    createEntity.Do(entity);
                }
                else
                {
                    UpdateViewer updateEntity = new UpdateViewer();
                    entity.UpdatedBy = SessionHelper.UserID;
                    entity.UpdatedDate = DateTime.Now;
                    updateEntity.Do(entity);
                }
                //List<Viewer_M> entityPaged = SearchViewers();


                Session["Message"] = "Viewer Saved Succesfully";
                return Redirect(ReturnUrl);
            }
            this.Response.StatusCode = 400;
            return PartialView("AddEdit", entity_m);
        }

        IPagedList<Viewer_M> SearchViewers()
        {
            List<Viewer> entitylist = SelectViewer.SelectAllViewers();

            List<Viewer_M> entityPagedL = (from vwa in entitylist
                                         where vwa.Active
                                         select new Viewer_M
                                         {
                                             ViewerID = vwa.ViewerID,
                                             Active = vwa.Active,
                                             ViewerName = vwa.ViewerName,

                                         }).ToList();



            IPagedList<Viewer_M> entityPaged = entityPagedL.ToPagedList(0, 10);

            return entityPaged;
        }

        [HttpPost]
        public ActionResult AjaxListing(string SearchViewer, int? Page)
        {
            int currentPageIndex = Page.HasValue ? Page.Value - 1 : 0;

            List<Viewer> lst = SelectViewer.SearchViewers(SearchViewer);

            List<Viewer_M> model = (from aa in lst
                                  select new Viewer_M
                                  {
                                      Active = aa.Active,
                                      ViewerName = aa.ViewerName,
                                      ViewerID = aa.ViewerID,
                                  }).ToList();


            IPagedList<Viewer_M> itemsPaged = model.ToPagedList(currentPageIndex, 10);
            return PartialView("Listing", itemsPaged);
        }
	}
}