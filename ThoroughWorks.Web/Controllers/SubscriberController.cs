﻿using MvcPaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThoroughWorks.Data;
using ThoroughWorks.Repository.SubscriberManagement;
using ThoroughWorks.Models;
using ThoroughWorks.Web.Models.Helpers;
using System.IO;

namespace ThoroughWorks.Web.Controllers
{
    [Authorize]
    public class SubscriberController : Controller
    {
        #region "get methods"

        public ActionResult Index()
        {

            IPagedList<Subscriber_M> subscirberm = SelectSubscriberFilterBased(new SubscriberSearch());
            return View(subscirberm);
        }

        [HttpGet]
        public ActionResult Add()
        {
            ViewBag.ReturnUrl = Request.Url.ToString().Replace("Add", "");
            return View("AddEdit", new Subscriber_M());
        }

        [HttpGet]
        public ActionResult Edit(int SubscriberID)
        {
            Subscriber subscriber = SelectSubscriber.SelecSubscriberByID(SubscriberID);

            Subscriber_M subscriber_m = new Subscriber_M()
            {
                Address = subscriber.Address,
                BusinessName = subscriber.BusinessName,
                City = subscriber.City,
                ContactName = subscriber.ContactName,
                EmailID = subscriber.EmailID,
                Fax = subscriber.Fax,
                Mobile = subscriber.Mobile,
                Phone = subscriber.Phone,
                PostalCode = subscriber.PostalCode,
                State = subscriber.State,
                SubscriberID = subscriber.SubscriberID,
                ImagePath = subscriber.LogoFile,
                ResolvedLogoFile = String.IsNullOrEmpty(subscriber.LogoFile) ? "" : Url.Content(subscriber.LogoFile)
            };

            ViewBag.ReturnUrl = Request.Url;

            return View("AddEdit", subscriber_m);
        }

        [HttpGet]
        public ActionResult DeleteRecord(int SubscriberID)
        {
            DeleteSubscriber deletesubscriber = new DeleteSubscriber();
            deletesubscriber.Do(SubscriberID, SessionHelper.UserID);


            IPagedList<Subscriber_M> subscriberPaged = SelectSubscriberFilterBased(new SubscriberSearch());
            return PartialView("Listing", subscriberPaged);
        }

        private IPagedList<Subscriber_M> SelectSubscriberFilterBased(SubscriberSearch search)
        {
            int currentPageIndex = search.Page.HasValue ? search.Page.Value - 1 : 0;
            List<Subscriber> subscribers = (from sub in SelectSubscriber.SelectAllSubscriber()
                                            where (String.IsNullOrWhiteSpace(search.SearchContactName) || Helpers.ConvertString(sub.ContactName).ToLower().Contains(Helpers.ConvertString(search.SearchContactName).ToLower())) &&
                                            (String.IsNullOrWhiteSpace(search.SearchBusinessName) || Helpers.ConvertString(sub.BusinessName).ToLower().Contains(Helpers.ConvertString(search.SearchBusinessName).ToLower()))
                                            select sub).ToList();

            List<Subscriber_M> subscribermodel = (from aa in subscribers
                                                  select new Subscriber_M
                                            {
                                                Address = aa.Address,
                                                BusinessName = aa.BusinessName,
                                                City = aa.City,
                                                ContactName = aa.ContactName,
                                                EmailID = aa.EmailID,
                                                Fax = aa.Fax,
                                                Mobile = aa.Mobile,
                                                Phone = aa.Phone,
                                                PostalCode = aa.PostalCode,
                                                State = aa.State,
                                                ImagePath = aa.LogoFile,
                                                ResolvedLogoFile = String.IsNullOrEmpty(aa.LogoFile) ? "" : Url.Content(aa.LogoFile),
                                                SubscriberID = aa.SubscriberID,
                                                SubscriberPayments = (from payment in aa.SubscriberPayments
                                                                      select new SubscriberPayment_M
                                                                      {
                                                                          SubsciberPaymentID = payment.SubsciberPaymentID,
                                                                          Amount = payment.Amount,
                                                                          DatePaid = payment.DatePaid,
                                                                          ExpirationDate = payment.ExpirationDate
                                                                      }).OrderByDescending(j => j.SubsciberPaymentID).ToList()
                                            }).ToList();
            IPagedList<Subscriber_M> subscriberPaged = subscribermodel.OrderBy(j => j.ContactName).ToPagedList(currentPageIndex, 20);
            return subscriberPaged;
        }

        public ActionResult ShowPayment(int SubscriberID)
        {
            Subscriber_M subscriber_m = SelectPaymentModelByID(SubscriberID);

            return PartialView("PaymentList", subscriber_m);
        }

        private static Subscriber_M SelectPaymentModelByID(int SubscriberID)
        {
            Subscriber subscriber = SelectSubscriber.SelecSubscriberByID(SubscriberID);
            Subscriber_M subscriber_m = new Subscriber_M()
            {
                Address = subscriber.Address,
                BusinessName = subscriber.BusinessName,
                City = subscriber.City,
                ContactName = subscriber.ContactName,
                EmailID = subscriber.EmailID,
                Fax = subscriber.Fax,
                Mobile = subscriber.Mobile,
                Phone = subscriber.Phone,
                PostalCode = subscriber.PostalCode,
                State = subscriber.State,
                ImagePath = subscriber.LogoFile,
                SubscriberID = subscriber.SubscriberID,
                SubscriberPayments = (from payment in subscriber.SubscriberPayments
                                      select new SubscriberPayment_M
                                      {
                                          Amount = payment.Amount,
                                          DatePaid = payment.DatePaid,
                                          ExpirationDate = payment.ExpirationDate,
                                          SubsciberPaymentID = payment.SubsciberPaymentID
                                      }).ToList(),
                SubscriberUsers = (from user in subscriber.SubscriberUsers
                                   select new SubscriberUser_M
                                   {
                                       SubscriberUserID = user.SubscriberUserID,
                                       UserID = user.UserID,
                                       UserName = user.UserDetail.UserName,
                                       RoleNames = user.UserDetail.UserRoles.Count > 0 ? String.Join("<br />", (from bb in user.UserDetail.UserRoles select Convert.ToString(bb.RoleMaster.RoleName)).ToArray()) : "",

                                   }).ToList()
            };
            return subscriber_m;
        }

        public ActionResult DeletePayment(int PaymentID)
        {
            DeleteSubscriber deletesubscriber = new DeleteSubscriber();
            SubscriberPayment payment = deletesubscriber.DeletePayment(PaymentID);

            //get latest payment after deleting
            Subscriber_M subscriber_m = SelectPaymentModelByID(payment.SubscriberID);
            return PartialView("PaymentList", subscriber_m);
        }

        public JsonResult GetPaymentByID(int PaymentID)
        {
            SubscriberPayment payment = SelectSubscriber.SelectSubscriberPaymentByID(PaymentID);

            SubscriberPayment_M model = new SubscriberPayment_M()
            {
                Amount = payment.Amount,
                DatePaid = payment.DatePaid,
                ExpirationDate = payment.ExpirationDate,
                SubsciberPaymentID = payment.SubsciberPaymentID
            };

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowUser(int SubscriberID)
        {
            Subscriber_M subscriber_m = SelectPaymentModelByID(SubscriberID);

            return PartialView("UserList", subscriber_m);
        }

        public ActionResult DeleteUser(int SubscriberUserID)
        {
            DeleteSubscriber deletesubscriber = new DeleteSubscriber();
            SubscriberUser user = deletesubscriber.DeleteUser(SubscriberUserID);

            //get latest payment after deleting
            Subscriber_M subscriber_m = SelectPaymentModelByID(user.SubscriberID);
            return PartialView("UserList", subscriber_m);

        }


        #endregion

        #region "Post methods"
        [HttpPost]
        public ActionResult SaveRecord(Subscriber_M subscriber_m, SubscriberSearch search, string ReturnUrl)
        {
            ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Request.UrlReferrer.ToString() : ReturnUrl;

            if (ModelState.IsValid) //if valid model then update the record and show list
            {
                Subscriber subscriber = new Subscriber()
                {
                    Address = subscriber_m.Address,
                    BusinessName = subscriber_m.BusinessName,
                    City = subscriber_m.City,
                    ContactName = subscriber_m.ContactName,
                    EmailID = subscriber_m.EmailID,
                    Fax = subscriber_m.Fax,
                    Mobile = subscriber_m.Mobile,
                    Phone = subscriber_m.Phone,
                    PostalCode = subscriber_m.PostalCode,
                    State = subscriber_m.State,
                    SubscriberID = subscriber_m.SubscriberID,
                    Active = subscriber_m.Active,
                    LogoFile = subscriber_m.ImagePath,
                };

                if (subscriber_m.Image != null)
                {
                    subscriber.LogoFile = subscriber_m.Image.FileName;
                }

                if (subscriber.SubscriberID == 0)
                {
                    CreateSubscriber createsubscriber = new CreateSubscriber();
                    subscriber.CreatedBy = SessionHelper.UserID;
                    subscriber.CreatedDate = DateTime.Now;
                    createsubscriber.Do(subscriber);
                    subscriber_m.SubscriberID = subscriber.SubscriberID;
                }
                else
                {
                    UpdateSubscriber updatesubscriber = new UpdateSubscriber();
                    subscriber.UpdatedBy = SessionHelper.UserID;
                    subscriber.UpdatedDate = DateTime.Now;
                    updatesubscriber.Do(subscriber);
                }

                if (subscriber_m.Image != null)
                {
                    var fullPath = Server.MapPath("~/SubscriberFiles/" + subscriber_m.SubscriberID + "/" + subscriber_m.Image.FileName.Replace(' ','_'));
                    // resize image to make sure it's no larger than say 400px wide.
                    using (var ms = new MemoryStream())
                    {
                        subscriber_m.Image.InputStream.CopyTo(ms);
                        byte[] array = ms.GetBuffer();

                        byte[] resizedImage = ImageResize.CreateThumbnail(array, 300);

                        // Create folder if not exists
                        System.IO.FileInfo file = new System.IO.FileInfo(fullPath);
                        file.Directory.Create();

                        if (resizedImage != null)
                            System.IO.File.WriteAllBytes(fullPath, resizedImage);
                    }
                }

            }
            ViewBag.ReturnUrl = ReturnUrl;
            Session["Message"] = "Subscription Saved Successfully";
            return Redirect(ReturnUrl);
        }

        [HttpPost]
        public ActionResult AjaxListing(SubscriberSearch search)
        {
            IPagedList<Subscriber_M> subscriberPaged = SelectSubscriberFilterBased(search);
            return PartialView("Listing", subscriberPaged);
        }

        [HttpPost]
        public ActionResult SavePayment(SubscriberPayment_M subscriber_m, int SubscriberID)
        {


            SubscriberPayment payment = new SubscriberPayment()
            {
                Amount = subscriber_m.Amount,
                DatePaid = subscriber_m.DatePaid,
                ExpirationDate = subscriber_m.ExpirationDate,
                SubscriberID = SubscriberID,
                SubsciberPaymentID = subscriber_m.SubsciberPaymentID
            };

            if (payment.SubsciberPaymentID == 0)
            {
                payment.CreatedBy = SessionHelper.UserID;
                payment.CreatedDate = DateTime.Now;

                CreateSubscriber createsubscriber = new CreateSubscriber();
                createsubscriber.CreatePayment(payment);
            }
            else
            {
                UpdateSubscriber udpatesubscriber = new UpdateSubscriber();
                payment.UpdatedBy = SessionHelper.UserID;
                payment.UpdatedDate = DateTime.Now;
                udpatesubscriber.UpdatePayment(payment);
            }
            Subscriber_M subscriberm = SelectPaymentModelByID(payment.SubscriberID);
            return PartialView("PaymentList", subscriberm);
            //}

            //this.Response.StatusCode = 400;
            //Subscriber_M subscribermm = SelectPaymentModelByID(SubscriberID);
            //return PartialView("PaymentList", subscribermm);
        }

        [HttpPost]

        public ActionResult SaveUser(SubscriberUser_M user)
        {
            SubscriberUser subuser = new SubscriberUser()
            {
                SubscriberID = user.SubscriberID,
                UserID = user.UserID,
                CreatedBy = SessionHelper.UserID
            };

            CreateSubscriber createsubscriber = new CreateSubscriber();
            createsubscriber.CreateUser(subuser);

            Subscriber_M subscriber_m = SelectPaymentModelByID(user.SubscriberID);
            return PartialView("UserList", subscriber_m);

        }

        [HttpGet]
        public ActionResult Current()
        {
            Subscriber subscriber = SelectSubscriber.SelecSubscriberByID(SessionHelper.SubscriptionID);

            Subscriber_M subscriber_m = new Subscriber_M()
            {
                Address = subscriber.Address,
                BusinessName = subscriber.BusinessName,
                City = subscriber.City,
                ContactName = subscriber.ContactName,
                EmailID = subscriber.EmailID,
                Fax = subscriber.Fax,
                Mobile = subscriber.Mobile,
                Phone = subscriber.Phone,
                PostalCode = subscriber.PostalCode,
                State = subscriber.State,
                SubscriberID = subscriber.SubscriberID,
                ImagePath = subscriber.LogoFile,
                ResolvedLogoFile = String.IsNullOrEmpty(subscriber.LogoFile) ? "" : Url.Content(subscriber.LogoFile)
            };

            ViewBag.ReturnUrl = Request.Url;

            return View("AddEdit", subscriber_m);
        }

        [HttpPost]
        public JsonResult RemoveImage(int id, string ReturnUrl)
        {
            UpdateSubscriber updatesub = new UpdateSubscriber();
            updatesub.RemoveImage(id, SessionHelper.UserID);

            return new JsonResult()
            {
                Data = true
            };
        }


        #endregion
    }
}