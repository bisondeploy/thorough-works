﻿using MvcPaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThoroughWorks.Data;
using ThoroughWorks.Repository.CardManagement;
using ThoroughWorks.Repository.UserHorseSaleManagement;
using ThoroughWorks.Repository.UserSaleLocationManagement;
using ThoroughWorks.Models;
using ThoroughWorks.Web.Models.Helpers;

namespace ThoroughWorks.Web.Controllers
{
    [Authorize]
    public class SaleLocationController : Controller
    {
        #region "get methods"

        public ActionResult Index()
        {
            UserSaleLocationSearch usersalelocationsearch = new UserSaleLocationSearch();

            //if (Helpers.isAdmin() == false)
            //    usersalelocationsearch.SearchUserID = Helpers.ConvertString(SessionHelper.UserID);

            usersalelocationsearch.SearchUserID = Helpers.ConvertString(SessionHelper.UserID);
            IPagedList<UserSaleLocation_M> saleLocationPaged = SelectSalesLocationFilterBased(usersalelocationsearch);
            return View(saleLocationPaged);
        }

        [HttpGet]
        public ActionResult Add()
        {
            var model = new UserSaleLocation_M() {
                UserID = SessionHelper.UserID,
                SubscriberID= SessionHelper.SubscriptionID,
                CreatedBy = SessionHelper.UserID,
                CreatedDate =DateTime.Now,
                Active = true,  
            };

            return View("AddEdit", model);
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            UserSaleLocation usersalelocation = SelectUserSaleLocation.SelectUserSaleLocationByID(id);

            UserSaleLocation_M usersalelocation_m = new UserSaleLocation_M()
            {
                Active = usersalelocation.Active,
                UserName = usersalelocation.UserDetail != null ? usersalelocation.UserDetail.UserName : "",
                UserSaleLocationID = usersalelocation.UserSaleLocationID,
                UserID = usersalelocation.UserID,
                SubscriberID = usersalelocation.SubscriberID,
                UpdatedDate = usersalelocation.UpdatedDate,
                UpdatedBy = usersalelocation.UpdatedBy,
                StartDate = usersalelocation.StartDate,
                SaleName = usersalelocation.SaleName,
                Location = usersalelocation.Location,
                EndDate = usersalelocation.EndDate,
                NumberSessions = usersalelocation.NumberSessions,
                CreatedDate = usersalelocation.CreatedDate,
                CreatedBy = usersalelocation.CreatedBy
            };
            return View("AddEdit", usersalelocation_m);
        }

        [HttpPost]
        public ActionResult DeleteRecord(int LocationID)
        {
            DeleteUserSaleLocation deleteusersalelocation = new DeleteUserSaleLocation();
            deleteusersalelocation.Do(LocationID);

            IPagedList<UserSaleLocation_M> locationPaged = SelectSalesLocationFilterBased(new UserSaleLocationSearch());
            return PartialView("Listing", locationPaged);
        }

        private IPagedList<UserSaleLocation_M> SelectSalesLocationFilterBased(UserSaleLocationSearch search)
        {
            int currentPageIndex = search.Page.HasValue ? search.Page.Value - 1 : 0;
            List<UserSaleLocation> sales = (from slocation in SelectUserSaleLocation.SelectAllUserSaleLocation(SessionHelper.SubscriptionID)
                                            where ((String.IsNullOrWhiteSpace(search.SearchName) || Helpers.ConvertString(slocation.SaleName).ToLower().Contains(Helpers.ConvertString(search.SearchName).ToLower())))
                                            //&& (String.IsNullOrWhiteSpace(search.SearchUserID) || Helpers.ConvertString(slocation.UserID).ToLower().Contains(Helpers.ConvertString(search.SearchUserID).ToLower())))
                                            select slocation).ToList();

            List<UserSaleLocation_M> locationmodel = (from aa in sales
                                                      select new UserSaleLocation_M
                                                      {
                                                          Active = aa.Active,
                                                          CreatedBy = aa.CreatedBy,
                                                          CreatedDate = aa.CreatedDate,
                                                          EndDate = aa.EndDate,
                                                          SaleName = aa.SaleName,
                                                          Location = aa.Location,
                                                          StartDate = aa.StartDate,
                                                          NumberSessions = aa.NumberSessions,
                                                          UpdatedBy = aa.UpdatedBy,
                                                          UpdatedDate = aa.UpdatedDate,
                                                          UserID = aa.UserID,
                                                          SubscriberID = aa.SubscriberID,
                                                          UserSaleLocationID = aa.UserSaleLocationID,
                                                          UserName = aa.UserDetail != null ? aa.UserDetail.UserName : "",
                                                          Horses = (from hors in aa.UserHorseSales
                                                                    select new UserHorseSale_M
                                                                    {
                                                                        HorseID = hors.HorseID,
                                                                        LotNumber = hors.LotNumber,
                                                                        SessionNumber = hors.SessionNumber,
                                                                        NumberSessions = aa.NumberSessions,
                                                                        Comments = hors.Comments,
                                                                        Sequence = hors.Sequence,
                                                                        UserHorseSaleID = hors.UserHorseSaleID,
                                                                        UserSaleLocationID = hors.UserSaleLocationID
                                                                    }).ToList()
                                                      }).ToList();
            IPagedList<UserSaleLocation_M> locationPaged = locationmodel.OrderBy(j => j.UserID).ToPagedList(currentPageIndex, 10);
            return locationPaged;
        }

        [HttpGet]
        public ActionResult ViewersHistory(int LocationID)
        {
            UserSaleLocation usersalelocation = SelectUserSaleLocation.SelectUserSaleLocationByID_LazyLoading(LocationID);

            UserSaleLocation_M usersalelocation_m = new UserSaleLocation_M()
            {
                Active = usersalelocation.Active,
                UserName = usersalelocation.UserDetail != null ? usersalelocation.UserDetail.UserName : "",
                UserSaleLocationID = usersalelocation.UserSaleLocationID,
                UserID = usersalelocation.UserID,
                SubscriberID = usersalelocation.SubscriberID,
                UpdatedDate = usersalelocation.UpdatedDate,
                UpdatedBy = usersalelocation.UpdatedBy,
                StartDate = usersalelocation.StartDate,
                SaleName = usersalelocation.SaleName,
                Location = usersalelocation.Location,
                EndDate = usersalelocation.EndDate,
                NumberSessions = usersalelocation.NumberSessions,
                CreatedDate = usersalelocation.CreatedDate,
                CreatedBy = usersalelocation.CreatedBy,
                Horses = (from hors in usersalelocation.UserHorseSales
                          select new UserHorseSale_M
                          {
                              HorseID = hors.HorseID,
                              SessionNumber = hors.SessionNumber,
                              LotNumber = hors.LotNumber,
                              Comments = hors.Comments,
                              Sequence = hors.Sequence,
                              UserHorseSaleID = hors.UserHorseSaleID,
                              UserSaleLocationID = hors.UserSaleLocationID,
                              HorseName = hors.Horse.Name,
                              UserSaleLocationName = hors.UserSaleLocation.SaleName,
                              HorseImage = hors.Horse.Image,
                              Viewers = (from viewer in hors.UserSaleLocation.ViewLogHistories
                                         where viewer.HorseID == hors.HorseID
                                         select new ViewLogHistory_M
                                         {
                                             UserID = viewer.UserID,
                                             UserName = viewer.UserDetail.UserName,
                                             ViewingDate = viewer.CreatedDate
                                         }).ToList()
                          }).ToList()
            };
            return View("HorseViewer", usersalelocation_m);
        }
        #endregion

        #region "Post methods"
        [HttpPost]
        public ActionResult SaveRecord(UserSaleLocation_M usersalelocation_m, UserSaleLocationSearch search)
        {
            if (ModelState.IsValid) //if valid model then update the record and show list
            {
                try
                {
                    UserSaleLocation usersalelocation = new UserSaleLocation()
                    {
                        Active = true,
                        UserSaleLocationID = usersalelocation_m.UserSaleLocationID,
                        SaleName = usersalelocation_m.SaleName,
                        UserID = usersalelocation_m.UserID,
                        Location = usersalelocation_m.Location,
                        EndDate = usersalelocation_m.EndDate,
                        StartDate = usersalelocation_m.StartDate,
                        NumberSessions = usersalelocation_m.NumberSessions,
                        SubscriberID = usersalelocation_m.SubscriberID,
                    };



                    if (usersalelocation.UserSaleLocationID == 0)
                    {
                        CreateUserSaleLocation createusersalelocation = new CreateUserSaleLocation();
                        usersalelocation.CreatedBy = SessionHelper.UserID;
                        usersalelocation.CreatedDate = DateTime.Now;
                        usersalelocation.UserID = SessionHelper.UserID;
                        createusersalelocation.Do(usersalelocation);
                    }
                    else
                    {
                        UpdateUserSaleLocation updateusersalelocation = new UpdateUserSaleLocation();
                        usersalelocation.UpdatedBy = SessionHelper.UserID;
                        usersalelocation.UpdatedDate = DateTime.Now;

                        updateusersalelocation.Do(usersalelocation);

                    }


                    IPagedList<UserSaleLocation_M> locationPaged = SelectSalesLocationFilterBased(search);

                    Session["Message"] = "Sale Location Saved Successfully";
                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                }
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult AjaxListing(UserSaleLocationSearch search)
        {
            IPagedList<UserSaleLocation_M> locationPaged = SelectSalesLocationFilterBased(search);
            return PartialView("Listing", locationPaged);
        }
        #endregion

        #region Sale-Horses
        //
        // GET: /SaleLocation/Horses
        public ActionResult Horses(int id)
        {
            ViewBag.SubscriberID = SessionHelper.SubscriptionID;
            List<UserHorseSale_M> lst = SelectUserHorseSaleFilterBased(id);
            UserSaleLocation usersalelocation = SelectUserSaleLocation.SelectUserSaleLocationByID(id);
            SetLocationName(id);

            ViewBag.NumberSessions = usersalelocation.NumberSessions;
            return View(lst);
        }

        [HttpPost]
        public ActionResult Horses(List<UserHorseSale_M> model, int LocationID)
        {
            ViewBag.SubscriberID = SessionHelper.SubscriptionID;
            // pass over to logic layer to save
            SetLocationName(LocationID);
            foreach (var item in model.Where(x => x.UserSaleLocationID == 0).ToList())
                item.UserSaleLocationID = LocationID;

            foreach (var item in model.Where(x => x.SessionNumber == 0).ToList())
                item.SessionNumber = 1;

            foreach (var item in model.Where(x => x.Sequence == 0).ToList())
                item.Sequence = model.Max(x => x.Sequence) + 1;


            if (ModelState.IsValid)
            {
                try
                {
                    var update = new SaveUserHorseSale(model, (int)ViewBag.SearchUserID);
                    if (update.Do())
                    {
                        Session["Message"] = "Horse List Saved Successfully";
                        return RedirectToAction("Horses", LocationID);
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                }
            }
            return View(model);
        }

        private void SetLocationName(int LocationID)
        {
            UserSaleLocation salelocation = SelectUserSaleLocation.SelectUserSaleLocationByID(LocationID);
            ViewBag.SearchUserSaleLocationID = LocationID;
            ViewBag.SaleLocationName = salelocation.SaleName;
            ViewBag.SaleLocationID = LocationID;
            ViewBag.SearchUserID = SessionHelper.UserID;
        }

        private List<UserHorseSale_M> SelectUserHorseSaleFilterBased(int LocationID)
        {
            //int currentPageIndex = search.page.HasValue ? search.page.Value - 1 : 0;
            List<UserHorseSale_M> saleshorse = SelectUserHorseSale.SelectUserHorseSaleByLocation(LocationID).Select(aa => new UserHorseSale_M
            {
                HorseID = aa.HorseID,
                HorseName = aa.Horse != null ? aa.Horse.Name : "",
                UserSaleLocationID = aa.UserSaleLocationID,
                UserSaleLocationName = aa.UserSaleLocation != null ? aa.UserSaleLocation.SaleName : "",
                UserHorseSaleID = aa.UserHorseSaleID,
                SessionNumber = aa.SessionNumber,
                NumberSessions = aa.UserSaleLocation.NumberSessions,
                LotNumber = aa.LotNumber,
                Comments = aa.Comments,
                Delete = false,
                Sequence = aa.Sequence
            }).OrderBy(j => j.Sequence).ToList();

            return saleshorse;
        }


        #endregion

    }
}