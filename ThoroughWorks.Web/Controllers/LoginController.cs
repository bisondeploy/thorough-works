﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using ThoroughWorks.Data;
using ThoroughWorks.Repository.SubscriberManagement;
using ThoroughWorks.Repository.UserManagement;
using ThoroughWorks.Models;
using ThoroughWorks.Web.Models.Helpers;

namespace ThoroughWorks.Web.Controllers
{
    public class LoginController : Controller
    {
        #region "GetRequest"
        //
        // GET: /Login/
        public ActionResult Index()
        {
            Session.Abandon();

            var model = new Login_M();
            return View(model);
        }

        [HttpGet]
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "Login");
        }

        [HttpGet]
        [Authorize]
        public ActionResult ChangePassword()
        {
            int UserID = SessionHelper.UserID;
            UserDetail userdetail = SelectUserDetails.SelectByUserID(UserID);
            Password_M password = new Password_M() { UserID = userdetail.UserID };
            //return PartialView("PasswordChange", password);
            return View(password);
        }
        #endregion

        #region "Post request"
        [HttpPost]
        public ActionResult Index(Login_M model)
        {

            if (ModelState.IsValid)
            {
                //var functionResult = new FunctionResult();
                bool success = false;

                string RoleMasterID = "";
                string DisplayName = "";
                int UserID = 0;
                int SubscriptionID = 0;

                UserDetail userdetail = SelectUserDetails.SelectUserByUserNamePassword(model.Username, model.Password);
                if (userdetail != null)
                {
                    if (userdetail.UserRoles.Any(x => x.RoleMasterID == (int)EnumHelper.Roles.SuperAdmin))
                    {
                        RoleMasterID = userdetail.UserRoles.Count > 0 ? String.Join(",", (from aa in userdetail.UserRoles select Convert.ToString(aa.RoleMasterID)).ToArray()) : "";
                        DisplayName = userdetail.FirstName + " " + userdetail.LastName;
                        UserID = userdetail.UserID;
                        SubscriptionID = 0;
                        success = true;

                        //functionResult.Success = true;
                    }

                    else
                    {
                        // password is correct, has the subscription expired or been deleted?
                        if (SelectSubscriber.IsSubscriptionValidUserID(userdetail.UserID))
                        {

                            UserID = userdetail.UserID;
                            SubscriptionID = userdetail.SubscriberUsers.FirstOrDefault().SubscriberID;
                            RoleMasterID = userdetail.UserRoles.Count > 0 ? String.Join(",", (from aa in userdetail.UserRoles select Convert.ToString(aa.RoleMasterID)).ToArray()) : "";
                            DisplayName = userdetail.FirstName + " " + userdetail.LastName;

                            success = true;

                        }
                        else
                        {
                            ModelState.AddModelError("", "Your subscription has expired");
                            success = false;
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Please enter a valid username and password");
                    success = false;
                }

                if (success)
                {

                    FormsAuthentication.SetAuthCookie(model.Username, model.RememberMe);

                    // Create the cookie that contains the forms authentication ticket
                    HttpCookie authCookie = FormsAuthentication.GetAuthCookie(model.Username, model.RememberMe);
                    // Get the FormsAuthenticationTicket out of the encrypted cookie
                    FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(authCookie.Value);

                    DateTime expirationDate = ticket.Expiration;

                    if (model.RememberMe)
                        expirationDate = DateTime.MaxValue;


                    FormsAuthenticationTicket newTicket = new FormsAuthenticationTicket(ticket.Version, ticket.Name, ticket.IssueDate, expirationDate, ticket.IsPersistent, String.Format("{0}|{1}|{2}|{3}", UserID.ToString(), SubscriptionID.ToString(), DisplayName, RoleMasterID));

                    // Update the authCookie's Value to use the encrypted version of newTicket
                    authCookie.Value = FormsAuthentication.Encrypt(newTicket);
                    // Manually add the authCookie to the Cookies collection

                    Response.Cookies.Add(authCookie);

                    return RedirectToAction("Index", "Dashboard");
                }
            }
            return View(model);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ChangePassword(Password_M model)
        {
            if (ModelState.IsValid) //if valid model then update the record and show list
            {
                try
                {
                    UpdateUserDetails updateUserDetail = new UpdateUserDetails();
                    UserDetail userdetail = new UserDetail() { UserID = model.UserID, Password = model.NewPassword };

                    if (!updateUserDetail.CheckPassword(model.UserID, model.CurrentPassword))
                    {
                        ModelState.AddModelError(string.Empty, "The existing password entered is incorrect. Please try again");
                    }
                    else
                    {

                        updateUserDetail.ChangePassword(userdetail);

                        Session["Message"] = "Your password has been updated";
                        return RedirectToAction("Index", "Dashboard");
                    }
                    
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                }
            }
            return View(model);
        }
        #endregion

    }

    public struct FunctionResult
    {
        public bool Success;
        public string Error;
    }

}