﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcPaging;
using ThoroughWorks.Data;
using ThoroughWorks.Repository.UserSaleLocationManagement;
using ThoroughWorks.Repository.UserHorseSaleManagement;
using ThoroughWorks.Models;
using ThoroughWorks.Web.Models.Helpers;
using ThoroughWorks.Repository.HorseManagement;
using ThoroughWorks.Repository.ViewLogHistoryManagement;
using ThoroughWorks.Repository.CardManagement;
using ThoroughWorks.Repository.OwnerManagement;
using ThoroughWorks.Repository.UserManagement;
using Postal;
using System.Net.Mail;
using CommunicationsManager;
using System.Configuration;
using ThoroughWorks.Repository.SubscriberManagement;

namespace ThoroughWorks.Web.Controllers
{
    [Authorize]
    public class SaleCardController : Controller
    {
        #region "Get Methods"
        public ActionResult Index(int id, string DateFrom = "", string DateTo = "", string ReturnUrl = "")
        {
            DateTime? fromDate;
            DateTime? toDate;

            // Set start date
            if (!String.IsNullOrEmpty(DateFrom))
            {
                fromDate = DateTime.Parse(DateFrom);
            }
            else
            {
                var sale = SelectUserSaleLocation.SelectUserSaleLocationByID(id);
                if (DateTime.Today >= sale.StartDate && DateTime.Today <= sale.EndDate)
                {
                    fromDate = DateTime.Today;
                }
                else
                {
                    fromDate = DateTime.MinValue;
                }
            }

            // set end date
            if (!String.IsNullOrEmpty(DateTo))
            {
                toDate = DateTime.Parse(DateTo);
            }
            else
            {
                var sale = SelectUserSaleLocation.SelectUserSaleLocationByID(id);
                if (DateTime.Today >= sale.StartDate && DateTime.Today <= sale.EndDate)
                {
                    toDate = sale.EndDate.HasValue ? sale.EndDate.Value : DateTime.MaxValue.AddDays(-1);
                }
                else
                {
                    toDate = DateTime.MaxValue.AddDays(-1);
                }
            }

            //IPagedList<SaleCard> lst = FilterRecord(new SaleCardSearch());
            List<Card> lst = SelectCard.SelectCardByLocation(id, fromDate.Value, toDate.Value.AddDays(1).AddTicks(-1));
            List<Card_M> cardm = (from card in lst
                                  select new Card_M
                                  {
                                      CardID = card.CardID,
                                      PersonName = card.PersonName,
                                      UserID = card.UserID,
                                      SubscriberID = card.SubscriberID,
                                      UserName = card.UserDetail.UserName,
                                      SaleName = card.UserSaleLocation.SaleName,
                                      UserRefNo = card.UserRefNo,
                                      UserSaleLocationID = card.UserSaleLocationID,
                                      CardUID = card.CardUID,
                                      InspectionDate= card.InspectionDate,
                                      Horses = (from hs in card.CardHorses
                                                select new Horse_M
                                                {
                                                    Active = hs.Horse.Active,
                                                    AttachmentsPath = hs.Horse.Attachments,
                                                    Comments = hs.Horse.Comments,
                                                    Dam = hs.Horse.Dam,
                                                    DateArrived = hs.Horse.DateArrived,
                                                    DateSold = hs.Horse.DateSold,
                                                    DOB = hs.Horse.DOB,
                                                    HorseID = hs.Horse.HorseID,
                                                    ImagePath = hs.Horse.Image,
                                                    Name = hs.Horse.Name,
                                                    Owners = hs.Horse.HorseOwners.Select(o => new HorseOwner_M()
                                                    {
                                                        HorseOwnerID = o.HorseOwnerID,
                                                        HorseID = o.HorseID,
                                                        Active = o.Owner.Active,
                                                        ContactNumber = o.Owner.ContactNumber,
                                                        EmailAddress = o.Owner.EmailAddress,
                                                        Name = o.Owner.Name,
                                                        OwnerID = o.Owner.OwnerID,
                                                        SubscriberID = o.Owner.SubscriberID,
                                                        Delete = false
                                                    }).ToList(),
                                                    SireID = (int) hs.Horse.SireID,
                                                    SireDisplay = hs.Horse.Sire.SireName,
                                                }).ToList()
                                  }).OrderByDescending(j => j.Horses.Count).OrderByDescending(j => j.PersonName).ToList();

            SetLocationName(id);

            ViewBag.FromDateField = fromDate.Value;
            ViewBag.ToDateField = toDate.Value;

            ViewBag.DateFrom = fromDate.Value.ToString("yyyyMMdd");
            ViewBag.DateTo = toDate.Value.ToString("yyyyMMdd");

            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Url.Action("Index", "Dashboard") : Server.UrlDecode(ReturnUrl);

            return View(cardm);
        }

        private void SetLocationName(int LocationID)
        {
            UserSaleLocation salelocation = SelectUserSaleLocation.SelectUserSaleLocationByID(LocationID);
            ViewBag.SearchUserSaleLocationID = LocationID;
            ViewBag.SaleLocationName = salelocation.SaleName;
            ViewBag.SaleLocationID = LocationID;
            ViewBag.SearchUserID = SessionHelper.UserID;
        }

        public ActionResult Add(int id, string ReturnUrl = "")
        {
            UserSaleLocation location = SelectUserSaleLocation.SelectUserSaleLocationByID_LazyLoading(id);
            SaleCard card = new SaleCard()
            {
                UserSaleLocationID = location.UserSaleLocationID,
                SaleName = location.SaleName,
                NumberSessions = location.NumberSessions,
                Active = true,
                SubscriberID = SessionHelper.SubscriptionID,
                UserID = SessionHelper.UserID,
                InspectionDate = DateTime.Now.Date,
                Horses = (from aa in location.UserHorseSales
                          select new SaleCardHorses
                          {
                              HorseImage = aa.Horse.Image,
                              HorseName = aa.Horse.Name,
                              HorseID = aa.Horse.HorseID,
                              Sequence = aa.Sequence,
                              DOB = aa.Horse.DOB,
                              Comment = aa.Horse.Comments,
                              AttachmentsPath = aa.Horse.Attachments,
                              Dam = aa.Horse.Dam,
                              Sire = aa.Horse.Sire.SireName,
                              Owners = aa.Horse.HorseOwners.Select(o => new HorseOwner_M()
                              {
                                  HorseOwnerID = o.HorseOwnerID,
                                  HorseID = o.HorseID,
                                  Active = o.Owner.Active,
                                  ContactNumber = o.Owner.ContactNumber,
                                  EmailAddress = o.Owner.EmailAddress,
                                  Name = o.Owner.Name,
                                  OwnerID = o.Owner.OwnerID,
                                  SubscriberID = o.Owner.SubscriberID,
                                  Delete = false
                              }).ToList(),
                              DateArrived = aa.Horse.DateArrived,
                              DateSold = aa.Horse.DateSold,
                              Sex = aa.Horse.Sex,
                              Colour = aa.Horse.Colour,
                              LotNumber = aa.LotNumber,
                              SessionNumber = aa.SessionNumber
                          }).OrderBy(j => j.LotNumber).ThenBy(j => j.Sequence).ToList()
            };
            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Url.Action("Index", "Dashboard") : Server.UrlDecode(ReturnUrl);
            return View("AddEdit", card);

        }

        public ActionResult ChooseSale(string ReturnUrlType = "", bool forceDisplaySales = false)
        {
            switch (ReturnUrlType.ToLower())
            {
                case "cards":
                    ViewBag.ReturnUrl = "/SaleCard/Index/{0}";
                    break;
                default:
                    ViewBag.ReturnUrl = "/Dashboard";
                    break;
            }

            //int currentPageIndex = search.page.HasValue ? search.page.Value - 1 : 0;
            List<UserSaleLocation> sales = (from slocation in SelectUserSaleLocation.SelectAllUserSaleLocation(SessionHelper.SubscriptionID)
                                            //where ((String.IsNullOrWhiteSpace(search.SearchName) || Helpers.ConvertString(slocation.SaleName).ToLower().Contains(Helpers.ConvertString(search.SearchName).ToLower())))
                                            //&& (String.IsNullOrWhiteSpace(search.SearchUserID) || Helpers.ConvertString(slocation.UserID).ToLower().Contains(Helpers.ConvertString(search.SearchUserID).ToLower())))
                                            select slocation).ToList();

            if (ReturnUrlType == "cards" && !forceDisplaySales)
            {
                // if we are in a current sale
                var currentSales = sales.Where(x => x.Active && x.StartDate.HasValue && (x.StartDate ?? DateTime.Today).AddDays(-7) <= DateTime.Now && ((x.EndDate ?? DateTime.Now).AddDays(7) >= DateTime.Now)).ToList();

                // and there's only 1
                if (currentSales.Count() == 1)
                {
                    // go to it automatically
                    return RedirectToAction("Index", new { id = currentSales.First().UserSaleLocationID });
                }
            }

            List<UserSaleLocation_M> locationmodel = (from aa in sales
                                                      select new UserSaleLocation_M
                                                      {
                                                          Active = aa.Active,
                                                          CreatedBy = aa.CreatedBy,
                                                          CreatedDate = aa.CreatedDate,
                                                          EndDate = aa.EndDate,
                                                          SaleName = aa.SaleName,
                                                          Location = aa.Location,
                                                          StartDate = aa.StartDate,
                                                          NumberSessions = aa.NumberSessions,
                                                          UpdatedBy = aa.UpdatedBy,
                                                          UpdatedDate = aa.UpdatedDate,
                                                          SubscriberID = aa.SubscriberID,
                                                          UserID = aa.UserID,
                                                          UserSaleLocationID = aa.UserSaleLocationID,
                                                          UserName = aa.UserDetail != null ? aa.UserDetail.UserName : "",
                                                          Horses = (from hors in aa.UserHorseSales
                                                                    select new UserHorseSale_M
                                                                    {
                                                                        HorseID = hors.HorseID,
                                                                        LotNumber = hors.LotNumber,
                                                                        SessionNumber = hors.SessionNumber,
                                                                        NumberSessions = aa.NumberSessions,
                                                                        Comments = hors.Comments,
                                                                        Sequence = hors.Sequence,
                                                                        UserHorseSaleID = hors.UserHorseSaleID,
                                                                        UserSaleLocationID = hors.UserSaleLocationID
                                                                    }).AsEnumerable(),
                                                      }).ToList();
            List<UserSaleLocation_M> locations = locationmodel.OrderByDescending(j => j.StartDate).ToList();
            return View(locations);
        }

        public ActionResult Edit(int id, string ReturnUrl = "")
        {
            var card = SelectCard.SelectCardByID(id);

            UserSaleLocation location = SelectUserSaleLocation.SelectUserSaleLocationByID_LazyLoading(card.UserSaleLocationID);

            SaleCard saleCard = new SaleCard()
            {
                UserSaleLocationID = location.UserSaleLocationID,
                SaleName = location.SaleName,
                NumberSessions = location.NumberSessions,
                UserID = location.UserID,
                SubscriberID = location.SubscriberID,
                UserName = location.UserDetail.UserName,
                CardID = card.CardID,
                Active = card.Active,
                CardUID = card.CardUID,
                PersonName = card.PersonName,
                InspectionDate = card.InspectionDate,
                Horses = (from aa in location.UserHorseSales
                          select new SaleCardHorses
                          {
                              HorseImage = aa.Horse.Image,
                              HorseName = aa.Horse.Name,
                              HorseID = aa.Horse.HorseID,
                              Sequence = aa.Sequence,
                              DOB = aa.Horse.DOB,
                              Comment = aa.Horse.Comments,
                              AttachmentsPath = aa.Horse.Attachments,
                              Dam = aa.Horse.Dam,
                              Sire = aa.Horse.Sire.SireName,
                              Owners = aa.Horse.HorseOwners.Select(o => new HorseOwner_M()
                              {
                                  HorseOwnerID = o.HorseOwnerID,
                                  HorseID = o.HorseID,
                                  Active = o.Owner.Active,
                                  ContactNumber = o.Owner.ContactNumber,
                                  EmailAddress = o.Owner.EmailAddress,
                                  Name = o.Owner.Name,
                                  OwnerID = o.Owner.OwnerID,
                                  SubscriberID = o.Owner.SubscriberID,
                                  Delete = false
                              }).ToList(),
                              DateArrived = aa.Horse.DateArrived,
                              DateSold = aa.Horse.DateSold,
                              Sex = aa.Horse.Sex,
                              Colour = aa.Horse.Colour,
                              LotNumber = aa.LotNumber,
                              SessionNumber = aa.SessionNumber,
                              Selected = card.CardHorses.Any(x => x.HorseID == aa.HorseID),
                          }).OrderBy(j => j.LotNumber).ThenBy(j => j.Selected ? 0 : 1).ThenBy(j => j.Sequence).ToList()
            };

            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? Url.Action("Index", "Dashboard") : Server.UrlDecode(ReturnUrl);
            return View("AddEdit", saleCard);

        }


        public ActionResult CardConfirmation(int id)
        {
            var card = SelectCard.SelectCardByID(id);

            UserSaleLocation location = SelectUserSaleLocation.SelectUserSaleLocationByID_LazyLoading(card.UserSaleLocationID);

            SaleCard saleCard = new SaleCard()
            {
                UserSaleLocationID = location.UserSaleLocationID,
                SaleName = location.SaleName,
                NumberSessions = location.NumberSessions,
                UserID = location.UserID,
                SubscriberID = location.SubscriberID,
                UserName = location.UserDetail.UserName,
                CardID = card.CardID,
                CardUID = card.CardUID,
                Active = card.Active,
                PersonName = card.PersonName,
                InspectionDate = card.InspectionDate,
                //horses = (from aa in location.UserHorseSales
                //          select new SaleCardHorses
                //          {
                //              HorseImage = aa.Horse.Image,
                //              HorseName = aa.Horse.Name,
                //              HorseID = aa.Horse.HorseID,
                //              Sequence = aa.Sequence,
                //              DOB = aa.Horse.DOB,
                //              Comment = aa.Horse.Comments,
                //              AttachmentsPath = aa.Horse.Attachments,
                //              Dam = aa.Horse.Dam,
                //              Sire = aa.Horse.Sire,
                //              Owner = aa.Horse.Owner,
                //              DateArrived = aa.Horse.DateArrived,
                //              DateSold = aa.Horse.DateSold,
                //              Sex = aa.Horse.Sex,
                //              Colour = aa.Horse.Colour,
                //              LotNumber = aa.LotNumber,
                //              Selected = card.CardHorses.Any(x => x.HorseID == aa.HorseID),
                //          }).OrderBy(j => j.Selected ? 0 : 1).ThenBy(j => j.Sequence).ToList()
            };


            return View(saleCard);

        }

        public ActionResult SaleStatistics(int id, string ReturnUrl, int OwnerID = 0)
        {
            var model = SelectUserSaleLocation.SelectStatistics(id, OwnerID);

            if (OwnerID > 0)
                model.OwnerID = OwnerID;

            if (model.HorseViews.Any())
            {
                model.HorseViewsChartData = "";
                foreach (var horse in model.HorseViews.OrderByDescending(x => x.Views).ThenBy(x => x.LotNumber).ToList())
                {
                    model.HorseViewsChartData += "{" + String.Format("Horse: \"{0}\", HorseID: \"{2}\", Views: \"{1}\"", horse.HorseName, horse.Views, horse.HorseID) + "},";
                }
                model.HorseViewsChartData = model.HorseViewsChartData.TrimEnd(',');

            }
            ViewBag.HorseList = model.HorseViews.OrderBy(j => j.HorseName).Select(x => new SelectListItem()
            {
                Text = x.HorseName,
                Value = x.HorseID.ToString(),
            });

            ViewBag.OwnerList = SelectOwners.SelectAllActiveOwners(SessionHelper.SubscriptionID).OrderBy(j => j.Name).Select(x => new SelectListItem()
                {
                    Selected = OwnerID == x.OwnerID,
                    Text = x.Name,
                    Value = x.OwnerID.ToString(),
                });

            ViewBag.ReturnUrl = String.IsNullOrEmpty(ReturnUrl) ? "/Dashboard" : Server.UrlDecode(ReturnUrl);
            ViewBag.SaleName = model.SaleName;
            return View(model);
        }

        public ActionResult HorseViewerDetail(int saleID, int horseID)
        {
            var model = new SaleCardViewModel();
            model.HorseID = horseID;
            var horse = new SelectHorses().SelectHorseByID(horseID);
            model.HorseName = horse.Name;
            var sale = SelectUserSaleLocation.SelectUserSaleLocationByID(saleID);
            model.SaleID = saleID;
            model.Comments = sale.UserHorseSales.Where(h => h.Horse.HorseID == horseID).FirstOrDefault().Comments;
            model.UserHorseSaleID = sale.UserHorseSales.Where(h => h.Horse.HorseID == horseID).FirstOrDefault().UserHorseSaleID;
            model.Viewers = SelectUserSaleLocation.SelectSaleHorseViewers(saleID, horseID).OrderByDescending(x => x.ViewList.Sum(y => y.Views)).ThenBy(x => x.ViewerName).ToList();

            // sale dates
            var dates = new List<DateTime>();
            var thisDate = sale.StartDate.Value;
            while (thisDate <= sale.EndDate)
            {
                if (thisDate <= DateTime.Today)
                {
                    dates.Add(thisDate);
                }
                thisDate = thisDate.AddDays(1);
            }
            model.ViewDates = dates;
            model.SaleStartDate = sale.StartDate ?? model.ViewDates.Min();

            ViewBag.HorseID = horseID;

            return PartialView(model);
        }

        public ActionResult EmailOwners(int userSaleLocationID, string saleName)
        {
            ViewBag.UserSaleLocationID = userSaleLocationID;
            ViewBag.SaleName = saleName;
            OwnerSearch ownersearch = (OwnerSearch)TempData["ownersearch"];
            ownersearch = ownersearch ?? new OwnerSearch();
            ViewBag.Search = ownersearch;
            IPagedList<Owner_M> ownerPaged = SelectOwnerFilterBased(ownersearch, saleName, userSaleLocationID);
            return View(ownerPaged);
        }

        private IPagedList<Owner_M> SelectOwnerFilterBased(OwnerSearch search, string saleName, int userSaleLocationID)
        {
            int currentPageIndex = search.Page.HasValue ? search.Page.Value - 1 : 0;

            List<Owner> owners = (from o in SelectOwners.SelectOwnersByHorseSale(userSaleLocationID)
                                  where ((String.IsNullOrWhiteSpace(search.SearchName) || Helpers.ConvertString(o.Name).ToLower().Contains(search.SearchName.ToLower())))
                                  select o).ToList();

            List<Owner_M> ownermodel = (from aa in owners
                                        select new Owner_M
                                        {
                                            OwnerID = aa.OwnerID,
                                            Name = aa.Name,
                                            EmailAddress = aa.EmailAddress,
                                            ContactNumber = aa.ContactNumber,
                                            Active = aa.Active,
                                            OwnedHorses = aa.HorseOwners.Select(h => Horse_M.CreateInfo(h.HorseID, h.Horse.Name)).ToList(),
                                            IsSelected = false,
                                            Message = "Here is a quick update on your lots at the " + saleName,
                                            HorsesOnSale = SelectUserSaleLocation.SelectHorsesOnSaleByOwnerID(userSaleLocationID, aa.OwnerID).Select(h => Horse_M.CreateInfo(h.HorseID, h.Name)).ToList()
                                        }).ToList();
            IPagedList<Owner_M> ownerpage = ownermodel.OrderBy(j => j.OwnerID).ToPagedList(currentPageIndex, ownermodel.Count+1);
            return ownerpage;
        }

        [HttpPost]
        public ActionResult AjaxEmailOwnersListing(OwnerSearch search, string saleName, int userSaleLocationID)
        {
            IPagedList<Owner_M> ownerPaged = SelectOwnerFilterBased(search, saleName, userSaleLocationID);
            return PartialView("EmailOwnersListing", ownerPaged);
        }

        #endregion

        #region "Post methods"
        [HttpPost]
        public ActionResult Add(SaleCard model)
        {
            if (ModelState.IsValid)
            {
                // no card exists so we'll pass it over to the logic layer to save.
                var create = new CreateCard(model);
                int cardID = create.Do();

                if (cardID > 0)
                {
                    return RedirectToAction("CardConfirmation", new { id = cardID });
                }
            }
            //return View(model);
            return View("AddEdit", model);
        }

        [HttpPost]
        public ActionResult Edit(SaleCard model)
        {
            if (ModelState.IsValid)
            {
                try
                {

                    // no card exists so we'll pass it over to the logic layer to save.
                    var save = new SaveCard(model);
                    if (save.Do())
                    {
                        Session["Message"] = "Inspection Card Saved Successfully";
                        return RedirectToAction("Index", new { id = model.UserSaleLocationID });
                    }
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError("", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                }
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Remove(int CardID, int UserSaleLocationID)
        {
            if (CardID > 0)
            {
                var remove = new RemoveCard(CardID, SessionHelper.UserID);
                if (remove.Do())
                {
                    Session["Message"] = "Inspection Card Removed Successfully";
                    return RedirectToAction("Index", new { id = UserSaleLocationID });
                }

            }
            return RedirectToAction("Index", new { id = UserSaleLocationID });
        }

        [HttpPost]
        public ActionResult ExportEmail(string message, int selectedOwnerID, int userSaleLocationID, string saleName, string ThisUrl)
        {
            var o = SelectOwners.SelectOwnerByID(selectedOwnerID);
            var ownerModel = Owner_M.Create(o.OwnerID, o.Name, o.EmailAddress, o.ContactNumber, o.Active, true, message, 0, o.HorseOwners.Select(h => Horse_M.CreateInfo(h.HorseID, h.Horse.Name)).ToList(), SelectUserSaleLocation.SelectHorsesOnSaleByOwnerID(userSaleLocationID, o.OwnerID).Select(h => Horse_M.CreateInfo(h.HorseID, h.Name)).ToList());

            var ownersList = new List<Owner_M>();
            ownersList.Add(ownerModel);
            this.EmailOwners(ownersList, saleName, userSaleLocationID);

            Session["Message"] = "Email Sent to Owner Successfully";
            return Redirect(Server.UrlDecode(ThisUrl));
        }

        [HttpPost]
        public bool SaveSaleCardViewerComments(SaleCardViewModel model)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    UpdateUserSaleLocation.UpdateComments(model.UserHorseSaleID, model.Comments);
                }
                catch (Exception ex)
                {
                    return false;// ModelState.AddModelError("", ex.InnerException != null ? ex.InnerException.Message : ex.Message);
                }
            }
            return true;
        }

        [HttpPost]
        public ActionResult EmailOwners(IEnumerable<Owner_M> owners, string saleName, int userSaleLocationID)
        {
            try
            {
                int count = 0;
                foreach (var owner in owners)
                {
                    //Only send an email if the owner has been selected and has an email address
                    if (owner.IsSelected && !string.IsNullOrEmpty(owner.EmailAddress))
                    {
                        //Get the email model & templates
                        owner.HorsesOnSale = SelectUserSaleLocation.SelectHorsesOnSaleByOwnerID(userSaleLocationID, owner.OwnerID).Select(h => Horse_M.CreateInfo(h.HorseID, h.Name)).ToList();

                        Subscriber subscriber = SelectSubscriber.SelecSubscriberByID(SessionHelper.SubscriptionID);

                        string LogoFile = ConfigurationManager.AppSettings["DefaultLogoFile"];
                        if (!String.IsNullOrEmpty(subscriber.LogoFile))
                        {
                            LogoFile = String.Format("{0}://{1}/SubscriberFiles/{2}/{3}", Request.Url.Scheme, Request.Url.Host, subscriber.SubscriberID, subscriber.LogoFile);
                        }

                        UserDetail userDetail = SelectUserDetails.SelectByUserID(SessionHelper.UserID);
                        var emailSaleCard = new EmailSaleCardViewModel();
                        emailSaleCard.From = ConfigurationManager.AppSettings["CommunicationManager.DefaultMailUsername"];
                        emailSaleCard.FromName = SessionHelper.DisplayName + " - ThoroughWorks";
                        emailSaleCard.To = owner.EmailAddress;
                        emailSaleCard.Subject = "Your Sales Statistics - " + saleName;
                        emailSaleCard.CustomMessage = owner.Message;
                        emailSaleCard.UserName = SessionHelper.DisplayName;
                        emailSaleCard.OwnerName = owner.Name;
                        emailSaleCard.ResolvedLogoFile = String.IsNullOrEmpty(LogoFile) ? "" : LogoFile;
                        emailSaleCard.UserEmail = userDetail.EmailID;
                        emailSaleCard.EmailSignature = userDetail.EmailSignature;
                        emailSaleCard.SaleCards = new List<SaleCardViewModel>();
                        List<SaleCardViewModel> saleCards = new List<SaleCardViewModel>();
                        //Create a Sale Card for each horse the owner has on sale
                        foreach (var saleHorse in owner.HorsesOnSale)
                        {
                            var model = new SaleCardViewModel();
                            model.HorseID = saleHorse.HorseID;
                            var horse = new SelectHorses().SelectHorseByID(saleHorse.HorseID);
                            model.HorseName = horse.Name;
                            model.HorseImagePath = !string.IsNullOrEmpty(Convert.ToString(horse.Image)) ? new Uri(new Uri(Request.Url, Url.Content("/files/" + horse.HorseID.ToString() + "/" + Convert.ToString(horse.Image))).ToString()) : null;
                            var sale = SelectUserSaleLocation.SelectUserSaleLocationByID(userSaleLocationID);
                            model.SaleID = userSaleLocationID;
                            model.Comments = sale.UserHorseSales.Where(h => h.Horse.HorseID == saleHorse.HorseID).FirstOrDefault().Comments;
                            model.UserHorseSaleID = sale.UserHorseSales.Where(h => h.Horse.HorseID == saleHorse.HorseID).FirstOrDefault().UserHorseSaleID;
                            model.LotNumber = sale.UserHorseSales.Where(h => h.Horse.HorseID == saleHorse.HorseID).FirstOrDefault().LotNumber;
                            model.Viewers = SelectUserSaleLocation.SelectSaleHorseViewers(userSaleLocationID, saleHorse.HorseID).OrderByDescending(x => x.ViewList.Sum(y => y.Views)).ThenBy(x => x.ViewerName).ToList();

                            // sale dates
                            var dates = new List<DateTime>();
                            var thisDate = sale.StartDate.Value;
                            while (thisDate <= sale.EndDate)
                            {
                                if (thisDate <= DateTime.Today)
                                {
                                    dates.Add(thisDate);
                                }
                                thisDate = thisDate.AddDays(1);
                            }
                            model.ViewDates = dates;
                            model.SaleStartDate = sale.StartDate ?? model.ViewDates.Min();

                            emailSaleCard.SaleCards.Add(model);
                        }
                        //Sort the Sale Cards by Lot Number
                        emailSaleCard.SaleCards.Sort((sc1, sc2) => sc1.LotNumber.CompareTo(sc2.LotNumber));
                        this.Send(emailSaleCard);

                        count++;
                    }
                }

                Session["Message"] = count + " emails sent successfully";

            }
            catch (Exception ex)
            {
                Session["Message"] = ex.InnerException != null ? ex.InnerException.Message : ex.Message;
                Session["MessageClass"] = "danger";
            }
            return RedirectToAction("EmailOwners", "SaleCard", new { userSaleLocationID = userSaleLocationID, saleName = saleName });
        }

        private int failCount = 0;
        private int successCount = 0;

        public ActionResult Send(EmailSaleCardViewModel model)
        {
            try
            {
                var email = new EmailSaleCardViewModel
                {
                    To = model.To,
                    From = model.From,
                    FromName = model.FromName,
                    Subject = model.Subject,
                    UserName = model.UserName,
                    EmailSignature = model.EmailSignature,
                    OwnerName = model.OwnerName,
                    UserEmail = model.UserEmail,
                    CustomMessage = model.CustomMessage,
                    SaleCards = model.SaleCards,
                    ViewName = "EmailSaleCard",
                    ResolvedLogoFile = model.ResolvedLogoFile
                };

                IEmailService emailService = new EmailService();

                var mail = emailService.CreateMailMessage(email);
                mail.BodyEncoding = System.Text.Encoding.UTF8;
                mail.SubjectEncoding = System.Text.Encoding.UTF8;

                var dataStream = mail.AlternateViews[0].ContentStream;
                byte[] byteBuffer = new byte[dataStream.Length];
                var body = System.Text.Encoding.ASCII.GetString(byteBuffer, 0, dataStream.Read(byteBuffer, 0, byteBuffer.Length));

                //CommunicationManager.InsertIntoCommunicationQueue("Email", email.From, email.To, !string.IsNullOrEmpty(email.UserEmail) ? email.UserEmail : null, null, email.Subject, body, email.FromName, SessionHelper.UserID, null);
                CommunicationManager.InsertIntoCommunicationQueue("Email", email.From, email.To, null, null, email.Subject, body, email.FromName, SessionHelper.UserID, null);
                successCount++;
            }
            catch (Exception ex)
            {
                failCount++;
            }
            return null;
        }

        #endregion


        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult GetAllViewers(string selectviewer)
        {
            List<Viewer_M> resultsList = DropDownHelper.GetAllViewers(selectviewer, SessionHelper.SubscriptionID);

            var serialisedJson = from result in resultsList 
                                 select new
                                 {
                                     name = result.ViewerName, //each json object will have 
                                     id = result.ViewerName      //these two variables [name, id]
                                 };
            return Json(serialisedJson, JsonRequestBehavior.AllowGet);
        }

    }
}