﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThoroughWorks.Models;

namespace ThoroughWorks.Web.App_Code
{
  public static class HtmlExtensions
  {
    public static IHtmlString HorseNamesCommaDelimited(this HtmlHelper htmlHelper, List<Horse_M> horses)
    {
      return new HtmlString(string.Join("<span>, <span>", horses.Select(h => HttpUtility.HtmlEncode(h.Name))));
    }

  }
}