﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ThoroughWorks.Data;
using ThoroughWorks.Models;
using ThoroughWorks.Repository.HorseManagement;
using ThoroughWorks.Repository.OwnerManagement;
using ThoroughWorks.Repository.RoleManagement;
using ThoroughWorks.Repository.UserManagement;
using ThoroughWorks.Repository.UserSaleLocationManagement;
using ThoroughWorks.Web.Models.Helpers;
using ThoroughWorks.Models.Attributes;
using ThoroughWorks.Models.Common;
using ThoroughWorks.Repository.SireManagement;
using ThoroughWorks.Repository.ViewerManagement;
using ThoroughWorks.Repository.CardManagement;

namespace ThoroughWorks.Web.Models.Helpers
{
    public class DropDownHelper
    {
         
        public static MultiSelectList GetAllRoles(string[] selectedRoles, bool isSuperAdmin)
        {
            SelectRoles selectroles = new SelectRoles();
            List<RoleMaster> lstroles = selectroles.SelectAllActiveRoles(isSuperAdmin);
            MultiSelectList list = new MultiSelectList(lstroles, "RoleMasterID", "RoleName", selectedRoles);

            return list;
        }

        //public static List<SelectListItem> GetRoleList(string[] selectedRoles)
        //{
        //    SelectRoles selectroles = new SelectRoles();
        //    List<SelectListItem> lstRoles = selectroles.SelectAllActiveRoles().Select(x => new SelectListItem()
        //        {
        //            Selected = selectedRoles.Contains(x.RoleMasterID.ToString()),
        //            Text = x.RoleName,
        //            Value = x.RoleMasterID.ToString(),
        //        }).ToList();
        //    //MultiSelectList list = new MultiSelectList(lstroles, "RoleMasterID", "RoleName", selectedRoles);

        //    return lstRoles;
        //}
        
        /// <summary>
        /// select all active user for dropdown
        /// </summary>
        /// <param name="selectuser"></param>
        /// <returns></returns>
        public static SelectList GetAllActiveUsers(object selectuser)
        {
            List<UserDetail> lstuser = SelectUserDetails.SelectAllActiveUsers(SessionHelper.SubscriptionID).OrderBy(j => j.UserName).ToList();
            lstuser.Insert(0, new UserDetail() { UserID = 0, UserName = "--Select User--" });
            SelectList list = new SelectList(lstuser, "UserID", "UserName", selectuser);

            return list;
        }

        public static SelectList GetAllUserHorses(object selecthorse, int SubscriberID)
        {
            List<Horse> lst = SelectHorses.SelectAllActiveHorse(SubscriberID, true).OrderBy(j => j.Name).ToList();
            
            SelectList list = new SelectList(lst, "HorseID", "Name", selecthorse);

            return list;
        }

        public static SelectList GetAllHorseOwners(object selectowner)
        {
          List<Owner> lst = SelectOwners.SelectAllActiveOwners(SessionHelper.SubscriptionID).OrderBy(j => j.Name).ToList();
          SelectList list = new SelectList(lst, "OwnerID", "Name", selectowner);
          return list;
        }

        public static SelectList GetAllOwners()
        {
            List<Owner> lst = SelectOwners.SelectAllActiveOwners(SessionHelper.SubscriptionID).OrderBy(j => j.Name).ToList();
          SelectList list = new SelectList(lst, "OwnerID", "Name");
          return list;
        }

        public static SelectList GetUserLocation(object selectlocation)
        {
            List<UserSaleLocation> lst = SelectUserSaleLocation.SelectAllUserSaleLocation(SessionHelper.SubscriptionID).OrderBy(j => j.SaleName).ToList();

            SelectList list = new SelectList(lst, "UserSaleLocationID", "SaleName", selectlocation);

            return list;
        }


        public static SelectList GetAllVets(object selectvet)
        {
            List<UserDetail> lst = SelectUserDetails.SelectVets().OrderBy(j => j.UserName).ToList();

            List<UserDetail_M> userlist_m = (from aa in lst
                                            select new UserDetail_M
                                            {
                                                Active = aa.Active,
                                                FirstName = aa.FirstName,
                                                LastName = aa.LastName,
                                                UserID = aa.UserID,
                                                UserName = aa.UserName,
                                                EmailSignature = aa.EmailSignature
                                            }).ToList();

            SelectList list = new SelectList(userlist_m, "UserID", "FullName", selectvet);
            return list;
        }

        public static List<SelectListItem> GetAllHorseAgeCategories()
        {
            var hacList = (from Horse_M.HorseAgeCategories hac in Enum.GetValues(typeof(Horse_M.HorseAgeCategories))
                           select new SelectListItem { Selected = false, Text = ExtensionMethods.GetStringValue(hac), Value = ((int)hac).ToString() }).ToList();
            return hacList;
            
        }

        public static SelectList GetAllSires(object selectsire)
        {
            List<Sire> lst = SelectSire.SelectAllSires().OrderBy(j => j.SireName).ToList();

            List<Sire_M> list_m = (from aa in lst
                                             select new Sire_M
                                             {
                                                 SireID = aa.SireID,
                                                 SireName = aa.SireName,
                                                 Active = aa.Active,
                                             }).ToList();

            SelectList list = new SelectList(list_m, "SireID", "SireName", selectsire);
            return list;

        }

        public static List<Viewer_M> GetAllViewers(string query, int SubscriberID)
        {
            List<Viewer> globalViewerList = SelectViewer.SelectAllViewers().Where(p => p.ViewerName.Contains(query)).ToList();

            List<Viewer_M> glist = (from aa in globalViewerList
                                     select new Viewer_M
                                   {
                                       ViewerID = aa.ViewerID,
                                       ViewerName = aa.ViewerName
                                   }).ToList();

            // People not in the Viewers table who previosly inspected horses from this subscriber
            List<Card> subscribersViewerList = SelectCard.SelectViewersBySubscriberID(SubscriberID).Where(p => p.PersonName != null && p.PersonName.Contains(query)).ToList();

            List<Viewer_M> slist = (from aa in subscribersViewerList
                                     select new Viewer_M
                                     {
                                         ViewerID = aa.CardID,  // any old number - not used
                                         ViewerName = aa.PersonName
                                     }).ToList();


            List<Viewer_M> fullList = glist;
            fullList.AddRange(slist.Where(x => !fullList.Any(y => y.ViewerName == x.ViewerName)));

            // if their search term did not match, add it as a new item
            if (glist.Count + slist.Count == 0)
            {
                Viewer_M vm = new Viewer_M() { ViewerName = query };
                fullList.Add(vm);
            }

            fullList = fullList.OrderBy(v => v.ViewerName).ToList();

            //SelectList list = new SelectList(fullList, "ViewerName", "ViewerName", selectviewer);
            return fullList;

        }


    }
}
