﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace ThoroughWorks.Web.Models.Helpers
{
    public class SessionHelper
    {


        public static int UserID
        {
            get
            {
                if (!String.IsNullOrEmpty(HttpContext.Current.User.Identity.Name))
                {
                    // check the cookie.
                    var cookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
                    var ticket = FormsAuthentication.Decrypt(cookie.Value);
                    // cookie format is user id, subscription id, name, roles
                    var userID = Convert.ToInt32(ticket.UserData.Split('|')[0]);

                    return userID;
                }
                else
                    return 0;
            }
        }
        public static int SubscriptionID
        {
            get
            {
                if (!String.IsNullOrEmpty(HttpContext.Current.User.Identity.Name))
                {
                    // check the cookie.
                    var cookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
                    var ticket = FormsAuthentication.Decrypt(cookie.Value);
                    // cookie format is user id, subscription id, name, roles
                    var subID = Convert.ToInt32(ticket.UserData.Split('|')[1]);

                    return subID;
                }
                else
                    return 0;
            }
        }


        public static string DisplayName
        {
            get
            {
                if (!String.IsNullOrEmpty(HttpContext.Current.User.Identity.Name))
                {
                    // check the cookie.
                    var cookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
                    var ticket = FormsAuthentication.Decrypt(cookie.Value);
                    // cookie format is user id, subscription id, name, roles
                    var displayName = ticket.UserData.Split('|')[2];

                    return displayName;
                }
                else
                    return "";
            }
        }

        public static string RoleMasterID
        {
            get
            {
                if (!String.IsNullOrEmpty(HttpContext.Current.User.Identity.Name))
                {
                    // check the cookie.
                    var cookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];
                    var ticket = FormsAuthentication.Decrypt(cookie.Value);
                    // cookie format is user id, subscription id, name, roles
                    var roles = ticket.UserData.Split('|')[3];

                    return roles;
                }
                else
                    return "";
            }
        }
    }
}