﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace ThoroughWorks.Web.Models.Helpers
{
    public class CustomAuthorize : AuthorizeAttribute
    {
        public string Role { get; set; }
        public CustomAuthorize(string r)
        {
            Role = r;
        }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (!String.IsNullOrWhiteSpace(Convert.ToString(filterContext.HttpContext.Session["username"])))
            {
                string sessionRole = Convert.ToString(filterContext.HttpContext.Session["role"]);
                if (Role.ToUpper() == sessionRole.ToUpper())
                {

                }
                else
                    HandleUnauthorizedRequest(filterContext);
            }
            else
                HandleUnauthorizedRequest(filterContext);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            filterContext.Result = new RedirectToRouteResult(new
            RouteValueDictionary(new { controller = "Error", action = "AccessDenied" }));
        }
    }
}