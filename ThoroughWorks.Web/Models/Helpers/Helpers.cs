﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ThoroughWorks.Repository.AdvertisingManagement;
using ThoroughWorks.Models;
using EO.Pdf;
using System.Drawing;

using ThoroughWorks.Models.Attributes;
using System.Reflection;

namespace ThoroughWorks.Web.Models.Helpers
{
    public class Helpers
    {
        public static string ConvertString(object s)
        {
            try
            {
                if (Convert.ToString(s) == null)
                {
                    return "";
                }
                else
                {
                    return Convert.ToString(s);
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return "";
            }
        }

        public static string ConvertDateToString(DateTime? datetime)
        {
            try
            {
                if (datetime.HasValue)
                {
                    return datetime.Value.ToString("dd/MM/yyyy");
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return "";
            }
        }
        public static DateTime StartOfWeek(DateTime dt, DayOfWeek startOfWeek)
        {
            int diff = dt.DayOfWeek - startOfWeek;
            if (diff < 0)
            {
                diff += 7;
            }

            return dt.AddDays(-1 * diff).Date;
        }

        public static string ConvertDateToStringWithTime(DateTime? datetime)
        {
            try
            {
                if (datetime.HasValue)
                {
                    return datetime.Value.ToString("dd/MM/yyyy hh:mm tt");
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                string message = ex.Message;
                return "";
            }
        }

        // For FullCalendar
        // String passed may represent a long UNIX DT or a string of format dd/MM/yyyy
        // Either way return it as a DateTime
        public static DateTime EnsureDate(string dateString)
        {
            DateTime dt;
            if (dateString.Contains("/"))
                dt = DateTime.Parse(dateString);
            else
            {
                long lDt = long.Parse(dateString);
                dt = new DateTime(lDt);
            }
            return dt;
        }


        public static bool IsInRole(string rolename)
        {
            bool result = false;
            if (SessionHelper.RoleMasterID.Split(',').Contains(rolename))
                result = true;
            else
                result = false;

            return result;
        }

        public static bool isAdmin()
        {
            bool isadminUser = false;
            if (SessionHelper.RoleMasterID.Split(',').Contains(Convert.ToString((int)EnumHelper.Roles.AdminUser)))
                isadminUser = true;
            else
                isadminUser = false;

            return isadminUser;
        }
        public static bool isSuperAdmin()
        {
            bool isSuperAdmin = false;
            if (SessionHelper.RoleMasterID.Split(',').Contains(Convert.ToString((int)EnumHelper.Roles.SuperAdmin)))
                isSuperAdmin = true;
            else
                isSuperAdmin = false;

            return isSuperAdmin;
        }

        public static bool isSalesManager()
        {
            bool isSales = false;
            if (SessionHelper.RoleMasterID.Split(',').Contains(Convert.ToString((int)EnumHelper.Roles.SaleManager)))
                isSales = true;
            else
                isSales = false;

            return isSales;
        }

        public static bool isVet()
        {
            bool isVet = false;
            if (SessionHelper.RoleMasterID.Split(',').Contains(Convert.ToString((int)EnumHelper.Roles.Vet)))
                isVet = true;
            else
                isVet = false;

            return isVet;
        }

        public static string GetIp()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            return ip;
        }
        public static string EmptyThanSymbol(string value, string symbol)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return symbol;
            }
            else
            {
                return value;
            }
        }

        public static AdvertisingBanner_M GetAdvertisements()
        {
            return SelectAdvertisingBanners.GetRandomBannerForDisplay();

        }

        public static bool PageToPDF(string url, string pdfFileLocation)
        {
            //Set output page size = landscape
            HtmlToPdf.Options.AutoFitX = HtmlToPdfAutoFitMode.ScaleToFit;
            HtmlToPdf.Options.OutputArea = new RectangleF(0.2F, 0.2F, PdfPageSizes.A4.Height - 0.4F, PdfPageSizes.A4.Width - 0.4F); //' in inches
            HtmlToPdf.Options.PageSize = new SizeF(PdfPageSizes.A4.Height, PdfPageSizes.A4.Width);


            //Convert the Url
            HtmlToPdf.ConvertUrl(url, pdfFileLocation);

            return true;
        }

      
    }
}