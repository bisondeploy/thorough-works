﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThoroughWorks.Web.Models.Paging
{
    public class Sorting
    {
        public string sortedBy { get; set; }
        public string sortingDirection { get; set; }
    }
}