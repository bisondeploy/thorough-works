﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ThoroughWorks.Web.Models.Helpers
{
    public class CustomAjax
    {
        public string UrlToPost { get; set; }
        public string FormToPost { get; set; }
        public string UpdateControl { get; set; }
        public string ProgressControl { get; set; }
        public UpdateType UpdateType { get; set; }
        public string BeforeFunction { get; set; }
        public string SuccessFunction { get; set; }
        public string ErrorFunction { get; set; }
        
        public Dictionary<string, string> ToCustomAttributes()
        {
            Dictionary<string, string> attr = new Dictionary<string, string>();
            attr.Add ("ajax-form", FormToPost);
            attr.Add("ajax-update-progress", ProgressControl);
            attr.Add("ajax-update-control", UpdateControl);
            attr.Add("ajax-type", UpdateType.ToString());
            attr.Add("ajax-post-url", UrlToPost);
            attr.Add("ajax-before-function", BeforeFunction);
            attr.Add("ajax-custom", "true");
            attr.Add("error-function", ErrorFunction);
            attr.Add("success-function", SuccessFunction );
            return attr;
        }
    }
    public enum UpdateType
    {
        append,
        replace
    }

}