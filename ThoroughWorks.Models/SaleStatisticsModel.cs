﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ThoroughWorks.Models
{
    public class SaleStatisticsModel
    {
        public static SaleStatisticsModel Create(int userID, int userSaleLocationID, int saleID, string saleName, int registeredCards, int horses, IEnumerable<SaleHorseViews> horseViews, string horseViewsChartData)
        {
            var vm = new SaleStatisticsModel
            {
                UserID = userID,
                UserSaleLocationID = userSaleLocationID,
                SaleID = saleID,
                SaleName = saleName,
                RegisteredCards = registeredCards,
                Horses = horses,
                HorseViews = horseViews,
                HorseViewsChartData = horseViewsChartData
            };
            return vm;
        }

        public int UserID { get; set; }
        public int UserSaleLocationID { get; set; }
        public int SaleID { get; set; }
        public string SaleName { get; set; }

        public int? OwnerID { get; set; }
        public int? HorseID { get; set; }

        public int RegisteredCards { get; set; }
        public int Horses { get; set; }
        public IEnumerable<SaleHorseViews> HorseViews { get; set; }
        public string HorseViewsChartData { get; set; }
    }

    public class SaleHorseViews
    {
        public static SaleHorseViews Create(int horseID, int lotNumber, string horseName, int views)
        {
            var vm = new SaleHorseViews
            {
                HorseID = horseID,
                LotNumber = lotNumber,
                HorseName = horseName,
                Views = views
            };
            return vm;
        }

        public int HorseID { get; set; }
        public int LotNumber { get; set; }
        public string HorseName { get; set; }
        public int Views { get; set; }
    }

}
