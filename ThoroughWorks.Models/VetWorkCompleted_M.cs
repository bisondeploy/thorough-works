﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ThoroughWorks.Models
{
    public class VetWorkCompleted_M
    {

        public int VetWorkCompletedID { get; set; }
        public int VetWorkID { get; set; }
        public int VetWorkAttributeID { get; set; }
        public int VetWorkAttributeSequence { get; set; }

        [AllowHtml]
        [StringLength(100, ErrorMessage = "Details cannot be greater than 100 characters.")]
        public string AttributeValue { get; set; }    // eg. removed kidney stone

        public string VetWorkAttributeName { get; set; }

        public virtual VetWork_M VetWork { get; set; }
        public virtual VetWorkAttribute_M VetWorkAttribute { get; set; } 
    }
}
