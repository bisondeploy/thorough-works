﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ThoroughWorks.Models
{
  public class SubscriberPayment_M
  {
    public static SubscriberPayment_M Create(int subsciberPaymentID, Decimal amount, DateTime datePaid, DateTime expirationDate)
    {
      var vm = new SubscriberPayment_M
      {
        SubsciberPaymentID = subsciberPaymentID,
        Amount = amount,
        DatePaid = datePaid,
        ExpirationDate = expirationDate
      };
      return vm;
    }

    [Range(0, int.MaxValue, ErrorMessage = "Salary must be between 3000 and 10000000")]
    public int SubsciberPaymentID { get; set; }
    [Required(ErrorMessage = "*")]
    public Decimal Amount { get; set; }
    [Required(ErrorMessage = "*")]
    public DateTime DatePaid { get; set; }
    [Required(ErrorMessage = "*")]
    public DateTime ExpirationDate { get; set; }
    public string DatePaidStr
    {
      get
      {
        return DatePaid.ToString("dd/MM/yyyy");
      }

    }
    public string ExpirationDateStr
    {
      get
      {
        return ExpirationDate.ToString("dd/MM/yyyy");
      }

    }
  }
}
