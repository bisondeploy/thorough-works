﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
    public class VetWorkSearch
    {
        public static VetWorkSearch Create(int? page, int vetID, DateTime dt)
        {
            var vm = new VetWorkSearch
            {
                Page = page,
                VetID = vetID,
                Dt = dt
                
            };
            return vm;
        }

        public int? Page { get; set; }
        public int VetID { get; set; }
        public DateTime Dt { get; set; }
    }
}



