﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace ThoroughWorks.Models
{
  public class UserHorseSale_M
  {

      public static UserHorseSale_M Create(int userHorseSaleID, int horseID, string horseName, string horseImage, int userSaleLocationID, string userSaleLocationName, int sessionNumber, int numberSessions, int lotNumber, string comments, int sequence, bool delete, List<ViewLogHistory_M> viewers)
    {
      var vm = new UserHorseSale_M
      {
        UserHorseSaleID = userHorseSaleID,
        HorseID = horseID,
        HorseName = horseName,
        HorseImage = horseImage,
        UserSaleLocationID = userSaleLocationID,
        UserSaleLocationName = userSaleLocationName,
        SessionNumber = sessionNumber,
        NumberSessions = numberSessions,
        LotNumber = lotNumber,
        Comments = comments,
        Sequence = sequence,
        Delete = delete,
        Viewers = viewers,
        
      };
      return vm;
    }

    public int UserHorseSaleID { get; set; }

    public int HorseID { get; set; }
    public string HorseName { get; set; }
    public string HorseImage { get; set; }
    public int UserSaleLocationID { get; set; }
    public string UserSaleLocationName { get; set; }

    [DisplayName("Session Number")]
    public int SessionNumber { get; set; }
    public int NumberSessions { get; set; }

    public int LotNumber { get; set; }
    [DataType(DataType.MultilineText)]
    public string Comments { get; set; }
    public int Sequence { get; set; }

    public bool Delete { get; set; }

    public List<ViewLogHistory_M> Viewers { get; set; }

    public UserHorseSale_M()
    {
      Viewers = new List<ViewLogHistory_M>();
    }
  }
}
