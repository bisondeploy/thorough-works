﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
    public class HorseOwner_M
    {
        public static HorseOwner_M Create(int horseOwnerID, int ownerID, int horseID, string name, string emailAddress, string contactNumber, bool active, List<Horse_M> ownedHorses)
        {
            var vm = new HorseOwner_M
            {
                HorseOwnerID = horseOwnerID,
                HorseID = horseID,
                OwnerID = ownerID,
                Name = name,
                EmailAddress = emailAddress,
                ContactNumber = contactNumber,
                Active = active,
                OwnedHorses = ownedHorses,
            };
            return vm;
        }

        public int HorseOwnerID { get; set; }

        public int HorseID { get; set; }

        public int OwnerID { get; set; }

        
        public string Name { get; set; }

        [DisplayName("Email Address")]
        public string EmailAddress { get; set; }

        [StringLength(20, ErrorMessage = "Contact Number cannot be greater than 200 character.")]
        [DisplayName("Contact Number")]
        public string ContactNumber { get; set; }

        public bool Active { get; set; }
        
        public int SubscriberID { get; set; }

        [DisplayName("Owned Horses")]
        public List<Horse_M> OwnedHorses { get; set; }

        public bool Delete { get; set; }
    }
}
