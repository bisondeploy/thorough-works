﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
  public class OwnerSearch
  {
    public static OwnerSearch Create(int? page, string searchName)
    {
      var vm = new OwnerSearch
      {
        Page = page,
        SearchName = searchName
      };
      return vm;
    }

    public int? Page { get; set; }
    public string SearchName { get; set; }
  }
}
