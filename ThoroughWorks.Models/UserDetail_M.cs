﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
    public class UserDetail_M
    {

      public static UserDetail_M Create(int userID, string userName, string password, string firstName, string lastName, string emailID, string phoneNumber, bool active, DateTime createdDate, int createdBy, DateTime? updatedDate, int? updatedBy, List<int>selectedRoles, string emailSignature)
      {
        var vm = new UserDetail_M
        {
          UserID = userID,
          UserName = userName,
          Password = password,
          FirstName = firstName,
          LastName = lastName,
          EmailID = emailID,
          PhoneNumber = phoneNumber,
          Active = active,
          CreatedDate = createdDate,
          UpdatedDate = updatedDate,
          UpdatedBy = updatedBy,
          SelectedRoles = selectedRoles,
          EmailSignature = emailSignature
        };
        return vm;
      }


        public int UserID { get; set; }

        [Required(ErrorMessage = "The User Name field is required.")]
        [StringLength(100,ErrorMessage ="UserName cannot be greater than 100 character.")]
        [DisplayName("User Name")]
        public String UserName { get; set; }

        [Required(ErrorMessage = "The Password field is required.")]
        [StringLength(50, ErrorMessage = "Password cannot be greater than 50 character.")]
        [DataType(DataType.Password)]
        public String Password { get; set; }

        [StringLength(100, ErrorMessage = "First Name cannot be greater than 100 character.")]
        [DisplayName("First Name")]
        public String FirstName { get; set; }

        [StringLength(100, ErrorMessage = "Last Name cannot be greater than 100 character.")]
        [DisplayName("Last Name")]
        public String LastName { get; set; }

        [RegularExpression(@"^[\w-\._\+%]+@(?:[\w-]+\.)+[\w]{2,6}$", ErrorMessage = "Please enter correct email address")]
        [StringLength(200, ErrorMessage = "Email cannot be greater than 200 character.")]
        [DisplayName("Email Address")]
        public String EmailID { get; set; }

        [StringLength(20, ErrorMessage = "Phone cannot be greater than 20 character.")]
        [DisplayName("Phone Number")]
        public String PhoneNumber { get; set; }

        [DisplayName("Roles")]
        public String[]  RoleMasterIDs { get; set; }
        public String RoleMasterName { get; set; }
        public bool Active { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int? UpdatedBy { get; set; }
        public int SubscriberID { get; set; }

        public string FullName
        {
            get { return FirstName + " " + LastName; }
            set { }
        }


        public List<int> SelectedRoles { get; set; }

        [DisplayName("EmailSignature")]
        public string EmailSignature{ get; set; }

    }
}
