﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
    public class UserHorseSaleSearch
    {

      public static UserHorseSaleSearch Create(int searchUserSaleLocationID, int searchUserID, string searchUserName)
      {
        var vm = new UserHorseSaleSearch
        {
          SearchUserSaleLocationID = searchUserSaleLocationID,
          SearchUserID = searchUserID,
          SearchUserName = searchUserName
        };
        return vm;
      }

        public int SearchUserSaleLocationID { get; set; }
        public int SearchUserID { get; set; }
        public string SearchUserName { get; set; }
    }
}
