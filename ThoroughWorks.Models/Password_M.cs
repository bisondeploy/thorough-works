﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
    public class Password_M
    {
        public int UserID { get; set; }

        [Display(Name = "Current Password")]
        [Required(ErrorMessage = "The Current Password field is required.")]
        public String CurrentPassword { get; set; }

        [Display(Name = "New Password")]
        [Required(ErrorMessage = "The New Password field is required.")]
        public String NewPassword { get; set; }

        [Display(Name = "Confirm New Password")]
        [Required(ErrorMessage = "The Re-Type Password field is required.")]
        [System.ComponentModel.DataAnnotations.CompareAttribute("NewPassword", ErrorMessage = "New Passwords do not match.  Please re-confirm your new password.")]
        public String ReTypePassword { get; set; }

    }
}
