﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
  public class UserSaleLocation_M
  {

    public static UserSaleLocation_M Create(int userSaleLocationID, int userID, string userName, int subscriberID, string saleName, string location, DateTime? startDate, DateTime? endDate, int numberSessions, bool active, DateTime createdDate, int createdBy, DateTime? updatedDate, int? updatedBy, IEnumerable<UserHorseSale_M> horses, ICollection<ViewLogHistory_M> viewLogHistory)
    {
      var vm = new UserSaleLocation_M
      {
        UserSaleLocationID = userSaleLocationID,
        UserID = userID,
        UserName = userName,
        SubscriberID = subscriberID,
        SaleName = saleName,
        Location = location,
        StartDate = startDate,
        EndDate = endDate,
        NumberSessions = numberSessions,
        Active = active,
        CreatedDate = createdDate,
        CreatedBy = createdBy,
        UpdatedDate = updatedDate,
        UpdatedBy = updatedBy,
        Horses = horses,
        ViewLogHistory = viewLogHistory
      };
      return vm;
    }


    public int UserSaleLocationID { get; set; }
    public int UserID { get; set; }
    public string UserName { get; set; }

    public int SubscriberID { get; set; }

    [Required(ErrorMessage = "The Sale Name field is required.")]
    [DisplayName("Sale Name")]
    public string SaleName { get; set; }

    [UIHint("CustomTextArea")]
    public string Location { get; set; }

    [UIHint("CustomDate")]
    [DisplayFormat(DataFormatString = General.DateFormat, ApplyFormatInEditMode = true)]
    [DisplayName("Start Date")]
    public DateTime? StartDate { get; set; }

    [UIHint("CustomDate")]
    [DisplayFormat(DataFormatString = General.DateFormat, ApplyFormatInEditMode = true)]
    [DisplayName("End Date")]
    public DateTime? EndDate { get; set; }

    [Required(ErrorMessage = "Number of Sessions is required.")]
    [DisplayName("Number of Sessions")]
    public int NumberSessions { get; set; }
    public bool Active { get; set; }
    public DateTime CreatedDate { get; set; }
    public int? CreatedBy { get; set; }
    public DateTime? UpdatedDate { get; set; }
    public int? UpdatedBy { get; set; }


    public IEnumerable<UserHorseSale_M> Horses { get; set; }
    public ICollection<ViewLogHistory_M> ViewLogHistory { get; set; }

  }
}
