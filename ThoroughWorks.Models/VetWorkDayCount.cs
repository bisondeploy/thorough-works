﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
    public class VetWorkDayCount
    {
        public static VetWorkDayCount Create(int vetID, string vetName, DateTime visitDate, int appointmentCount)
        {
            var vm = new VetWorkDayCount
            {
                VetID = vetID,
                VetName = vetName,
                VisitDate = visitDate,
                AppointmentCount = appointmentCount
            };
            return vm;
        }

        public int VetID { get; set; }
        public string VetName { get; set; }
        public DateTime VisitDate { get; set; }
        public int AppointmentCount { get; set; }
    }
}
