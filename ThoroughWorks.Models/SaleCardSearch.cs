﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
    public class SaleCardSearch
    {
      public static SaleCardSearch Create(int? page, string cardName, string searchName)
      {
        var vm = new SaleCardSearch
        {
          Page = page,
          CardName = cardName
        };
        return vm;
      }

        public int? Page { get; set; }
        public string CardName { get; set; }
    }
}
