﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
  public class ViewLogHistory_M
  {
    public static ViewLogHistory_M Create(int userID, string userName, DateTime viewingDate)
    {
      var vm = new ViewLogHistory_M
      {
        UserID = userID,
        UserName = userName,
        ViewingDate = viewingDate
      };
      return vm;
    }

    public int UserID { get; set; }
    public string UserName { get; set; }
    public DateTime ViewingDate { get; set; }
  }
}
