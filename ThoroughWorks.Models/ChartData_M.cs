﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
  public class ChartData_M
  {
    public static ChartData_M Create(string xAxis, string yAxis)
    {
      var vm = new ChartData_M
      {
        XAxis = xAxis,
        YAxis = yAxis
      };
      return vm;
    }

    public string XAxis { get; set; }
    public string YAxis { get; set; }
  }
}
