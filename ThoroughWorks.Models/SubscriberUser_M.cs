﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
  public class SubscriberUser_M
  {
    public static SubscriberUser_M Create(int subscriberUserID, int subscriberID, int userID, string userName)
    {
      var vm = new SubscriberUser_M
      {
        SubscriberUserID = subscriberUserID,
        SubscriberID = subscriberID,
        UserID = userID,
        UserName = userName
      };
      return vm;
    }

    public int SubscriberUserID { get; set; }
    public int SubscriberID { get; set; }

    public int UserID { get; set; }
    public string UserName { get; set; }

    public string RoleNames { get; set; }
  }
}
