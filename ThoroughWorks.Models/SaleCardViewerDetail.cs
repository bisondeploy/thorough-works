﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ThoroughWorks.Models
{
  public class SaleCardViewModel
  {
    public static SaleCardViewModel Create(int saleID, int horseID, string horseName, List<DateTime> viewDates, DateTime saleStartDate, string comments, int userHorseSaleID, int lotNumber, Uri horseImagePath, List<CardHorseViewerModel> viewers)
    {
      var vm = new SaleCardViewModel
      {
        SaleID = saleID,
        HorseID = horseID,
        HorseName = horseName,
        ViewDates = viewDates,
        SaleStartDate = saleStartDate,
        Comments = comments,
        UserHorseSaleID = userHorseSaleID,
        LotNumber = lotNumber,
        HorseImagePath = horseImagePath,
        Viewers = viewers
      };
      return vm;
    }

    public int SaleID { get; set; }
    public int HorseID { get; set; }
    public string HorseName { get; set; }
    public List<DateTime> ViewDates { get; set; }
    public DateTime SaleStartDate { get; set; }
    [DataType(DataType.MultilineText)]
    public string Comments { get; set; }
    public int UserHorseSaleID { get; set; }
    public int LotNumber { get; set; }
    public Uri HorseImagePath { get; set; }

    public List<CardHorseViewerModel> Viewers { get; set; }
  }

  public class CardHorseViewerModel
  {
    public static CardHorseViewerModel Create(string viewerName, List<ViewsByViewerDateModel>viewList)
    {
      var vm = new CardHorseViewerModel
      {
        ViewerName = viewerName,
        ViewList = viewList
      };
      return vm;
    }

    public string ViewerName { get; set; }
    public List<ViewsByViewerDateModel> ViewList { get; set; }
  }

  public class ViewsByViewerDateModel
  {
    public static ViewsByViewerDateModel Create(DateTime date, int views)
    {
      var vm = new ViewsByViewerDateModel
      {
        Date = date,
        Views = views
      };
      return vm;
    }

    public DateTime Date { get; set; }
    public int Views { get; set; }
  }
} 
