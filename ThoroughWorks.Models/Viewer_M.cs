﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
    public class Viewer_M
    {
        public int ViewerID { get; set; }

        //public int Sequence { get; set; }

        [Required]
        [Display(Name = "Viewer Name")]
        [StringLength(100, ErrorMessage = "Viewer Name cannot be greater than 100 characters.")]
        public string ViewerName { get; set; }

        public bool Active { get; set; }
    }
}
