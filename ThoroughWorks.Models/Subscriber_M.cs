﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ThoroughWorks.Models
{
    public class Subscriber_M
    {
        public static Subscriber_M Create(int subscriberID, string userName, string businessName, string contactName, string address, string city, string state, string postalCode, string phone, string mobile, string fax, string emalID, List<SubscriberPayment_M> subscriberPayments, List<SubscriberUser_M> subscriberUsers, bool active)
        {
            var vm = new Subscriber_M
            {
                SubscriberID = subscriberID,
                UserName = userName,
                BusinessName = businessName,
                ContactName = contactName,
                Address = address,
                City = city,
                State = state,
                PostalCode = postalCode,
                Phone = phone,
                Mobile = mobile,
                Fax = fax,
                EmailID = emalID,
                SubscriberPayments = subscriberPayments,
                SubscriberUsers = subscriberUsers,
                Active = active,
            };
            return vm;
        }


        public int SubscriberID { get; set; }
        [DisplayName("User Name")]
        public string UserName { get; set; }
        [DisplayName("Business Name")]
        public string BusinessName { get; set; }

        [Required(ErrorMessage = "Contact Name is required.")]
        [DisplayName("Contact Name")]
        public string ContactName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        [DisplayName("Postal Code")]
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }

        public bool Active { get; set; }
        [RegularExpression(@"^[\w-\._\+%]+@(?:[\w-]+\.)+[\w]{2,6}$", ErrorMessage = "Please enter correct email address")]
        [DisplayName("Email Address")]
        public string EmailID { get; set; }

        public string ResolvedLogoFile { get; set; }

        [DisplayName("Update Logo by selecting a image file")]
        [StringLength(500, ErrorMessage = "Image name cannot be greater than 500 character.")]
        public string ImagePath { get; set; }

        [UIHint("CustomFileUpload")]
        [DisplayName("Logo File")]
        public HttpPostedFileBase Image { get; set; }

        public List<SubscriberPayment_M> SubscriberPayments { get; set; }
        public List<SubscriberUser_M> SubscriberUsers { get; set; }
        public Subscriber_M()
        {
            SubscriberPayments = new List<SubscriberPayment_M>();
            SubscriberUsers = new List<SubscriberUser_M>();
            Active = true;
        }
    }
}
