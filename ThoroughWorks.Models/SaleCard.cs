﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ThoroughWorks.Models
{

    // Inspection data must not be in future
    public sealed class InspectionDateAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            // Get Value of the Date property
            string dateString = value.ToString();
            DateTime dateNow = DateTime.Now;
            DateTime dateProperty = DateTime.Parse(dateString);

            return dateProperty < dateNow;
        }
    }


  public class SaleCard
  {
      public static SaleCard Create(int cardID, string cardUID, int userSaleLocationID, string saleName, int userID, int subscriberID, string userName, string personName, DateTime inspectionDate, IEnumerable<SaleCardHorses> horses, bool active, int numberSessions)
      {
          var vm = new SaleCard
          {
              CardID = cardID,
              CardUID = cardUID,
              UserSaleLocationID = userSaleLocationID,
              SaleName = saleName,
              UserID = userID,
              SubscriberID = subscriberID,
              UserName = userName,
              PersonName = personName,
              InspectionDate = inspectionDate,
              Horses = horses,
              Active = active,
              NumberSessions = numberSessions
          };
          return vm;
      }

    public int CardID { get; set; }
    [Display(Name = "Card ID")]
    public string CardUID { get; set; }
    public int UserSaleLocationID { get; set; }
    public string SaleName { get; set; }
    public int UserID { get; set; }
    public int SubscriberID { get; set; }
    public string UserName { get; set; }

    [Display(Name = "Inspection Date")]
    [InspectionDate(ErrorMessage = "Inspection date cannot be in the future.")]
    public DateTime InspectionDate { get; set; }

    [Required(ErrorMessage = "The viewers name is required.")]
    [Display(Name = "Viewer")]
    public string PersonName { get; set; }
    public IEnumerable<SaleCardHorses> Horses { get; set; }
    public bool Active { get; set; }
    [Display(Name = "Sessions")]
    public int NumberSessions { get; set; }
  }


  public class SaleCardHorses
  {
      public static SaleCardHorses Create(int horseID, string horseName, string horseImage, int sequence, DateTime? dob, string comment, string sex, string colour, int lotNumber, int sessionNumber, string sire, string dam, List<HorseOwner_M> owners, DateTime? dateArrived, DateTime? dateSold, string attachmentsPath, bool selected)
    {
      var vm = new SaleCardHorses
      {
        HorseID = horseID,
        HorseName = horseName,
        HorseImage = horseImage,
        Sequence = sequence,
        DOB = dob,
        Comment = comment,
        Sex = sex,
        Colour = colour,
        LotNumber = lotNumber,
        SessionNumber = sessionNumber,
        Sire = sire,
        Dam = dam,
        Owners = owners,
        DateArrived = dateArrived,
        DateSold = dateSold,
        AttachmentsPath = attachmentsPath,
        Selected = selected
      };
      return vm;
    }

    public int HorseID { get; set; }
    public string HorseName { get; set; }
    public string HorseImage { get; set; }
    public int Sequence { get; set; }
    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    public DateTime? DOB { get; set; }
    public string Comment { get; set; }
    public string Sex { get; set; }
    public string Colour { get; set; }
    public int LotNumber { get; set; }
    public int SessionNumber { get; set; }
    public string Sire { get; set; }
    public string Dam { get; set; }
    public List<HorseOwner_M> Owners { get; set; }
    public DateTime? DateArrived { get; set; }
    public DateTime? DateSold { get; set; }
    public string AttachmentsPath { get; set; }

    public bool Selected { get; set; }

  }
}
