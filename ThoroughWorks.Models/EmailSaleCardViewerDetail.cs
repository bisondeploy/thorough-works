﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Postal;

namespace ThoroughWorks.Models
{
  public class EmailSaleCardViewModel : Email
  {
    public static EmailSaleCardViewModel Create(string fromAddress, string fromName, string to, string subject, string userName, string userEmail, string emailSignature, string ownerName, string customMessage, string resolvedLogoFile, List<SaleCardViewModel>saleCards)
    {
      var vm = new EmailSaleCardViewModel
      {
        From = fromAddress,
        FromName = fromName,
        To = to,
        Subject = subject,
        UserName = userName,
        UserEmail = userEmail,
        EmailSignature = emailSignature,
        OwnerName = ownerName,
        CustomMessage = customMessage,
        SaleCards = saleCards,
        ResolvedLogoFile = resolvedLogoFile
      };
      return vm;
    }

    public string From { get; set; }
    public string FromName { get; set; }
    public string To { get; set; }
    public string Subject { get; set; }
    public string UserName { get; set; }
    public string UserEmail { get; set; }
    public string EmailSignature { get; set; }
    public string OwnerName { get; set; }
    public string CustomMessage { get; set; }
    public string ResolvedLogoFile { get; set; }
    public List<SaleCardViewModel> SaleCards { get; set; }
  }

} 
