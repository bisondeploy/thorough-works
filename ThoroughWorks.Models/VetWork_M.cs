﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

using ThoroughWorks.Models.Common;
using System.Web.Mvc;

namespace ThoroughWorks.Models
{
    public class VetWork_M
    {

        public int VetWorkID { get; set; }

        //[Required(ErrorMessage = "The Horse field is required")]
        [Range(1, double.MaxValue, ErrorMessage = "Please Select Horse.")]
        [DisplayName("Horse")]
        public int HorseID { get; set; }

        [Required(ErrorMessage = "The Vet field is required")]
        [DisplayName("Vet")]
        public int VetID { get; set; }

        [DisplayName("Horse")]
        public string HorseName { get; set; }

        [DisplayName("Vet")]
        public string VetName { get; set; }

        [DisplayName("Age Category")]
        public int? AgeCategory { get; set; }

        public string AgeCategoryDisplay
        {
            get
            {
                if (AgeCategory != null)
                    return ExtensionMethods.GetStringValue((Horse_M.HorseAgeCategories)AgeCategory);
                return "";

            }
        }

        [Required(ErrorMessage="Please specify Visit Date")]
        [DisplayName("Visit Date")]
        //[DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? VisitDate { get; set; }

        [DisplayName("Last Visit")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? LastVisitDate { get; set; }

        [DisplayName("In For")]
        [StringLength(500, ErrorMessage = "In For cannot exceed 500 characters")]
        public string InFor { get; set; }

        [UIHint("CustomTextArea")]
        [AllowHtml]
        [DisplayName("Comments")]
        [StringLength(500, ErrorMessage = "Comments cannot exceed 500 characters")]
        public string WorkCompleted { get; set; }

        [DisplayName("Status")]
        [DefaultValue("Pending")]
        public string Status { get; set; }

        [DisplayName("Next Visit")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? NextVisitDate { get; set; }

        [DisplayName("Next In For")]
        [StringLength(500, ErrorMessage = "Next In For cannot exceed 500 characters")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string NextInFor { get; set; }

        public bool Active { get; set; }

        public int NextVisitID { get; set; }

        public List<VetWorkCompleted_M> VetWorkCompleted { get; set; }

    }


}
