﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace ThoroughWorks.Models
{
  public class Login_M
  {
    public static Login_M Create(string userName, string password, bool rememberMe)
    {
      var vm = new Login_M
      {
        Username = userName,
        Password = password,
        RememberMe = rememberMe
      };
      return vm;
    }

    [Required]
    public string Username { get; set; }
    [Required]
    public string Password { get; set; }

    public bool RememberMe { get; set; }

    //public Login_M()
    //{
    //  RememberMe = true;
    //}
  }
}
