﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
    public class VetWorkAttribute_M
    {
        public int VetWorkAttributeID { get; set; }

        public int SubscriberID { get; set; }

        public int Sequence { get; set; }

        [Required]
        [Display(Name = "Attribute Name")]
        [StringLength(100, ErrorMessage = "Attribute Name cannot be greater than 100 characters.")]
        public string AttributeName { get; set; }

        public bool Active { get; set; }

	
    }
}






