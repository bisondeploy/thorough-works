﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
  public class HorseSearch
  {
      public static HorseSearch Create(int? page, string searchTags, bool currentOnly)
    {
      var vm = new HorseSearch
      {
        Page = page,
        SearchTags = searchTags,
        CurrentOnly = currentOnly,
      };
      return vm;
    }

    public int? Page { get; set; }
    public string SearchTags { get; set; }
    public bool CurrentOnly { get; set; }

  }
}
