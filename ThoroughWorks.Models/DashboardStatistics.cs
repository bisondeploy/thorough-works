﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
  public class DashboardStatistics
  {
    public static DashboardStatistics Create(int adminSubscribers, int horses, int saleLocation, List<UserSaleLocation_M> currentSaleLocations)
    {
      var vm = new DashboardStatistics
      {
        AdminSubscribers = adminSubscribers,
        Horses = horses,
        SalesLocation = saleLocation,
        CurrentSaleLocations = currentSaleLocations
      };
      return vm;
    }

    public int AdminSubscribers { get; set; }
    public int Horses { get; set; }
    public int SalesLocation { get; set; }

    public List<UserSaleLocation_M> CurrentSaleLocations { get; set; }
    public List<VetWork_M> VetWork{ get; set; }

  }
}
