﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
    public class UserDetailSearch
    {

      public static UserDetailSearch Create(int? page, string searchFirstName, string searchLastName)
      {
        var vm = new UserDetailSearch
        {
          page = page,
          SearchFirstName = searchFirstName,
          SearchLastName = searchLastName
        };
        return vm;
      }

        public int? page { get; set; }
        public string SearchFirstName { get; set; }
        public string SearchLastName { get; set; }
    }
}
