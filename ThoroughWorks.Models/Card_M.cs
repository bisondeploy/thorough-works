﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{




    public class Card_M
    {
        public static Card_M Create(int cardID, string cardUID, int userID, int subscriberID, string userName, int userSaleLocationID, string saleName, string personName, string userRefNo, List<Horse_M> horses, DateTime inspectionDate)
        {
            var vm = new Card_M
            {
                CardID = cardID,
                CardUID = cardUID,
                UserID = userID,
                SubscriberID = subscriberID,
                UserName = userName,
                UserSaleLocationID = userSaleLocationID,
                SaleName = saleName,
                PersonName = personName,
                UserRefNo = userRefNo,
                Horses = horses,
                InspectionDate = inspectionDate,
            };
            return vm;
        }

        public int CardID { get; set; }
        public string CardUID { get; set; }
        public int UserID { get; set; }
        public int SubscriberID { get; set; }
        public string UserName { get; set; }
        public int UserSaleLocationID { get; set; }
        //public UserSaleLocation_M UserSaleLocation { get; set; }
        public string SaleName { get; set; }
        public string PersonName { get; set; }
        public string UserRefNo { get; set; }
        public DateTime? InspectionDate { get; set; }

        public List<Horse_M> Horses { get; set; }

        //public Card_M()
        //{
        //  horses = new List<Horse_M>();
        //}
    }
}
