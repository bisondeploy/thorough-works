﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
  public class UserSaleLocationSearch
  {
    public static UserSaleLocationSearch Create(int? page, string searchUserID, string searchName)
    {
      var vm = new UserSaleLocationSearch
      {
        Page = page,
        SearchUserID = searchUserID,
        SearchName = searchName
      };
      return vm;
    }

    public int? Page { get; set; }
    public string SearchUserID { get; set; }
    public string SearchName { get; set; }
  }
}
