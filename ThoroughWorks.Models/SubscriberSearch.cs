﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
  public class SubscriberSearch
  {
    public static SubscriberSearch Create(int? page, string searchContactName, string searchBusinessName)
    {
      var vm = new SubscriberSearch
      {
        Page = page,
        SearchContactName = searchContactName,
        SearchBusinessName = searchBusinessName
      };
      return vm;
    }

    public int? Page { get; set; }
    public string SearchContactName { get; set; }
    public string SearchBusinessName { get; set; }
  }
}
