﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ThoroughWorks.Models.Attributes;

namespace ThoroughWorks.Models
{
  public class Owner_M
  {
    public static Owner_M Create(int ownerID, string name, string emailAddress, string contactNumber, bool active, bool isSelected, string message, int emailHorseID, List<Horse_M> ownedHorses, List<Horse_M> horsesOnSale)
    {
      var vm = new Owner_M
      {
        OwnerID = ownerID,
        Name = name,
        EmailAddress = emailAddress,
        ContactNumber = contactNumber,
        Active = active,
        IsSelected = isSelected,
        Message = message,
        EmailHorseID = emailHorseID,
        OwnedHorses = ownedHorses,
        HorsesOnSale = horsesOnSale
      };
      return vm;
    }

    public int OwnerID { get; set; }

    [Required]
    [StringLength(200, ErrorMessage = "Name cannot be greater than 200 character.")]
    public string Name { get; set; }
    
    //[RegularExpression(@"^[\w-\._\+%]+@(?:[\w-]+\.)+[\w]{2,6}$", ErrorMessage = "Please enter correct email address")]
    //[StringLength(200, ErrorMessage = "Email Address cannot be greater than 200 character.")]
    [DisplayName("Email Address")]
    public string EmailAddress { get; set; }

    [StringLength(20, ErrorMessage = "Contact Number cannot be greater than 200 character.")]
    [DisplayName("Contact Number")]
    public string ContactNumber { get; set; }

    public bool Active { get; set; }
    public bool IsSelected { get; set; }
    
    [DataType(DataType.MultilineText)]
    public string Message { get; set; }

    public int EmailHorseID { get; set; }

    public int SubscriberID { get; set; }

    [DisplayName("Owned Horses")]
    public List<Horse_M> OwnedHorses { get; set; }
    [DisplayName("Horses On Sale")]
    public List<Horse_M> HorsesOnSale { get; set; }
  }
}
