﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web;
using ThoroughWorks.Models.Attributes;

namespace ThoroughWorks.Models
{
  public class AdvertisingBanner_M
  {

    public static AdvertisingBanner_M Create(int id, string name, string url, string imagePath, DateTime displayedFrom, DateTime? displayedTo, bool active, DateTime created, int createdBy, DateTime? lastUpdated, int lastUpdatedBy)
    {
      var vm = new AdvertisingBanner_M
      {
        ID = id,
        Name = name,
        Url = url,
        ImagePath = imagePath,
        DisplayedFrom = displayedFrom,
        DisplayedTo = displayedTo,
        Active = active,
        Created = created,
        CreatedBy = createdBy,
        LastUpdated = lastUpdated,
        LastUpdatedBy = lastUpdatedBy
      };
      return vm;
    }


    public int ID { get; set; }
    [Display(Name = "Name")]
    public string Name { get; set; }
    [Display(Name = "URL To Link to")]
    public string Url { get; set; }
    [Display(Name = "Image File")]
    public string ImagePath { get; set; }


    [UIHint("CustomDate")]
    [Display(Name = "Shown From")]
    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    [DataType(DataType.Date)]
    public DateTime DisplayedFrom { get; set; }

    [UIHint("CustomDate")]
    [Display(Name = "Shown To")]
    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    [DataType(DataType.Date)]
    public DateTime? DisplayedTo { get; set; }

    [Display(Name = "Active?")]
    public bool Active { get; set; }
    public DateTime Created { get; set; }
    public int CreatedBy { get; set; }
    public DateTime? LastUpdated { get; set; }
    public int? LastUpdatedBy { get; set; }


    [UIHint("CustomFileUpload")]
    //[ImageFile]
    public HttpPostedFileBase Image { get; set; }

  }
}
