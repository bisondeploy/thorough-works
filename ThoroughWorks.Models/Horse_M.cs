﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ThoroughWorks.Models.Attributes;
using ThoroughWorks.Models.Common;

namespace ThoroughWorks.Models
{
  public class Horse_M
  {
      public enum HorseAgeCategories
      {
          [StringValue("Weanling")]
          Weanling = 1,
          [StringValue("Yearling")]
          Yearling = 2,
          [StringValue("2YO")]
          TwoYearOld = 3,
          [StringValue("Brood Mare")]
          BroodMare = 4
      }

      public static Horse_M CreateInfo(int horseID, string name)
      {
          var vm = new Horse_M
          {
              HorseID = horseID,
              Name = name
          };
          return vm;
      }

    public static Horse_M CreateInfo(int horseID, string name, bool current, List<HorseOwner_M> owners)
    {
      var vm = new Horse_M
      {
        HorseID = horseID,
        Name = name,
        Owners = owners,
        Current = current
      };
      return vm;
    }

    public static Horse_M Create(int horseID, int? userID, int subscriberID, string userName, string name, int ageCategory, DateTime? dob, int sireID, string sireDisplay, string dam, string sex, string colour, int? ownerID, string imagePath, DateTime? dateArrived, DateTime? dateSold, string comments, string attachmentsPath, bool active, bool current, List<HorseOwner_M> owners)
    {
      var vm = new Horse_M
      {
        HorseID = horseID,
        UserID = userID,
        SubscriberID = subscriberID,
        UserName = userName,
        Name = name,
        DOB = dob,
        SireID = sireID,
        SireDisplay = sireDisplay,
        Dam = dam,
        Sex = sex,
        Colour = colour,
        ImagePath = imagePath,
        DateArrived = dateArrived,
        DateSold = dateSold,
        Comments = comments,
        AttachmentsPath = attachmentsPath,
        Active = active,
        Current = current,
        Owners = owners,
        AgeCategory = ageCategory
      };
      return vm;
    }

    public int HorseID { get; set; }

    [Range(1, double.MaxValue, ErrorMessage = "Please Select User.")]
    public int? UserID { get; set; }
    public int SubscriberID { get; set; }

    [DisplayName("User Name")]
    public string UserName { get; set; }

    [Required(ErrorMessage = "The Name field is required.")]
    [StringLength(200, ErrorMessage = "Name cannot be greater than 200 character.")]
    public string Name { get; set; }

    [Display(Name = "Age Category")]
    public int? AgeCategory { get; set; }

    public string AgeCategoryDisplay
    {
        get
        {
            if( AgeCategory != null )
                return ExtensionMethods.GetStringValue((HorseAgeCategories)AgeCategory);
            return "";
            
        }
    }

    //[UIHint("CustomDate")]
    [DisplayName("Date of Birth")]
    [DataType(DataType.Date)]
    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    public DateTime? DOB { get; set; }

    [DisplayName("Sire")]
    [Required(ErrorMessage = "Please select a Sire")]
    public int SireID { get; set; }

    public string SireDisplay { get; set; }
    

    public string Dam { get; set; }

    [Required]
    [Display(Name = "Sex")]
    public string Sex { get; set; }
    public string SexDisplay
    {
      get
      {
        return Sex == "F" ? "Filly" : "Colt";
      }
    }

    [Required]
    [Display(Name = "Colour")]
    [StringLength(400, ErrorMessage = "Colour cannot be greater than 50 characters.")]
    public string Colour { get; set; }

    [Display(Name = "Owners")]
    public List<HorseOwner_M> Owners { get; set; }

    [UIHint("CustomFileUpload")]
    //[ImageFile]
    public HttpPostedFileBase Image { get; set; }

    [DisplayName("Image")]
    [StringLength(500, ErrorMessage = "Image name cannot be greater than 500 character.")]
    public string ImagePath { get; set; }

    //[UIHint("CustomDate")]
    [DisplayName("Date Arrived")]
    [DataType(DataType.Date)]
    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    public DateTime? DateArrived { get; set; }

    //[UIHint("CustomDate")]
    [DisplayName("Date Sold")]
    [DataType(DataType.Date)]
    [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
    public DateTime? DateSold { get; set; }

    [UIHint("CustomTextArea")]
    public string Comments { get; set; }

    [UIHint("CustomFileUpload")]
    public HttpPostedFileBase Attachments { get; set; }

    [StringLength(200, ErrorMessage = "Attachment cannot be greater than 200 character ")]
    public string AttachmentsPath { get; set; }

    public bool Active { get; set; }
    public bool Current { get; set; }

    public List<VetWork_M> VetWorks { get; set; }

  }
}
