﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThoroughWorks.Models
{
    public class Sire_M
    {
        public int SireID { get; set; }

        //public int Sequence { get; set; }

        [Required]
        [Display(Name = "Sire Name")]
        [StringLength(100, ErrorMessage = "Sire Name cannot be greater than 100 characters.")]
        public string SireName { get; set; }

        public bool Active { get; set; }
    }
}
