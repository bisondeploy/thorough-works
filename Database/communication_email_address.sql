CREATE TABLE [dbo].[communication_email_address](
	[from_id] [int] IDENTITY(1,1) NOT NULL,
	[from_mail_server] [varchar](100) NOT NULL,
	[from_port] [int] NULL,
	[from_use_ssl] [bit] NULL,
	[from_mail_username] [varchar](100) NULL,
	[from_mail_password] [varchar](50) NULL,
	[from_date_created] [smalldatetime] NULL,
	[from_date_updated] [smalldatetime] NULL,
	[from_mail_address] [varchar](100) NOT NULL,
	[from_communication_type] [varchar](10) NOT NULL,
 constraint pk_communication_email_addresses primary key (from_id))
go



