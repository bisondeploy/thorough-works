SET IDENTITY_INSERT [RoleMaster] ON 
GO
INSERT [RoleMaster] ([RoleMasterID], [RoleName], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) 
VALUES 
(1, N'Admin', 1, getdate(), 0, NULL, NULL),
(2, N'NormalUser', 1, getdate(), 0, NULL, NULL),
(5, N'Super Admin', 1, getdate(), 0, NULL, NULL),
(6, N'Sale Manager', 1, getdate(), 0, NULL, NULL)
GO
SET IDENTITY_INSERT [RoleMaster] OFF
GO
SET IDENTITY_INSERT [UserDetail] ON 
INSERT [UserDetail] ([UserID], [UserName], [Password], [FirstName], [LastName], [EmailID], [Active], [CreatedDate], [CreatedBy]) 
VALUES (1, N'admin', N'JHalford123', N'Jack', N'Halford', N'jack@thoroughworks.com', 1, getdate(), 0)
GO
SET IDENTITY_INSERT [UserDetail] OFF
GO
INSERT [UserRole] ([UserID], [RoleMasterID]) VALUES (1,5)
GO
