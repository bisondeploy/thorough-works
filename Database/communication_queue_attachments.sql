
CREATE TABLE [dbo].[communication_queue_attachments](
	[cqa_id] [int] IDENTITY(1,1) NOT NULL,
	[cqa_cq_id] [int] NOT NULL,
	[cqa_full_path] [varchar](500) NULL,
	[cqa_active] [varchar](1) NOT NULL,
	[cqa_created] [smalldatetime] NOT NULL,
	[cqa_created_by] [int] NOT NULL,
	[cqa_last_updated] [smalldatetime] NULL,
	[cqa_last_updated_by] [int] NULL,
    CONSTRAINT [PK_comm_queue_attachments] PRIMARY KEY (cqa_id),
	constraint [FK_comm_queue_attachments_communication_queue] FOREIGN KEY([cqa_cq_id]) REFERENCES [dbo].[communication_queue] ([cq_id])
)
GO
