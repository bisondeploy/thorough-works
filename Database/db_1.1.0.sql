CREATE TABLE [dbo].[Owner](
	[OwnerID] [int] IDENTITY(1,1) NOT NULL,
	SubscriberID int not null,
	[Name] [varchar](200) NOT NULL,
	[EmailAddress] [varchar](max) NULL,
	[ContactNumber] [varchar](20) NULL,
	[Active] bit not null
 CONSTRAINT [PK_Owner] PRIMARY KEY CLUSTERED ([OwnerID] ASC),
 constraint DF_Owner_Active  default ((1)) for Active,
 constraint FK_Owner_SubscriberID foreign key (SubscriberID) references Subscriber(SubscriberID)
)
go


alter table Horse
add OwnerID int null
go

alter table Horse
add constraint FK_Horse_OwnerID foreign key (OwnerID) references Owner(OwnerID)
go

-- loop back old owner details if entered
insert into Owner
(Name, subscriberID)
select Owner, SubscriberID from Horse where owner is not null
go

update h
set ownerid = o.ownerid
from horse h join owner o on h.Owner = o.Name
where h.Owner is not null
go

-- remove old owner field on horse
alter table Horse
drop column Owner
go

alter table UserHorseSale
add Comments varchar(max) Null
go

