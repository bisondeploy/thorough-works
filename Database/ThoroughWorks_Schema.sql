CREATE TABLE [Card](
	[CardID] [int] IDENTITY(1,1) NOT NULL,
	[CardUID] [char](36) NOT NULL,
	[UserID] [int] NOT NULL,
	[UserSaleLocationID] [int] NOT NULL,
	[PersonName] [varchar](500) NULL,
	[UserRefNo] [char](36) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Card] PRIMARY KEY CLUSTERED 
(
	[CardID] ASC
))
GO
CREATE TABLE [CardHorses](
	[CardHorsesID] [int] IDENTITY(1,1) NOT NULL,
	[CardID] [int] NOT NULL,
	[HorseID] [int] NOT NULL,
 CONSTRAINT [PK_CardHorses] PRIMARY KEY CLUSTERED 
(
	[CardHorsesID] ASC
))
GO
CREATE TABLE [Horse](
	[HorseID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[Name] [varchar](200) NULL,
	[DOB] [datetime] NULL,
	[Sire] [varchar](400) NULL,
	[Dam] [varchar](max) NULL,
	[Owner] [varchar](500) NULL,
	[Image] [varchar](500) NULL,
	[DateArrived] [datetime] NULL,
	[DateSold] [datetime] NULL,
	[Comments] [varchar](max) NULL,
	[Attachments] [varchar](200) NULL,
	[Active] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[Sex] [varchar](20) NOT NULL,
	[Colour] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Horse] PRIMARY KEY CLUSTERED 
(
	[HorseID] ASC
))
GO
CREATE TABLE [RoleMaster](
	[RoleMasterID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](500) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_RoleMaster] PRIMARY KEY CLUSTERED 
(
	[RoleMasterID] ASC
))
GO
CREATE TABLE [Subscriber](
	[SubscriberID] [int] IDENTITY(1,1) NOT NULL,
	[BusinessName] [varchar](1000) NULL,
	[ContactName] [varchar](500) NULL,
	[Address] [varchar](max) NULL,
	[City] [varchar](200) NULL,
	[State] [varchar](200) NULL,
	[PostalCode] [varchar](20) NULL,
	[Phone] [varchar](20) NULL,
	[Mobile] [varchar](20) NULL,
	[Fax] [varchar](50) NULL,
	[EmailID] [varchar](400) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Subscriber] PRIMARY KEY CLUSTERED 
(
	[SubscriberID] ASC
))
GO
CREATE TABLE [SubscriberPayment](
	[SubsciberPaymentID] [int] IDENTITY(1,1) NOT NULL,
	[SubscriberID] [int] NOT NULL,
	[Amount] [numeric](18, 4) NOT NULL,
	[DatePaid] [datetime] NOT NULL,
	[ExpirationDate] [datetime] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_SubscriberPayment] PRIMARY KEY CLUSTERED 
(
	[SubsciberPaymentID] ASC
))
GO
CREATE TABLE [SubscriberUser](
	[SubscriberUserID] [int] IDENTITY(1,1) NOT NULL,
	[SubscriberID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_SubscriberUser] PRIMARY KEY CLUSTERED 
(
	[SubscriberUserID] ASC
))
GO
CREATE TABLE [UserDetail](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](100) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[FirstName] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[EmailID] [varchar](200) NULL,
	[PhoneNumber] [varchar](20) NULL,
	[Active] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_UserDetail] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
))
GO
CREATE TABLE [UserHorseSale](
	[UserHorseSaleID] [int] IDENTITY(1,1) NOT NULL,
	[HorseID] [int] NOT NULL,
	[UserSaleLocationID] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
	[LotNumber] [int] NOT NULL,
 CONSTRAINT [PK_UserHorseSale] PRIMARY KEY CLUSTERED 
(
	[UserHorseSaleID] ASC
))
GO
CREATE TABLE [UserRole](
	[UserRoleID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[RoleMasterID] [int] NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[UserRoleID] ASC
))
GO
CREATE TABLE [UserSaleLocation](
	[UserSaleLocationID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[SaleName] [varchar](500) NOT NULL,
	[Location] [varchar](max) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_UserSaleLocation] PRIMARY KEY CLUSTERED 
(
	[UserSaleLocationID] ASC
))
GO
CREATE TABLE [ViewLogHistory](
	[ViewLogHistoryID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[UserSaleLocationID] [int] NOT NULL,
	[HorseID] [int] NOT NULL,
	[IPAddress] [varchar](50) NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ViewerName] [varchar](500) NULL,
 CONSTRAINT [PK_ViewLogHistory] PRIMARY KEY CLUSTERED 
(
	[ViewLogHistoryID] ASC
))
GO
ALTER TABLE [Card]  WITH CHECK ADD  CONSTRAINT [FK_Card_Card] FOREIGN KEY([UserSaleLocationID])
REFERENCES [UserSaleLocation] ([UserSaleLocationID])
GO
ALTER TABLE [Card] CHECK CONSTRAINT [FK_Card_Card]
GO
ALTER TABLE [Card]  WITH CHECK ADD  CONSTRAINT [FK_Card_UserDetail] FOREIGN KEY([UserID])
REFERENCES [UserDetail] ([UserID])
GO
ALTER TABLE [Card] CHECK CONSTRAINT [FK_Card_UserDetail]
GO
ALTER TABLE [CardHorses]  WITH CHECK ADD  CONSTRAINT [FK_CardHorses_Card] FOREIGN KEY([CardID])
REFERENCES [Card] ([CardID])
GO
ALTER TABLE [CardHorses] CHECK CONSTRAINT [FK_CardHorses_Card]
GO
ALTER TABLE [CardHorses]  WITH CHECK ADD  CONSTRAINT [FK_CardHorses_Horse] FOREIGN KEY([HorseID])
REFERENCES [Horse] ([HorseID])
GO
ALTER TABLE [CardHorses] CHECK CONSTRAINT [FK_CardHorses_Horse]
GO
ALTER TABLE [Horse]  WITH CHECK ADD  CONSTRAINT [FK_Horse_UserDetail] FOREIGN KEY([UserID])
REFERENCES [UserDetail] ([UserID])
GO
ALTER TABLE [Horse] CHECK CONSTRAINT [FK_Horse_UserDetail]
GO
ALTER TABLE [SubscriberPayment]  WITH CHECK ADD  CONSTRAINT [FK_SubscriberPayment_Subscriber] FOREIGN KEY([SubscriberID])
REFERENCES [Subscriber] ([SubscriberID])
ON DELETE CASCADE
GO
ALTER TABLE [SubscriberPayment] CHECK CONSTRAINT [FK_SubscriberPayment_Subscriber]
GO
ALTER TABLE [SubscriberUser]  WITH CHECK ADD  CONSTRAINT [FK_SubscriberUser_Subscriber] FOREIGN KEY([SubscriberID])
REFERENCES [Subscriber] ([SubscriberID])
ON DELETE CASCADE
GO
ALTER TABLE [SubscriberUser] CHECK CONSTRAINT [FK_SubscriberUser_Subscriber]
GO
ALTER TABLE [SubscriberUser]  WITH CHECK ADD  CONSTRAINT [FK_SubscriberUser_UserDetail] FOREIGN KEY([UserID])
REFERENCES [UserDetail] ([UserID])
ON DELETE CASCADE
GO
ALTER TABLE [SubscriberUser] CHECK CONSTRAINT [FK_SubscriberUser_UserDetail]
GO
ALTER TABLE [UserHorseSale]  WITH CHECK ADD  CONSTRAINT [FK_UserHorseSale_Horse] FOREIGN KEY([HorseID])
REFERENCES [Horse] ([HorseID])
GO
ALTER TABLE [UserHorseSale] CHECK CONSTRAINT [FK_UserHorseSale_Horse]
GO
ALTER TABLE [UserHorseSale]  WITH CHECK ADD  CONSTRAINT [FK_UserHorseSale_UserSaleLocation1] FOREIGN KEY([UserSaleLocationID])
REFERENCES [UserSaleLocation] ([UserSaleLocationID])
GO
ALTER TABLE [UserHorseSale] CHECK CONSTRAINT [FK_UserHorseSale_UserSaleLocation1]
GO
ALTER TABLE [UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_RoleMaster] FOREIGN KEY([RoleMasterID])
REFERENCES [RoleMaster] ([RoleMasterID])
GO
ALTER TABLE [UserRole] CHECK CONSTRAINT [FK_UserRole_RoleMaster]
GO
ALTER TABLE [UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_UserDetail] FOREIGN KEY([UserID])
REFERENCES [UserDetail] ([UserID])
GO
ALTER TABLE [UserRole] CHECK CONSTRAINT [FK_UserRole_UserDetail]
GO
ALTER TABLE [UserSaleLocation]  WITH CHECK ADD  CONSTRAINT [FK_UserSaleLocation_UserDetail] FOREIGN KEY([UserID])
REFERENCES [UserDetail] ([UserID])
GO
ALTER TABLE [UserSaleLocation] CHECK CONSTRAINT [FK_UserSaleLocation_UserDetail]
GO
ALTER TABLE [ViewLogHistory]  WITH CHECK ADD  CONSTRAINT [FK_ViewLogHistory_Horse] FOREIGN KEY([HorseID])
REFERENCES [Horse] ([HorseID])
GO
ALTER TABLE [ViewLogHistory] CHECK CONSTRAINT [FK_ViewLogHistory_Horse]
GO
ALTER TABLE [ViewLogHistory]  WITH CHECK ADD  CONSTRAINT [FK_ViewLogHistory_UserDetail] FOREIGN KEY([UserID])
REFERENCES [UserDetail] ([UserID])
GO
ALTER TABLE [ViewLogHistory] CHECK CONSTRAINT [FK_ViewLogHistory_UserDetail]
GO
ALTER TABLE [ViewLogHistory]  WITH CHECK ADD  CONSTRAINT [FK_ViewLogHistory_UserSaleLocation] FOREIGN KEY([UserSaleLocationID])
REFERENCES [UserSaleLocation] ([UserSaleLocationID])
GO
ALTER TABLE [ViewLogHistory] CHECK CONSTRAINT [FK_ViewLogHistory_UserSaleLocation]
GO


CREATE PROCEDURE [GetSalesCardByUserID] 
	
AS
BEGIN
	select * from salescard
END
GO
-- HorseViewerChart
CREATE PROCEDURE [HorseViewerChart]
	@UserID varchar(max)=null
AS
BEGIN
	select H.Name xAxis,cast(COUNT(*) as varchar(max)) yAxis from ViewLogHistory VH
	left join UserSaleLocation USL on VH.UserSaleLocationID= USL.UserSaleLocationID 
	left join Horse H on VH.HorseID = H.HorseID
	where USL.UserID in (select items from dbo.Split(@UserID,',')) and USL.Active=1
	group by H.Name
END


GO


CREATE FUNCTION [Split](@String varchar(8000), @Delimiter char(1))       
returns @temptable TABLE (items varchar(8000))
as       
begin       
    declare @idx int       
    declare @slice varchar(8000)       
      
    select @idx = 1       
        if len(@String)<1 or @String is null  return       
      
    while @idx!= 0       
    begin       
        set @idx = charindex(@Delimiter,@String)       
        if @idx!=0       
            set @slice = left(@String,@idx - 1)       
        else       
            set @slice = @String       
          
        if(len(@slice)>0)  
            insert into @temptable(Items) values(@slice)       
  
        set @String = right(@String,len(@String) - @idx)       
        if len(@String) = 0 break       
    end   
return       
end

GO
