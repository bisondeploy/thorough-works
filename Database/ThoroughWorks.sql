/****** Object:  Table [dbo].[UserDetail]    Script Date: 03/17/2014 16:02:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserDetail](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [varchar](100) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[FirstName] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[EmailID] [varchar](200) NULL,
	[PhoneNumber] [varchar](20) NULL,
	[Active] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_UserDetail] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[UserDetail] ON
INSERT [dbo].[UserDetail] ([UserID], [UserName], [Password], [FirstName], [LastName], [EmailID], [PhoneNumber], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (59, N'Admin', N'Admin25', N'Admin', N'Admin', N'Admin@Admin.com', N'9998471231', 1, CAST(0x0000A2E900B35A43 AS DateTime), 0, CAST(0x0000A2ED00B69F54 AS DateTime), 59)
INSERT [dbo].[UserDetail] ([UserID], [UserName], [Password], [FirstName], [LastName], [EmailID], [PhoneNumber], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (60, N'Normal', N'Normal25', N'Normal', N'Normal User', N'Normal@Normal.com', N'96018810866', 1, CAST(0x0000A2E900B54204 AS DateTime), 59, CAST(0x0000A2ED00B6DBB7 AS DateTime), 59)
INSERT [dbo].[UserDetail] ([UserID], [UserName], [Password], [FirstName], [LastName], [EmailID], [PhoneNumber], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (61, N'test', N'test', N'test', N'test', N'test@test.com', N'test', 0, CAST(0x0000A2E900B5D8AE AS DateTime), 59, NULL, NULL)
INSERT [dbo].[UserDetail] ([UserID], [UserName], [Password], [FirstName], [LastName], [EmailID], [PhoneNumber], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (62, N'Chandni', N'Chandni25', N'Chandni', N'Solanki', N'chandni@gmail.com', N'123456789', 1, CAST(0x0000A2E9016CC3E2 AS DateTime), 59, NULL, NULL)
INSERT [dbo].[UserDetail] ([UserID], [UserName], [Password], [FirstName], [LastName], [EmailID], [PhoneNumber], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (63, N'Chandni', N'Solanki', N'Chandni', N'Solanki', N'Chandni@gmail.com', N'978978', 0, CAST(0x0000A2ED008B7A29 AS DateTime), 59, CAST(0x0000A2ED008DE4CD AS DateTime), 59)
INSERT [dbo].[UserDetail] ([UserID], [UserName], [Password], [FirstName], [LastName], [EmailID], [PhoneNumber], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (64, N'Chandni', N'Solanki', N'chandni', N'solanki', N'chandni@gn.com', N'46646', 1, CAST(0x0000A2ED008E0F7B AS DateTime), 59, NULL, NULL)
INSERT [dbo].[UserDetail] ([UserID], [UserName], [Password], [FirstName], [LastName], [EmailID], [PhoneNumber], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (65, N'chandresh', N'chand123', N'Chandresh', N'Chandresh', N'chandresh@g.com', N'888', 1, CAST(0x0000A2ED008E55CB AS DateTime), 59, NULL, NULL)
INSERT [dbo].[UserDetail] ([UserID], [UserName], [Password], [FirstName], [LastName], [EmailID], [PhoneNumber], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (66, N'Monish', N'Solanki', N'Monish', N'Solanki', N'monish@gmail.com', N'8888888', 1, CAST(0x0000A2ED008FA6C4 AS DateTime), 59, CAST(0x0000A2ED009001A4 AS DateTime), 59)
INSERT [dbo].[UserDetail] ([UserID], [UserName], [Password], [FirstName], [LastName], [EmailID], [PhoneNumber], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (67, N'Jigar', N'Jigar', N'Jigar', NULL, NULL, NULL, 1, CAST(0x0000A2F100CC45E0 AS DateTime), 59, CAST(0x0000A2F100CC5327 AS DateTime), 59)
SET IDENTITY_INSERT [dbo].[UserDetail] OFF
/****** Object:  Table [dbo].[RoleMaster]    Script Date: 03/17/2014 16:02:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RoleMaster](
	[RoleMasterID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](500) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_RoleMaster] PRIMARY KEY CLUSTERED 
(
	[RoleMasterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[RoleMaster] ON
INSERT [dbo].[RoleMaster] ([RoleMasterID], [RoleName], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, N'Admin', 1, CAST(0x0000A3FF00000000 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[RoleMaster] ([RoleMasterID], [RoleName], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (2, N'NormalUser', 1, CAST(0x0000A3FF00000000 AS DateTime), NULL, NULL, NULL)
INSERT [dbo].[RoleMaster] ([RoleMasterID], [RoleName], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (4, N'vets', 1, CAST(0x0000A3FF00000000 AS DateTime), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RoleMaster] OFF
/****** Object:  Table [dbo].[Horse]    Script Date: 03/17/2014 16:02:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Horse](
	[HorseID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[Name] [varchar](200) NULL,
	[DOB] [datetime] NULL,
	[Sire] [varchar](400) NULL,
	[Dam] [varchar](max) NULL,
	[Owner] [varchar](500) NULL,
	[Image] [varchar](500) NULL,
	[DateArrived] [datetime] NULL,
	[DateSold] [datetime] NULL,
	[Comments] [varchar](max) NULL,
	[Attachments] [varchar](200) NULL,
	[Active] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_Horse] PRIMARY KEY CLUSTERED 
(
	[HorseID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Horse] ON
INSERT [dbo].[Horse] ([HorseID], [UserID], [Name], [DOB], [Sire], [Dam], [Owner], [Image], [DateArrived], [DateSold], [Comments], [Attachments], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, 59, N'Horse 1', CAST(0x0000A2E900000000 AS DateTime), N'Sir1', N'Dam1', N'Owner1', N'sonarika-bhaduria.jpg', CAST(0x0000A2E600000000 AS DateTime), CAST(0x0000A2F500000000 AS DateTime), N'some comment', N'Lighthouse.jpg', 1, CAST(0x0000A2E900B93AE7 AS DateTime), 59, CAST(0x0000A2F100CCC2FF AS DateTime), NULL)
INSERT [dbo].[Horse] ([HorseID], [UserID], [Name], [DOB], [Sire], [Dam], [Owner], [Image], [DateArrived], [DateSold], [Comments], [Attachments], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (2, 60, N'Horse2', CAST(0x0000A2E900000000 AS DateTime), N'Sir2', N'Dam2', N'Owner2', N'Koala.jpg', CAST(0x0000A2F900000000 AS DateTime), CAST(0x0000A2FA00000000 AS DateTime), N'comments', N'Penguins.jpg', 1, CAST(0x0000A2E900B97BE3 AS DateTime), 59, CAST(0x0000A2E900B9E8F6 AS DateTime), NULL)
INSERT [dbo].[Horse] ([HorseID], [UserID], [Name], [DOB], [Sire], [Dam], [Owner], [Image], [DateArrived], [DateSold], [Comments], [Attachments], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (3, 60, N'Horse3', CAST(0x0000A2E300000000 AS DateTime), N'Sir3', N'Dam3', N'Owner3', NULL, CAST(0x0000A2EC00000000 AS DateTime), CAST(0x0000A2FD00000000 AS DateTime), N'commentsss', NULL, 1, CAST(0x0000A2E900B9BF6B AS DateTime), 59, CAST(0x0000A2E900DCE893 AS DateTime), NULL)
INSERT [dbo].[Horse] ([HorseID], [UserID], [Name], [DOB], [Sire], [Dam], [Owner], [Image], [DateArrived], [DateSold], [Comments], [Attachments], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (4, 60, N'Horse4', CAST(0x0000A2E900000000 AS DateTime), N'Sir4', N'Dam4', N'Owner4', N'Tulips.jpg', CAST(0x0000A2EC00000000 AS DateTime), CAST(0x0000A2F300000000 AS DateTime), N'test dddd', NULL, 1, CAST(0x0000A2E9012BCE2F AS DateTime), 60, CAST(0x0000A2ED00A29F10 AS DateTime), NULL)
INSERT [dbo].[Horse] ([HorseID], [UserID], [Name], [DOB], [Sire], [Dam], [Owner], [Image], [DateArrived], [DateSold], [Comments], [Attachments], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (5, 62, N'Horse5', CAST(0x0000A2E900000000 AS DateTime), N'test test', N'test', N'Chandni', N'Lighthouse.jpg', CAST(0x0000A2ED00000000 AS DateTime), CAST(0x0000A2F900000000 AS DateTime), N'test some comments
', N'Koala.jpg', 1, CAST(0x0000A2E9016D149E AS DateTime), 62, CAST(0x0000A2E9016D8F1D AS DateTime), NULL)
INSERT [dbo].[Horse] ([HorseID], [UserID], [Name], [DOB], [Sire], [Dam], [Owner], [Image], [DateArrived], [DateSold], [Comments], [Attachments], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (6, 62, N'test', CAST(0x0000A2E500000000 AS DateTime), N'test', N'test', N'test', NULL, CAST(0x0000A2E400000000 AS DateTime), NULL, NULL, NULL, 1, CAST(0x0000A2E9016D3D4C AS DateTime), 62, NULL, NULL)
INSERT [dbo].[Horse] ([HorseID], [UserID], [Name], [DOB], [Sire], [Dam], [Owner], [Image], [DateArrived], [DateSold], [Comments], [Attachments], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (7, 60, N'horse5556', CAST(0x0000A2ED00000000 AS DateTime), N'123', N'123', N'123', N'Lighthouse.jpg', CAST(0x0000A2EC00000000 AS DateTime), CAST(0x0000A2FB00000000 AS DateTime), N'test comments edits
', N'Tulips.jpg', 1, CAST(0x0000A2ED00A32622 AS DateTime), 59, CAST(0x0000A2ED00A4F197 AS DateTime), NULL)
INSERT [dbo].[Horse] ([HorseID], [UserID], [Name], [DOB], [Sire], [Dam], [Owner], [Image], [DateArrived], [DateSold], [Comments], [Attachments], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (8, 64, N'Chandni horse', CAST(0x0000A2EE00000000 AS DateTime), NULL, NULL, NULL, N'Chrysanthemum.jpg', CAST(0x0000A2E400000000 AS DateTime), CAST(0x0000A2ED00000000 AS DateTime), N'some comment', NULL, 1, CAST(0x0000A2EE00B1D6C8 AS DateTime), 64, CAST(0x0000A2EE00B1F891 AS DateTime), NULL)
INSERT [dbo].[Horse] ([HorseID], [UserID], [Name], [DOB], [Sire], [Dam], [Owner], [Image], [DateArrived], [DateSold], [Comments], [Attachments], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (9, 64, N'Chandni horse2', CAST(0x0000A2EE00000000 AS DateTime), NULL, NULL, N'Chandni', NULL, CAST(0x0000A2C900000000 AS DateTime), CAST(0x0000A2F500000000 AS DateTime), NULL, NULL, 1, CAST(0x0000A2EE00B1EC67 AS DateTime), 64, CAST(0x0000A2EE00B204A9 AS DateTime), NULL)
INSERT [dbo].[Horse] ([HorseID], [UserID], [Name], [DOB], [Sire], [Dam], [Owner], [Image], [DateArrived], [DateSold], [Comments], [Attachments], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (10, 60, N'test', CAST(0x0000A2E600000000 AS DateTime), NULL, NULL, NULL, NULL, CAST(0x0000A2EC00000000 AS DateTime), CAST(0x0000A2EB00000000 AS DateTime), N'test comments', NULL, 1, CAST(0x0000A2EF011A5204 AS DateTime), 60, CAST(0x0000A2EF014D5186 AS DateTime), NULL)
INSERT [dbo].[Horse] ([HorseID], [UserID], [Name], [DOB], [Sire], [Dam], [Owner], [Image], [DateArrived], [DateSold], [Comments], [Attachments], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (11, 59, N'Horse 2', CAST(0x0000A2EB00000000 AS DateTime), NULL, NULL, NULL, NULL, CAST(0x0000A2FA00000000 AS DateTime), CAST(0x0000A2FC00000000 AS DateTime), N'test comment', NULL, 1, CAST(0x0000A2F000E45479 AS DateTime), 59, NULL, NULL)
INSERT [dbo].[Horse] ([HorseID], [UserID], [Name], [DOB], [Sire], [Dam], [Owner], [Image], [DateArrived], [DateSold], [Comments], [Attachments], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (12, 59, N'Horse 3', CAST(0x0000A2E400000000 AS DateTime), N'Test sire', N'Test Dam', N'Test Owner', N'images.jpg', CAST(0x0000A2E300000000 AS DateTime), CAST(0x0000A2E400000000 AS DateTime), N'Test Comment', NULL, 1, CAST(0x0000A2F100CD2F06 AS DateTime), 59, CAST(0x0000A2F100CD3DBE AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[Horse] OFF
/****** Object:  Table [dbo].[UserSaleLocation]    Script Date: 03/17/2014 16:02:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserSaleLocation](
	[UserSaleLocationID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[SaleName] [varchar](500) NOT NULL,
	[Location] [varchar](max) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Active] [bit] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_UserSaleLocation] PRIMARY KEY CLUSTERED 
(
	[UserSaleLocationID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[UserSaleLocation] ON
INSERT [dbo].[UserSaleLocation] ([UserSaleLocationID], [UserID], [SaleName], [Location], [StartDate], [EndDate], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (1, 60, N'Sale2', N'address2,
city
state
city
zip', CAST(0x0000A2ED00000000 AS DateTime), CAST(0x0000A2ED00000000 AS DateTime), 0, CAST(0x0000A2EC00D15984 AS DateTime), 60, CAST(0x0000A2EC00D1721C AS DateTime), 60)
INSERT [dbo].[UserSaleLocation] ([UserSaleLocationID], [UserID], [SaleName], [Location], [StartDate], [EndDate], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (2, 60, N'sale1', N'fff', CAST(0x0000A2EC00000000 AS DateTime), CAST(0x0000A2EC00000000 AS DateTime), 1, CAST(0x0000A2EC00D539EA AS DateTime), 60, NULL, NULL)
INSERT [dbo].[UserSaleLocation] ([UserSaleLocationID], [UserID], [SaleName], [Location], [StartDate], [EndDate], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (3, 60, N'sale2', N'test address,
test city', CAST(0x0000A2F900000000 AS DateTime), CAST(0x0000A2FB00000000 AS DateTime), 0, CAST(0x0000A2EC00D57A26 AS DateTime), 60, CAST(0x0000A2ED00C81D3F AS DateTime), 60)
INSERT [dbo].[UserSaleLocation] ([UserSaleLocationID], [UserID], [SaleName], [Location], [StartDate], [EndDate], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (4, 62, N'Chandni Sale', N'Location', CAST(0x0000A2ED00000000 AS DateTime), CAST(0x0000A2ED00000000 AS DateTime), 1, CAST(0x0000A2ED00CF85C1 AS DateTime), 62, NULL, NULL)
INSERT [dbo].[UserSaleLocation] ([UserSaleLocationID], [UserID], [SaleName], [Location], [StartDate], [EndDate], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (5, 64, N'Location1', N'Location1', CAST(0x0000A2F200000000 AS DateTime), CAST(0x0000A2FB00000000 AS DateTime), 0, CAST(0x0000A2EE00B218FB AS DateTime), 64, NULL, NULL)
INSERT [dbo].[UserSaleLocation] ([UserSaleLocationID], [UserID], [SaleName], [Location], [StartDate], [EndDate], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (6, 60, N'sale3', N'test', NULL, NULL, 1, CAST(0x0000A2EF00AA1AC7 AS DateTime), 60, NULL, NULL)
INSERT [dbo].[UserSaleLocation] ([UserSaleLocationID], [UserID], [SaleName], [Location], [StartDate], [EndDate], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (7, 60, N'sale5', N'test test location', CAST(0x0000A2F400000000 AS DateTime), CAST(0x0000A2FC00000000 AS DateTime), 0, CAST(0x0000A2EF00FC621E AS DateTime), 60, CAST(0x0000A2EF0101F498 AS DateTime), 60)
INSERT [dbo].[UserSaleLocation] ([UserSaleLocationID], [UserID], [SaleName], [Location], [StartDate], [EndDate], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (8, 60, N'Sale 5', N'Location edit', CAST(0x0000A2EF00000000 AS DateTime), CAST(0x0000A2F500000000 AS DateTime), 1, CAST(0x0000A2EF0152FA07 AS DateTime), 60, CAST(0x0000A2EF0153084A AS DateTime), 60)
INSERT [dbo].[UserSaleLocation] ([UserSaleLocationID], [UserID], [SaleName], [Location], [StartDate], [EndDate], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (9, 59, N'Sale 1', N'location1', CAST(0x0000A2F000000000 AS DateTime), CAST(0x0000A2F000000000 AS DateTime), 1, CAST(0x0000A2F000E46F1A AS DateTime), 59, NULL, NULL)
INSERT [dbo].[UserSaleLocation] ([UserSaleLocationID], [UserID], [SaleName], [Location], [StartDate], [EndDate], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (10, 59, N'sale 2', N'location 2', CAST(0x0000A2F000000000 AS DateTime), CAST(0x0000A2F000000000 AS DateTime), 1, CAST(0x0000A2F000E47EC5 AS DateTime), 59, NULL, NULL)
INSERT [dbo].[UserSaleLocation] ([UserSaleLocationID], [UserID], [SaleName], [Location], [StartDate], [EndDate], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (11, 64, N'sale 3', N'location', CAST(0x0000A2F100000000 AS DateTime), CAST(0x0000A2F100000000 AS DateTime), 1, CAST(0x0000A2F100B693E3 AS DateTime), 64, NULL, NULL)
INSERT [dbo].[UserSaleLocation] ([UserSaleLocationID], [UserID], [SaleName], [Location], [StartDate], [EndDate], [Active], [CreatedDate], [CreatedBy], [UpdatedDate], [UpdatedBy]) VALUES (12, 59, N'Sale 3', N'Location', CAST(0x0000A2F100000000 AS DateTime), CAST(0x0000A2F100000000 AS DateTime), 1, CAST(0x0000A2F100CD586C AS DateTime), 59, NULL, NULL)
SET IDENTITY_INSERT [dbo].[UserSaleLocation] OFF
/****** Object:  Table [dbo].[UserRole]    Script Date: 03/17/2014 16:02:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRole](
	[UserRoleID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NULL,
	[RoleMasterID] [int] NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[UserRoleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[UserRole] ON
INSERT [dbo].[UserRole] ([UserRoleID], [UserID], [RoleMasterID]) VALUES (2, 59, 1)
INSERT [dbo].[UserRole] ([UserRoleID], [UserID], [RoleMasterID]) VALUES (7, 60, 2)
INSERT [dbo].[UserRole] ([UserRoleID], [UserID], [RoleMasterID]) VALUES (8, 60, 4)
INSERT [dbo].[UserRole] ([UserRoleID], [UserID], [RoleMasterID]) VALUES (9, 61, 1)
INSERT [dbo].[UserRole] ([UserRoleID], [UserID], [RoleMasterID]) VALUES (10, 62, 2)
INSERT [dbo].[UserRole] ([UserRoleID], [UserID], [RoleMasterID]) VALUES (11, 62, 4)
INSERT [dbo].[UserRole] ([UserRoleID], [UserID], [RoleMasterID]) VALUES (16, 63, 1)
INSERT [dbo].[UserRole] ([UserRoleID], [UserID], [RoleMasterID]) VALUES (17, 63, 4)
INSERT [dbo].[UserRole] ([UserRoleID], [UserID], [RoleMasterID]) VALUES (18, 65, 1)
INSERT [dbo].[UserRole] ([UserRoleID], [UserID], [RoleMasterID]) VALUES (20, 66, 1)
INSERT [dbo].[UserRole] ([UserRoleID], [UserID], [RoleMasterID]) VALUES (22, 67, 2)
SET IDENTITY_INSERT [dbo].[UserRole] OFF
/****** Object:  Table [dbo].[UserHorseSale]    Script Date: 03/17/2014 16:02:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserHorseSale](
	[UserHorseSaleID] [int] IDENTITY(1,1) NOT NULL,
	[HorseID] [int] NOT NULL,
	[UserSaleLocationID] [int] NOT NULL,
	[Sequence] [int] NOT NULL,
 CONSTRAINT [PK_UserHorseSale] PRIMARY KEY CLUSTERED 
(
	[UserHorseSaleID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[UserHorseSale] ON
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (1, 2, 2, 1)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (2, 3, 2, 2)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (5, 8, 5, 0)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (8, 9, 5, 0)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (30, 2, 3, 1)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (31, 3, 3, 1)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (32, 4, 3, 1)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (35, 2, 6, 3)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (40, 3, 6, 2)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (41, 4, 6, 1)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (44, 4, 2, 3)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (50, 3, 7, 1)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (51, 4, 7, 2)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (53, 7, 7, 3)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (55, 3, 8, 3)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (56, 4, 8, 2)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (57, 7, 8, 1)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (58, 2, 8, 4)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (59, 1, 9, 1)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (60, 11, 10, 2)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (61, 1, 10, 1)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (62, 8, 11, 1)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (63, 9, 11, 2)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (64, 1, 12, 2)
INSERT [dbo].[UserHorseSale] ([UserHorseSaleID], [HorseID], [UserSaleLocationID], [Sequence]) VALUES (65, 11, 12, 1)
SET IDENTITY_INSERT [dbo].[UserHorseSale] OFF
/****** Object:  ForeignKey [FK_Horse_UserDetail]    Script Date: 03/17/2014 16:02:00 ******/
ALTER TABLE [dbo].[Horse]  WITH CHECK ADD  CONSTRAINT [FK_Horse_UserDetail] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserDetail] ([UserID])
GO
ALTER TABLE [dbo].[Horse] CHECK CONSTRAINT [FK_Horse_UserDetail]
GO
/****** Object:  ForeignKey [FK_UserSaleLocation_UserDetail]    Script Date: 03/17/2014 16:02:00 ******/
ALTER TABLE [dbo].[UserSaleLocation]  WITH CHECK ADD  CONSTRAINT [FK_UserSaleLocation_UserDetail] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserDetail] ([UserID])
GO
ALTER TABLE [dbo].[UserSaleLocation] CHECK CONSTRAINT [FK_UserSaleLocation_UserDetail]
GO
/****** Object:  ForeignKey [FK_UserRole_RoleMaster]    Script Date: 03/17/2014 16:02:00 ******/
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_RoleMaster] FOREIGN KEY([RoleMasterID])
REFERENCES [dbo].[RoleMaster] ([RoleMasterID])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_RoleMaster]
GO
/****** Object:  ForeignKey [FK_UserRole_UserDetail]    Script Date: 03/17/2014 16:02:00 ******/
ALTER TABLE [dbo].[UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UserRole_UserDetail] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserDetail] ([UserID])
GO
ALTER TABLE [dbo].[UserRole] CHECK CONSTRAINT [FK_UserRole_UserDetail]
GO
/****** Object:  ForeignKey [FK_UserHorseSale_Horse]    Script Date: 03/17/2014 16:02:00 ******/
ALTER TABLE [dbo].[UserHorseSale]  WITH CHECK ADD  CONSTRAINT [FK_UserHorseSale_Horse] FOREIGN KEY([HorseID])
REFERENCES [dbo].[Horse] ([HorseID])
GO
ALTER TABLE [dbo].[UserHorseSale] CHECK CONSTRAINT [FK_UserHorseSale_Horse]
GO
/****** Object:  ForeignKey [FK_UserHorseSale_UserSaleLocation1]    Script Date: 03/17/2014 16:02:00 ******/
ALTER TABLE [dbo].[UserHorseSale]  WITH CHECK ADD  CONSTRAINT [FK_UserHorseSale_UserSaleLocation1] FOREIGN KEY([UserSaleLocationID])
REFERENCES [dbo].[UserSaleLocation] ([UserSaleLocationID])
GO
ALTER TABLE [dbo].[UserHorseSale] CHECK CONSTRAINT [FK_UserHorseSale_UserSaleLocation1]
GO
