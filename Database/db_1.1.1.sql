


CREATE TABLE [dbo].[VetWork](
	[VetWorkID] [int] IDENTITY(1,1) NOT NULL,
	[HorseID] [int] NOT NULL,
	[UserID] [int] NOT NULL,
	[VisitDate] [datetime] NOT NULL,
	[InFor] [varchar](500) NOT NULL,
	[Status] [varchar](20) NULL,
	[CreatedDate] [datetime] NOT NULL,
	[CreatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[Active] [bit] NOT NULL,
	[AgeCategory] [int] NULL,
	[WorkCompleted] [varchar](500) NULL,
 CONSTRAINT [PK_VetWork] PRIMARY KEY CLUSTERED 
(
	[VetWorkID] ASC
)
)


GO

ALTER TABLE [dbo].[VetWork]  WITH CHECK ADD  CONSTRAINT [FK_HorseVetWork] FOREIGN KEY([HorseID])
REFERENCES [dbo].[Horse] ([HorseID])
GO

ALTER TABLE [dbo].[VetWork] CHECK CONSTRAINT [FK_HorseVetWork]
GO

ALTER TABLE [dbo].[VetWork]  WITH CHECK ADD  CONSTRAINT [FK_VetWorkUserDetail] FOREIGN KEY([UserID])
REFERENCES [dbo].[UserDetail] ([UserID])
GO

ALTER TABLE [dbo].[VetWork] CHECK CONSTRAINT [FK_VetWorkUserDetail]
GO

ALTER TABLE Horse 
ADD AgeCategory int
GO

CREATE TABLE [dbo].[VetWorkAttributes](
	[VetWorkAttributeID] [int] IDENTITY(1,1) NOT NULL,
	SubscriberID int not null,
	[AttributeName] [varchar](100) not NULL,
	Sequence int not null,
	[Active] [bit] not NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
    CONSTRAINT [PK_VetWorkAttributes] PRIMARY KEY CLUSTERED ([VetWorkAttributeID] ASC),
	CONSTRAINT FK_VetWorkAttributes_Subscriber Foreign KEY (SubscriberID) References Subscriber(SubscriberID)
)



CREATE TABLE [dbo].[VetWorkCompleted](
	[VetWorkCompletedID] [int] IDENTITY(1,1) NOT NULL,
	[VetWorkID] [int] NOT NULL,
	[VetWorkAttributeID] [int] NOT NULL,
	[AttributeValue] [varchar](100) NULL,
	[CreatedBy] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_VetWorkCompleted] PRIMARY KEY CLUSTERED 
(
	[VetWorkCompletedID] ASC
)
)

GO

ALTER TABLE [dbo].[VetWorkCompleted]  WITH CHECK ADD  CONSTRAINT [FK_VetWorkCompleted_VetWork] FOREIGN KEY([VetWorkID])
REFERENCES [dbo].[VetWork] ([VetWorkID])
GO

ALTER TABLE [dbo].[VetWorkCompleted] CHECK CONSTRAINT [FK_VetWorkCompleted_VetWork]
GO

ALTER TABLE [dbo].[VetWorkCompleted]  WITH CHECK ADD  CONSTRAINT [FK_VetWorkCompleted_VetWorkAttributes] FOREIGN KEY([VetWorkAttributeID])
REFERENCES [dbo].[VetWorkAttributes] ([VetWorkAttributeID])
GO

ALTER TABLE [dbo].[VetWorkCompleted] CHECK CONSTRAINT [FK_VetWorkCompleted_VetWorkAttributes]
GO


-- insert role
insert into RoleMaster
(RoleName, Active, CreatedBy, CreatedDate)
values
('Vet',1,0,getdate())
go

CREATE UNIQUE NONCLUSTERED INDEX [NonClusteredIndex-VetWork_VetWorkAttribute_Uniq] ON [dbo].[VetWorkCompleted]
(
	[VetWorkID] ASC,
	[VetWorkAttributeID] ASC
)
GO

ALTER TABLE VetWork
ALTER COLUMN InFor varchar(500) null