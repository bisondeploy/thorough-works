-- Cards
alter table Card 
add SubscriberID int
go

update c
set c.SubscriberID = u.SubscriberID
from Card c join SubscriberUser u on u.UserID = c.UserID
go

alter table Card 
alter column SubscriberID int not null
go

alter table Card 
add constraint FK_Card_SubscriberID foreign key (SubscriberID) references Subscriber(SubscriberID)
go


-- Horses
alter table Horse
add SubscriberID int
go

update c
set c.SubscriberID = u.SubscriberID
from Horse c join SubscriberUser u on u.UserID = c.UserID
go

alter table Horse
alter column SubscriberID int not null
go

alter table Horse
add constraint FK_Horse_SubscriberID foreign key (SubscriberID) references Subscriber(SubscriberID)
go

-- SaleLocation
alter table UserSaleLocation
add SubscriberID int
go

update c
set c.SubscriberID = u.SubscriberID
from UserSaleLocation c join SubscriberUser u on u.UserID = c.UserID
go

alter table UserSaleLocation
alter column SubscriberID int not null
go

alter table UserSaleLocation
add constraint FK_UserSaleLocation_SubscriberID foreign key (SubscriberID) references Subscriber(SubscriberID)
go

-- Role name update
update rolemaster
set rolename ='Normal User'
where rolemasterid = 2
go