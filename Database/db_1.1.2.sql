CREATE TABLE [dbo].[Sire](
	[SireID] [int] IDENTITY(1,1) NOT NULL,
	[SireName] [varchar](400) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_Sire] PRIMARY KEY CLUSTERED 
(
	[SireID] ASC
)
)

CREATE TABLE [dbo].[HorseOwner](
	[HorseOwnerID] [int] IDENTITY(1,1) NOT NULL,
	[HorseID] [int] NOT NULL,
	[OwnerID] [int] NOT NULL,
 CONSTRAINT [PK_HorseOwner] PRIMARY KEY CLUSTERED 
(
	[HorseOwnerID] ASC
)
) 

GO

ALTER TABLE [dbo].[HorseOwner]  WITH CHECK ADD  CONSTRAINT [FK_HorseOwner_Horse] FOREIGN KEY([HorseID])
REFERENCES [dbo].[Horse] ([HorseID])
GO

ALTER TABLE [dbo].[HorseOwner] CHECK CONSTRAINT [FK_HorseOwner_Horse]
GO

ALTER TABLE [dbo].[HorseOwner]  WITH CHECK ADD  CONSTRAINT [FK_HorseOwner_Owner] FOREIGN KEY([OwnerID])
REFERENCES [dbo].[Owner] ([OwnerID])
GO

ALTER TABLE [dbo].[HorseOwner] CHECK CONSTRAINT [FK_HorseOwner_Owner]
GO


INSERT INTO [HorseOwner]
SELECT HorseID, OwnerID FROM Horse WHERE OwnerID IS NOT NULL
GO

ALTER TABLE [dbo].[Horse] DROP CONSTRAINT [FK_Horse_OwnerID]

ALTER TABLE Horse DROP COLUMN OwnerID

alter table Horse add SireID int
go



-- Init Sire table from existing data
INSERT INTO Sire
SELECT Distinct 
Sire, 
1 AS Active,
1 AS CreatedBy,
GETDATE() AS CreatedDate,
null,
null
FROM Horse
WHERE Sire IS NOT NULL
go

-- Point Horse table to new SireIDs
UPDATE Horse 
SET Horse.SireID = Sire.SireID 
FROM Sire WHERE Sire.SireName = Horse.Sire
GO

-- Get rid of Sire column as now using FK to Sire table
ALTER TABLE Horse DROP COLUMN Sire
GO

ALTER TABLE [dbo].[UserSaleLocation] ADD NumberSessions int
GO
UPDATE [dbo].[UserSaleLocation] SET NumberSessions=1
GO
ALTER TABLE [dbo].[UserSaleLocation] ALTER COLUMN NumberSessions int not null
GO
ALTER TABLE [dbo].[UserHorseSale] ADD SessionNumber int
GO
UPDATE [dbo].[UserHorseSale] SET SessionNumber=1
GO
ALTER TABLE [dbo].[UserHorseSale] ALTER COLUMN SessionNumber int not null
GO
ALTER TABLE [dbo].[Card] ADD InspectionDate datetime
GO
UPDATE Card SET InspectionDate = CreatedDate
GO
ALTER TABLE [dbo].[Card] ALTER COLUMN InspectionDate datetime not null
GO

-- 1.1.2a

-- Add current flag to horse table
ALTER TABLE Horse Add [Current] bit
go

UPDATE Horse Set [Current] = 1
go
ALTER TABLE Horse alter column [Current] bit not null
go

--Logo
ALTER TABLE [dbo].[Subscriber] ADD LogoFile varchar(5000)
go

-- Viewers
	CREATE TABLE [dbo].[Viewer](
	[ViewerID] [int] IDENTITY(1,1) NOT NULL,
	[ViewerName] [varchar](400) NOT NULL,
	[Active] [bit] NOT NULL,
	[CreatedBy] [int] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[UpdatedBy] [int] NULL,
	[UpdatedDate] [datetime] NULL,
 CONSTRAINT [PK_Viewer] PRIMARY KEY CLUSTERED 
(
	[ViewerID] ASC
)
)
go

alter table UserDetail
add EmailSignature varchar(max)
go
