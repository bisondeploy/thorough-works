CREATE TABLE [dbo].[communication_queue](
	[cq_id] [int] IDENTITY(1,1) NOT NULL,
	[cq_from_id] [int] NOT NULL,
	[cq_address_from] [varchar](200) NULL,
	[cq_address_to] [varchar](400) NULL,
	[cq_address_cc] [varchar](400) NULL,
	[cq_address_bcc] [varchar](400) NULL,
	[cq_subject] [varchar](200) NULL,
	[cq_text] [varchar](max) NULL,
	[cq_from_name] [varchar](200) NULL,
	[cq_sent_date] [smalldatetime] NULL,
	[cq_cancelled_date] [smalldatetime] NULL,
	[cq_cancel_reason] [varchar](max) NULL,
	[cq_created_by] [int] NOT NULL,
	[cq_created] [smalldatetime] NULL,
	[cq_communication_type] [varchar](10) NOT NULL,
	constraint [PK_communication_queue] PRIMARY KEY (cq_id),
	constraint fk_communication_queue_from foreign key (cq_from_id) references communication_email_address (from_id)
)
GO

create index idx_communication_queue_1 on communication_queue (cq_sent_date, cq_cancelled_date)
go
